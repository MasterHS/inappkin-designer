//
//  NDMenuViewController.m
//  iNappkinDesigner
//
//  Created by Fei Pan on 02/06/14.
//  Copyright (c) 2014 Adesso. All rights reserved.
//

#import "NDProjectsViewController.h"
#import "NDMainMenuCollectionViewCell.h"
#import "ADSServerRequestHandler.h"
#import "NDCreateProjectViewController.h"
#import "NDProjectViewController.h"
#import "NDBackgoundLayer.h"
#import "NDHelpers.h"

#import "CoreDataManager.h"
#import "Project.h"
#import "Sketch.h"
#import "SlimServerRequestHandler.h"
#import "ServerSettingsViewController.h"


@interface NDProjectsViewController ()

@end

@implementation NDProjectsViewController

- (void) viewDidLoad{
	[super viewDidLoad];
    
	// Add refresh button
	UIBarButtonItem * refreshBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Pull Projects from Server" style:UIBarButtonItemStyleBordered target: self action: @selector(synchronizeWithServer:)];
	self.navigationItem.leftBarButtonItems = [[NSArray alloc] initWithObjects : self.navigationItem.leftBarButtonItems[0],refreshBarButtonItem, nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(removeUpdateView) name:@"ServercommunicationEnded" object:nil];
    
	[self setNeedsStatusBarAppearanceUpdate];
}

- (void) viewWillAppear : (BOOL) animated{
	CAGradientLayer * bgLayer = [NDBackgroundLayer blueGradient];
	bgLayer.frame = self.view.bounds;
	[self.view.layer insertSublayer: bgLayer atIndex : 0];
    [self getAllProjects];
	[self.projectsCollectionView reloadData];
}

#pragma mark - Button Actions

-(IBAction)synchronizeWithServer:(id)sender{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Proceed?" message:@"Pulling projects from the server will revert all unsaved local changes to projects. Do you want to proceed?" delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES", nil];
    [alert show];
}

-(IBAction)showServerSettings:(id)sender{
    ServerSettingsViewController *aVC= [[ServerSettingsViewController alloc]init];
    [self presentViewController:aVC animated:YES completion:nil];
}

#pragma mark - UICollectionView Data Source
- (NSInteger) numberOfSectionsInCollectionView : (UICollectionView *) collectionView{
	return 1;
}

- (NSInteger) collectionView : (UICollectionView *) collectionView numberOfItemsInSection : (NSInteger) section{
    return [allProjects count];
}

- (UICollectionViewCell *) collectionView: (UICollectionView *)collectionView cellForItemAtIndexPath: (NSIndexPath *)indexPath{
	NDMainMenuCollectionViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier: @"NDMenuCollectionViewCell" forIndexPath :indexPath];
    
    Project *project= [allProjects objectAtIndex:indexPath.row];
    cell.projectNameLabel.text = project.name;
    
	cell.projectNameLabel.numberOfLines = 0; // allow line breaks
	cell.projectNameLabel.textAlignment = NSTextAlignmentCenter; // center align
    
	// Project thumbnail
	UIImage * image;
    
    if ([[project sketches]count]> 0) {
        Sketch *tSketch = [[project sketches]objectAtIndex:0];
        image= [UIImage imageWithData:tSketch.imageFile];
    }
    else image = [UIImage imageNamed : @"ProjectDummyThumbnail"];
    
	cell.projectThumbnail.image = image;
	return cell;
}

#pragma mark - TOOLS

-(void)getAllProjects{
    NSManagedObjectContext *managedObjectContext = [[CoreDataManager sharedManager] managedObjectContext];
	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
	
	NSEntityDescription *entity = [NSEntityDescription entityForName:@"Project" inManagedObjectContext:managedObjectContext];
	[fetchRequest setEntity:entity];
	
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObjects: sortDescriptor, nil];
   	[fetchRequest setSortDescriptors:sortDescriptors];
    
    
    
	NSFetchedResultsController *fetchedExhibitorController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:managedObjectContext sectionNameKeyPath:nil cacheName:nil];
	[fetchedExhibitorController performFetch:nil];
    allProjects = [fetchedExhibitorController fetchedObjects];
    [self.projectsCollectionView reloadData];
}

#pragma mark - Navigation
// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void) prepareForSegue : (UIStoryboardSegue *) segue sender : (id) sender{
	if ([sender isKindOfClass : [UICollectionViewCell class]]) {
		NDMainMenuCollectionViewCell * cell = (NDMainMenuCollectionViewCell *) sender;
		NSIndexPath * indexPath = [self.projectsCollectionView indexPathForCell : sender];
		if (indexPath) {
			if ([segue.identifier isEqualToString : @"OpenProject"]) {
				NDProjectViewController * projectController =(NDProjectViewController *) segue.destinationViewController;
                projectController.image = cell.projectThumbnail.image;
                projectController.currentProject = [allProjects objectAtIndex:indexPath.row];
			}
		}
	}
}

- (IBAction) unwindToList : (UIStoryboardSegue *) segue{
    [self getAllProjects];
}

#pragma mark - UIALertView Delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 1) {
        
        [[CoreDataManager sharedManager] resetDatastore];
        self.navigationController.navigationBar.userInteractionEnabled = NO;
        self.updateIndicatorView.hidden= NO;
        [self.view bringSubviewToFront:self.updateIndicatorView];
        
        SlimServerRequestHandler *requestHandler= [[SlimServerRequestHandler alloc] initWithBaseURLStringForGeneralFunctionality:[NDHelpers getBaseServerName] baseURLStringForImageDownload:[NDHelpers getImageServerName]];
        
        [requestHandler downloadAllProjectsWithAllSubdata];
        
        [self getAllProjects];
        [self.projectsCollectionView reloadData];
    }
}

#pragma mark - Notification Methoden
-(void)removeUpdateView{
    [self getAllProjects];
    self.navigationController.navigationBar.userInteractionEnabled = YES;
    self.updateIndicatorView.hidden = YES;
    [[CoreDataManager sharedManager] saveData];
}

@end
