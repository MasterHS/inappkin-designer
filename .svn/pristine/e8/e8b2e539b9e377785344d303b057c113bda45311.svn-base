//
//  NDSketchViewController.m
//  iNappkinDesigner
//
//  Created by Fei Pan on 03/06/14.
//  Copyright (c) 2014 Adesso. All rights reserved.
//

#import "NDSketchViewController.h"
#import "NDProjectCollectionViewCell.h"
#import "NDLandscapeImagePickerController.h"
#import "NDHelpers.h"
#import "ADSFileHandler.h"
#import "Project.h"
#import "Sketch.h"
#import "CoreDataManager.h"
#import "NDTransitionButtonOverlayView.h"

@interface NDSketchViewController ()
@end

@implementation NDSketchViewController{
}

- (void) viewDidLoad{
	[super viewDidLoad];
    
    self.transitionButtonOverlay.delegate = self;
	self.sideCollectionView.allowsMultipleSelection = NO;
	self.transCollectionView.allowsMultipleSelection = NO;
    self.deleteTransitionButton.enabled=NO;
    self.transitionDestinationSelectView.hidden=YES;
    
    self.fileHandler = [[ADSFileHandler alloc] init];
}

-(void) viewDidAppear:(BOOL)animated{
}

-(void)autoSelectFirstSketch{
    if ([[self.currentProject sketches]count]>0) {
        self.currentlySelectedSketch= [[self.currentProject sketches]objectAtIndex:0];
        self.selectedSketchIndexPath = [NSIndexPath indexPathForItem:0 inSection:0];
        
        UIImage *tImage=[UIImage imageWithData:self.currentlySelectedSketch.imageFile];
        [self.sketchDetailedImage setImage:tImage];
        [self.sideCollectionView selectItemAtIndexPath:self.selectedSketchIndexPath animated:YES scrollPosition:UICollectionViewScrollPositionBottom];
        [self.sideCollectionView scrollToItemAtIndexPath: self.selectedSketchIndexPath atScrollPosition: UICollectionViewScrollPositionBottom animated: NO];
    }
    //wenns keinen Sketch gibt
    else{
        self.currentlySelectedSketch= nil;
        self.sketchDetailedImage.image= nil;
    }
    self.transitionButtonOverlay.frame= [self getDisplayRectForCurrentImage];
    [self.transitionButtonOverlay setNewActiveSketch:self.currentlySelectedSketch];
}

#pragma mark - UICollectionView Data Source
- (NSInteger) numberOfSectionsInCollectionView : (UICollectionView *) collectionView{
	return 1;
}

- (NSInteger) collectionView : (UICollectionView *) collectionView numberOfItemsInSection : (NSInteger) section{
	return [[self.currentProject sketches]count];
}

- (UICollectionViewCell *) collectionView: (UICollectionView *) collectionView cellForItemAtIndexPath: (NSIndexPath *)indexPath{
	NDProjectCollectionViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier : @"projectSidebarThumbnail" forIndexPath: indexPath];
	if ([[self.currentProject sketches]objectAtIndex:indexPath.row] != nil){
        Sketch * sketch = [[self.currentProject sketches]objectAtIndex:indexPath.row];
        
        UIImage *cellImage= [UIImage imageWithData:sketch.imageFile];
        
		cell.projectSidebarThumbnail.image = cellImage;
		cell.backgroundColor = [UIColor clearColor];
        cell.sketch= sketch;
        
        if(collectionView == self.sideCollectionView){
            if ([indexPath isEqual: self.selectedSketchIndexPath]) {
                cell.alpha = 1.0;
                cell.backgroundColor = [UIColor redColor];
            } else {
                cell.alpha = 0.6;
                cell.backgroundColor = [UIColor clearColor];
            }
        }
        if(collectionView==self.transCollectionView){
            if (cell.selected== YES) {
                cell.alpha = 1.0;
                cell.backgroundColor = [UIColor redColor];
            } else {
                cell.alpha = 0.6;
                cell.backgroundColor = [UIColor clearColor];
            }
        }
	}
	return cell;
}

#pragma mark - CollectionView Delegate
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    self.transitionDestinationSelectView.hidden = YES;
    self.deleteTransitionButton.enabled= NO;
    for (UICollectionViewCell * visibleCell in [collectionView visibleCells]) {
        visibleCell.backgroundColor = [UIColor clearColor];
        visibleCell.alpha = 0.6;
    }
    UICollectionViewCell * datasetCell = [collectionView cellForItemAtIndexPath : indexPath];
    datasetCell.alpha = 1.0;
    datasetCell.backgroundColor = [UIColor redColor];
    
    if ([collectionView isEqual : self.sideCollectionView]) {
        self.selectedSketchIndexPath = indexPath;
		self.sketchDetailedImage.alpha = 1;
        
        NDProjectCollectionViewCell *cCell= (NDProjectCollectionViewCell*)datasetCell;
		self.sketchDetailedImage.image = cCell.projectSidebarThumbnail.image;
        
        self.currentlySelectedSketch= [[self.currentProject sketches]objectAtIndex:indexPath.row];
        self.transitionButtonOverlay.frame= [self getDisplayRectForCurrentImage];
        [self.transitionButtonOverlay setNewActiveSketch: self.currentlySelectedSketch];
        
	}
 	if ([collectionView isEqual : self.transCollectionView]) {        // -.-
 		if (indexPath.item >= 0){
 			Sketch * selectedDestinationSketch =[[self.currentProject sketches]objectAtIndex: indexPath.row];
            [self.currentlySelectedTransitionButton setDestinationSketch:selectedDestinationSketch];
            self.deleteTransitionButton.enabled= NO;
            [self.transitionButtonOverlay setAllButtonBackgroundcolorsToBlue];
        }
 	}
}


#pragma mark - Bilder Aufnehmen/aus der roll hinzufuegen
#pragma mark IBActions fuer Toolbar
- (IBAction) chooseSketchFromRoll : (id) sender{
	self.pictureSource = @"roll";
	NDLandscapeImagePickerController* picker = [[NDLandscapeImagePickerController alloc] init];
	picker.delegate = self;
	[picker setSourceType : UIImagePickerControllerSourceTypeSavedPhotosAlbum];
	[self presentViewController: picker animated: YES completion: NULL];
}


- (IBAction) takeSketchPicture : (id) sender{
	NDLandscapeImagePickerController* picker = [[NDLandscapeImagePickerController alloc]init];
	picker.delegate = self;
	[picker setSourceType : UIImagePickerControllerSourceTypeCamera];
    self.pictureSource = @"camera";
	
    NSDictionary *valueDict= [NDHelpers getAspectRatioAndScreensizeForDeviceType:self.currentProject.deviceType];
    
	CGFloat heightAspect= [[valueDict objectForKey:@"aspectRatioHeight"]floatValue];
	CGFloat widthAspect= [[valueDict objectForKey:@"aspectRatioWidth"]floatValue];
    
	CGRect newFrame;
	newFrame.size.height = picker.view.frame.size.width;
	newFrame.size.width = newFrame.size.height * (widthAspect / heightAspect);
	newFrame.origin.x = (picker.view.frame.size.height - newFrame.size.width) / 2;
	UIView * overlayView = [[UIView alloc] initWithFrame : newFrame];
	[overlayView setFrame : newFrame];
	[overlayView.layer setBorderWidth : 2.0];
	[overlayView.layer setBorderColor : [[UIColor colorWithRed : 0.10 green : 0.45 blue : 0.73 alpha: 1.0]CGColor]];
	[overlayView.layer setCornerRadius : 2.0];
	[overlayView.layer setShadowOffset : CGSizeMake(-2, -2)];
	[overlayView.layer setShadowColor : [[UIColor lightGrayColor] CGColor]];
	[overlayView.layer setShadowOpacity : 0.5];
	picker.cameraOverlayView = overlayView;
	[self presentViewController: picker animated: YES completion: NULL];
}

# pragma  mark - Delete Operations
-(IBAction)deleteCurrentlySelectedTransitionButton:(id)sender{
    [[CoreDataManager sharedManager] deleteTransitionButton:self.currentlySelectedTransitionButton fromSketch:self.currentlySelectedSketch];
    //jetzt noch den Sketch mit der Datenbank updaten
    [[[CoreDataManager sharedManager] managedObjectContext] refreshObject:self.currentlySelectedSketch mergeChanges:YES];
    [self.transitionButtonOverlay setNewActiveSketch:self.currentlySelectedSketch];
    self.deleteTransitionButton.enabled= NO;
    self.transitionDestinationSelectView.hidden=YES;
}

- (IBAction) deleteSketch: (id)sender{
	if (self.currentlySelectedSketch != nil){
        [[CoreDataManager sharedManager]deleteSketch:self.currentlySelectedSketch fromProject:self.currentProject];
        [[[CoreDataManager sharedManager] managedObjectContext] refreshObject:self.currentProject mergeChanges:YES];
        [self.transCollectionView reloadData];
        [self.sideCollectionView reloadData];
        [self autoSelectFirstSketch];
	}
}

#pragma mark - Delegates
#pragma mark Delegate fuer TransitionButtonOverlayView
-(void)jumpToDestinationSketchWithIdentifier:(NSInteger)identifier{
    NSIndexPath * selectionDest = [NSIndexPath indexPathForItem : identifier inSection : 0];
    [self.sideCollectionView scrollToItemAtIndexPath : selectionDest atScrollPosition: UICollectionViewScrollPositionBottom animated : NO];
    [self collectionView : self.sideCollectionView didSelectItemAtIndexPath : selectionDest];
}

-(IBAction)transitionButtonPressed:(UIButton*)sender{
    NSInteger selectedButtonIndex=sender.tag;
    self.currentlySelectedTransitionButton = [[self.currentlySelectedSketch transitionButtons]objectAtIndex:selectedButtonIndex];
    //wir sind im Testmode -> die Transition ausfuehren
    if (self.enableTest.on) {
        Sketch *toSketch= (Sketch*) [self.currentlySelectedTransitionButton destinationSketch];
        NSUInteger toSketchIndex=  [self.currentProject.sketches indexOfObject:toSketch];
        if (toSketchIndex != NSNotFound) {
            self.selectedSketchIndexPath = [NSIndexPath indexPathForItem: toSketchIndex inSection: 0];
            self.currentlySelectedSketch= toSketch;
            
            UIImage *tImage=[UIImage imageWithData:toSketch.imageFile];
            [self.sketchDetailedImage setImage:tImage];
            
            self.transitionButtonOverlay.frame= [self getDisplayRectForCurrentImage];
            [self.transitionButtonOverlay setNewActiveSketch:self.currentlySelectedSketch];
            [self.sideCollectionView selectItemAtIndexPath:self.selectedSketchIndexPath animated:YES scrollPosition:UICollectionViewScrollPositionTop];
            [self.sideCollectionView reloadData];
        }
        return;
    }
    //wir sind im DesignerMode-> die TransitionView anzeigen damit man einen Zielsketch auswaehlen kann
    NSInteger indexOfCurrentDestinationSketch= [[self.currentProject sketches]indexOfObject:self.currentlySelectedTransitionButton.destinationSketch];
    [self.transCollectionView reloadData];
    self.transitionDestinationSelectView.hidden = NO;
    if (indexOfCurrentDestinationSketch != NSNotFound) {
        NSIndexPath *tIndexPath= [NSIndexPath indexPathForItem:indexOfCurrentDestinationSketch inSection:0];
        [self.transCollectionView scrollToItemAtIndexPath: tIndexPath atScrollPosition: UICollectionViewScrollPositionTop animated : NO];
        [self.transCollectionView selectItemAtIndexPath: tIndexPath animated:YES  scrollPosition: UICollectionViewScrollPositionTop];
    }
    
    for (UIView *tView in [self.transitionButtonOverlay subviews]) {
        if ([tView isKindOfClass:[UIButton class]]) {
            if (tView.tag == selectedButtonIndex) {
                sender.backgroundColor = [NDHelpers purpleButtonColor];
            }
            else tView.backgroundColor = [NDHelpers blueButtonColor];
        }
    }
    self.deleteTransitionButton.enabled = YES;
}

-(void)transitionButtonCreated{
    //self.transitionDestinationSelectView.hidden=NO;
}

#pragma mark Image Picker Delegate
- (void) imagePickerController: (UIImagePickerController *) picker didFinishPickingMediaWithInfo: (NSDictionary *) info{
	UIImage *image = [info objectForKey : UIImagePickerControllerOriginalImage];
    image=[NDHelpers cropScaleAndCompressImage:image forDeviceType:self.currentProject.deviceType];
    
	if ([self.pictureSource isEqualToString : @"camera"]) {
		UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil);
	}
    NSString *uniqueImageName = [[NSProcessInfo processInfo] globallyUniqueString];
    NSString *filePath =[NSString stringWithFormat:@"%@/%@",self.currentProject.name,uniqueImageName];
    NSData *tData=  UIImagePNGRepresentation(image);
    
    Sketch *sketch = [[CoreDataManager sharedManager]createSketchWithImageFilePath:filePath imageFile:tData];
    sketch.sketchOrderNumber = [NSNumber numberWithInt:[self.currentProject.sketches count]];
	[self.currentProject addSketchesObject: sketch];
    
	[self.sideCollectionView reloadData];
    [self.transCollectionView reloadData];
    [self.sideCollectionView selectItemAtIndexPath:self.selectedSketchIndexPath animated:NO scrollPosition:UICollectionViewScrollPositionTop];
    
	[self dismissViewControllerAnimated : YES completion : NULL];
    
    NSError *error = [[CoreDataManager sharedManager] saveData];
	if (error) {
		NSLog(@"Error saving Data: %@",[error localizedDescription]);
	}
    
	// Set light status bar theme
	[[UIApplication sharedApplication] setStatusBarStyle : UIStatusBarStyleLightContent];
}

- (void) imagePickerControllerDidCancel : (UIImagePickerController *) picker{
	[[UIApplication sharedApplication] setStatusBarStyle : UIStatusBarStyleLightContent];
	[self dismissViewControllerAnimated : YES completion : NULL];
}

#pragma mark - View Funktionen
- (void) touchesBegan: (NSSet *) touches withEvent: (UIEvent *) event{
    UITouch *touch = [[event allTouches] anyObject];
    if ([touch view] != self.transCollectionView) {
        self.transitionDestinationSelectView.hidden= YES;
        //alle buttons wieder auf blau setzen
        self.deleteTransitionButton.enabled = NO;
        [self.transitionButtonOverlay setAllButtonBackgroundcolorsToBlue];
    }
}

#pragma mark - Toolbar Funktionen
- (IBAction) testerModeChanged : (id) sender{
    self.deleteTransitionButton.enabled= NO;
    self.transitionDestinationSelectView.hidden=YES;
    [self.transitionButtonOverlay setAllButtonBackgroundcolorsToBlue];
}


#pragma mark - TOOLS
-(CGRect)getDisplayRectForCurrentImage{
    UIImageView *iv = self.sketchDetailedImage;
    CGSize imageSize = iv.image.size;
    CGFloat imageScale = fminf(CGRectGetWidth(iv.bounds)/imageSize.width, CGRectGetHeight(iv.bounds)/imageSize.height);
    CGSize scaledImageSize = CGSizeMake(imageSize.width*imageScale, imageSize.height*imageScale);
    CGRect imageFrame = CGRectMake(roundf(0.5f*(CGRectGetWidth(iv.bounds)-scaledImageSize.width))+self.sketchDetailedImage.frame.origin.x, roundf(0.5f*(CGRectGetHeight(iv.bounds)-scaledImageSize.height))+self.sketchDetailedImage.frame.origin.y, roundf(scaledImageSize.width), roundf(scaledImageSize.height));
    return imageFrame;
}

@end
