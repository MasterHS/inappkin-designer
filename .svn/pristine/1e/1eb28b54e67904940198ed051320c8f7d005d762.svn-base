//
//  NDHelpers.m
//  iNappkinDesigner
//
//  Created by Orest on 17/05/14.
//  Copyright (c) 2014 Adesso. All rights reserved.
//

#import "NDHelpers.h"
#import "ADSProject.h"
#import "ADSTask.h"
#import "ADSSketch.h"
#import "ADSStoryboard.h"
#import "ADSServerRequestHandler.h"
#import "NDAppDelegate.h"
#import "ADSUIElement.h"
#import "ADSTasksBoard.h"


NSString * const BaseHostnameString = @"ios14adesso-bruegge.in.tum.de";
NSString * const BaseURLString = @"http://ios14adesso-bruegge.in.tum.de:7000/";
NSString * const BaseURLStringForImage = @"http://ios14adesso-bruegge.in.tum.de:7001/";

@implementation NDHelpers

+ (void) initHelpers{
	appDelegate = [[UIApplication sharedApplication] delegate];
}

+ (ADSServerRequestHandler *) generateServerRequestHandler{
	return [[ADSServerRequestHandler alloc]
			initWithBaseURLStringForGeneralFunctionality : BaseURLString
            baseURLStringForImageDownload : BaseURLStringForImage];
}

#pragma mark - Device ID
+ (NSString *) getDevId{
	NSString * identifierForVendor = [[UIDevice currentDevice].identifierForVendor UUIDString];
	return identifierForVendor;
}

#pragma mark - Colors
+ (UIColor *)normalTextBoarderColor {
    return[UIColor colorWithRed: 120.0/255.0 green : 120.0 /255.0 blue: 120.0/255.0 alpha : 0.5];
}

+ (UIColor *)tasksBoarderColor {
    return[UIColor colorWithRed: 120.0/255.0 green : 120.0 /255.0 blue: 120.0/255.0 alpha : 0.3];
}

#pragma mark - Bildbearbeitung
+ (UIImage*)cropScaleAndCompressImage:(UIImage*)image forDeviceType:(NSString*)deviceType{
    
    // Display aspect ratios (height:width) and screensize (heightxwidth
	// iPhone 4:        3:2         960 x 640
	// iPhone 5:        16:9        1136 x 640
	// iPad Retina:     4:3         2048 x 1536
    
	// Aspect ratio to use; default 4:3
	CGFloat heightAspect = 4.0, widthAspect = 3.0;
    // New resolution; default 2048x1535
	CGFloat sheight= 2048.0f, swidth = 1535.0f;
    
	// Determine which device type the current project is meant for
	if ([deviceType isEqualToString : @"iPhone 4"]) {
		sheight = 960;
		swidth = 640;
        heightAspect = 3.0;
		widthAspect = 2.0;
	} else if ([deviceType isEqualToString : @"iPhone 5"]) {
		sheight = 1136;
		swidth = 640;
        heightAspect = 16.0;
		widthAspect = 9.0;
	} else if ([deviceType isEqualToString : @"iPad Retina"]) {
		sheight = 2048;
		swidth = 1536;
        heightAspect = 4.0;
		widthAspect = 3.0;
	}
    
	// Determine new dimensions and crop area
	CGFloat height, width;
	CGRect clippedRect;
    
	if (image.size.height * (widthAspect / heightAspect) == image.size.width) {
		NSLog(@"Skipping cropping");
		return image;
	} else if (image.size.height * (widthAspect / heightAspect) > image.size.width) {
		width = image.size.width;
		height = width * (heightAspect / widthAspect);
		NSLog(@"Too high, cropping to %.2f x %.2f", width, height);
		clippedRect = CGRectMake(0, (image.size.height - height) / 2, width, height);
	} else {
		height = image.size.height;
		width = height * (widthAspect / heightAspect);
		NSLog(@"Too wide, cropping to %.2f x %.2f", width, height);
		clippedRect = CGRectMake( (image.size.width - width) / 2, 0, width, height );
	}
    
	// Crop logic
	CGImageRef imageRef = CGImageCreateWithImageInRect([image CGImage], clippedRect);
	UIImage * newImage = [UIImage imageWithCGImage : imageRef scale : 1.0 orientation : image.imageOrientation];
	CGImageRelease(imageRef);
    
	// Scale
	CGRect rect = CGRectMake(0,0,width,height);
	UIGraphicsBeginImageContext(rect.size);
	[newImage drawInRect: rect];
	newImage = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
    
	// Compress to save space
	// Either reduce file size to 100 KB or stop trying at compressionQuality = 10%
	CGFloat compressionQuality = 0.9f; // Maximal (starting) compression quality
	CGFloat minimalCompressionQuality = 0.1f;
	int maxFileSize = 100 * 1024; // KB, for raw data only
	NSData * imageData = UIImageJPEGRepresentation(newImage, compressionQuality);
	while ([imageData length] > maxFileSize && compressionQuality > minimalCompressionQuality){
		compressionQuality -= 0.1;
		// NSLog(@"Trying compression %.2f", compression);
		imageData = UIImageJPEGRepresentation(newImage, compressionQuality);
		// NSLog(@"Got sketch size: %.2f KB", (float)(float)[imageData length]/1024.0);
	}
	// Warning: For a calculated NSData size around 200 KB, UIImage will be ca. 100 KB larger
	return [UIImage imageWithData : imageData];
    
}

@end
