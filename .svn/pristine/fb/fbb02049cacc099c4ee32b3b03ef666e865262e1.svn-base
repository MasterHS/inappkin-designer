//
//  NDTransitionButtonOverlayViewController.m
//  iNappkinDesigner
//
//  Created by Julian-Lennart Weigand on 18.08.14.
//  Copyright (c) 2014 Adesso. All rights reserved.
//

#import "NDTransitionButtonOverlayViewController.h"
#import "CoreDataManager.h"
#import "NDHelpers.h"
#import "NDSketchViewController.h"


@interface NDTransitionButtonOverlayViewController ()

@end

@implementation NDTransitionButtonOverlayViewController

-(void)setNewActiveSketch:(Sketch*)sketch{
    self.currentSketch=sketch;
   
    [self drawAllButtons];
}

- (void) drawAllButtons{
    //erstmal alle Buttons von der View entfernen
    for (UIButton *TButton in [self subviews]) {
        [TButton removeFromSuperview];
    }
    NSInteger counter=0;
    for (TransitionButton *tButton in [self.currentSketch transitionButtons]) {
        
        float absolutX= ([tButton.x floatValue]/100)*self.frame.size.width;
        float absolutY= ([tButton.y floatValue]/100)*self.frame.size.height;
        float absolutWidth = ([tButton.width floatValue]/100)*self.frame.size.width;
        float absolutHeight = ([tButton.height floatValue]/100)*self.frame.size.height;
        CGRect absolutRect= CGRectMake(absolutX, absolutY, absolutWidth, absolutHeight);
        
        UIButton *rButton = [[UIButton alloc]initWithFrame:absolutRect];
        rButton.backgroundColor = [NDHelpers blueButtonColor];
        [rButton addTarget:self.delegate action:@selector(transitionButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        rButton.tag= counter;
        counter++;
        [self addSubview:rButton];
    }
}

-(void)setAllButtonBackgroundcolorsToBlue{
    for (UIView *tView in [self subviews]) {
        if ([tView isKindOfClass:[UIButton class]]) {
            tView.backgroundColor = [NDHelpers blueButtonColor];
        }
    }
}

#pragma mark - View Delegate
- (void) touchesBegan : (NSSet *) touches withEvent : (UIEvent *) event{
    [super touchesBegan:touches withEvent:event];
	UITouch * touch = [[event allTouches] anyObject];
	self.startPoint = [touch locationInView: touch.view];
}

- (void) touchesMoved : (NSSet *) touches withEvent : (UIEvent *) event{
	for (UITouch * touch in touches) { // Get location of Touch
		CGPoint location = [touch locationInView : self];
		self.endPoint = location;
	}
}

- (void) touchesEnded : (NSSet *) touches withEvent : (UIEvent *) event{
    
    if ([self.delegate enableTest].on) {
        return;
    }
    
    UITouch * touch = [[event allTouches] anyObject];
    self.endPoint = [touch locationInView : touch.view];
    
    CGRect newButtonRect = CGRectMake(MIN(self.startPoint.x, self.endPoint.x),
                                      MIN(self.startPoint.y, self.endPoint.y),
                                      fabs(self.startPoint.x - self.endPoint.x),
                                      fabs(self.startPoint.y - self.endPoint.y));
    
    //falls der Button ausserhalb der View liegen wuerde erstellen wir ihn natuerlich nicht
    if (newButtonRect.origin.x+newButtonRect.size.width>self.frame.size.width|| newButtonRect.origin.y+newButtonRect.size.height>self.frame.size.height || newButtonRect.origin.x<0 || newButtonRect.origin.y<0) {
        return;
    }
    
    // disable the creation of new UIElement if it intersects with another UIElement
    if ([[self.currentSketch transitionButtons]count] > 0) {
        for (TransitionButton *tButton in [self.currentSketch transitionButtons]) {
            CGRect tRect= CGRectMake([tButton.x floatValue], [tButton.y floatValue], [tButton.width floatValue], [tButton.height floatValue]);
            if ( CGRectIntersectsRect(newButtonRect, tRect) ) {
                return;
            }
        }
    }
    
    //Falls das Rect genuegend gross ist wird der Knopf erstellt
    if (fabs(self.endPoint.x -self.startPoint.x) > 20 && fabs(self.endPoint.y - self.startPoint.y) > 20) {

        //Umrechnung der Absolutwerte in Prozentwerte um die Darstellung auf verschiedenen Devices zu beguenstigen
        float percentX = (newButtonRect.origin.x/self.frame.size.width)*100;
        float percentY= (newButtonRect.origin.y/self.frame.size.height)*100;
        float percentWidth= (newButtonRect.size.width/self.frame.size.width)*100;
        float percentHeight= (newButtonRect.size.height/self.frame.size.height)*100;
        
        CGRect percentRect = CGRectMake(percentX, percentY, percentWidth, percentHeight);
        TransitionButton *transButton= [[CoreDataManager sharedManager]createTransitionButtonWithRect:percentRect];
        [self.currentSketch addTransitionButtonsObject:transButton];
        [self.delegate transitionButtonCreated];
        [self drawAllButtons];
    }
    
    self.startPoint = CGPointMake(0, 0);
    self.endPoint = CGPointMake(0, 0);
}

#pragma mark - TOOLS



@end
