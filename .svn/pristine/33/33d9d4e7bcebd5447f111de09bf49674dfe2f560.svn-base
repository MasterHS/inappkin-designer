//
//  NDTasksViewController.m
//  iNappkinDesigner
//
//  Created by Orest on 11/06/14.
//  Copyright (c) 2014 Adesso. All rights reserved.
//

#import "NDTasksViewController.h"
#import "ADSProject.h"
#import "ADSTasksBoard.h"
#import "ADSTask.h"
#import "NDTasksTableViewCell.h"
#import "NDUIElementViewTasks.h"
#import "NDCreateTaskViewController.h"
#import "ADSAction.h"
#import "ADSButtonClick.h"
#import "ADSServerRequestHandler.h"
#import "NDHelpers.h"
#import "WebViewController.h"

@interface NDTasksViewController ()

@end

//Wenn hier Werte angepasst werden: Auch die Werte im Storyboard fürs Textfield und Textview anpassen
static NSString* placeHolderTaskTitle= @"Add New Task";


@implementation NDTasksViewController

- (void) viewDidLoad{
	[super viewDidLoad];
	self.elementView.delegate = self;
    
    [self.taskDescriptionTextView.layer setBorderColor : [[UIColor darkGrayColor] CGColor]];
	[self.taskDescriptionTextView.layer setBorderWidth : 1.0];
    self.currentTask = nil;
    [self updateTaskInfosDisplay];
    [self displayFirstSketch];
}

-(void)viewWillAppear:(BOOL)animated{
    [self autoselectAddNewTaskRow];
}

-(void)autoselectAddNewTaskRow{
    NSIndexPath *rowToSelect =[NSIndexPath indexPathForRow:0 inSection:0];
    [self.sideTableView selectRowAtIndexPath:rowToSelect animated:YES scrollPosition:UITableViewScrollPositionTop];
}


#pragma mark - Table View DataSource + Delegate
- (NSInteger) tableView : (UITableView *) tableView numberOfRowsInSection : (NSInteger) section{
	return [self.project.tasksBoard getTasksCount]+1;
}

- (CGFloat) tableView : (UITableView *) tableView heightForRowAtIndexPath : (NSIndexPath *)indexPath{
	return 100.0;
}

//Add new Task falls es die erste Zeile ist, sonst den Task Namen und Description
- (UITableViewCell *) tableView : (UITableView *) tableView cellForRowAtIndexPath : (NSIndexPath *)indexPath{
	NDTasksTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier : @"NDTasksTableViewCell"];
	if (cell == nil) {
		cell =[[NDTasksTableViewCell alloc] initWithStyle : UITableViewCellStyleDefault reuseIdentifier :@"NDTasksTableViewCell"];
	}
    if (indexPath.row == 0) {
        cell.primaryLabel.text = placeHolderTaskTitle;
        cell.primaryLabel.textColor = [UIColor blueColor];
    }
    else{
        ADSTask * task = [self.project.tasksBoard getTaskAtIndex : indexPath.row-1];
        cell.primaryLabel.text = task.name;
        cell.secondaryLabel.text = task.description;
    }
	return cell;
}

- (void) tableView : (UITableView *) tableView didSelectRowAtIndexPath : (NSIndexPath *) indexPath{
    if(indexPath.row == 0){
        self.currentTask = nil;
    }
    else {
        self.currentTask = [self.project.tasksBoard getTaskAtIndex : indexPath.row-1];
    }
    // add first sketch & buttons to imageView
	[self displayFirstSketch];
    [self updateTaskInfosDisplay];
}

#pragma mark - Bau der Anzeige von Task Infos
-(void)updateTaskInfosDisplay{
    if (![self.project.storyboard getSketchesCount]) {
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle : @"Error" message :@"No sketches added! Please add sketches before you create tasks." delegate : nil cancelButtonTitle : @"OK" otherButtonTitles : nil];
        [alert show];
        
        self.taskDetailView.hidden =YES;
        self.sideTableView.hidden = YES;
        self.toolbar.hidden = YES;
		return;
	}
    self.recordingSwitch.on = NO;
    self.resetRecordingButton.enabled = NO;
	self.toolbar.backgroundColor = [UIColor whiteColor];
    self.taskDetailView.hidden =NO;
    self.sideTableView.hidden = NO;
    self.toolbar.hidden = NO;
    
    if (self.currentTask) {
        [self showInfosofCurrentTask];
    }
    else [self showInfosofNewTask];
}

-(void)showInfosofNewTask{
    self.taskTitleTextField.text = @"";
	self.taskDescriptionTextView.text = @"";
    self.transitionCountLabel.text = @"0";
}

- (void) showInfosofCurrentTask{
	self.taskTitleTextField.text = self.currentTask.name;
	self.taskDescriptionTextView.text = self.currentTask.description;
	self.transitionCountLabel.text = @"0";
	if (self.currentTask.actions.count) {
		self.transitionCountLabel.text =[NSString stringWithFormat: @"%lu",(unsigned long) self.currentTask.actions.count];
	}
}

- (void) displayFirstSketch{
	UIImage * image = [self.project.storyboard getSketchAtIndex: 0].image;
	self.sketchDetailImage.image = image;
}

-(void)updateTableView{
    int row = [self.project.tasksBoard getTaskIndex:self.currentTask]+1;
    NSIndexPath *iPath = [NSIndexPath indexPathForRow:row inSection:0];
    [self.sideTableView reloadData];
    [self.sideTableView selectRowAtIndexPath:iPath animated:NO scrollPosition:UITableViewScrollPositionBottom];
}

# pragma mark - Task Transitions & Recording
//wird aufgerufen wenn Transitions recorded werden und man dafuer auf einen Button in der UIElementView tippt
- (void) jumpToDestionation : (NSInteger) destIndex{
	UIImage * image = [self.project.storyboard getSketchAtIndex : destIndex].image;
	self.sketchDetailImage.image = image;
	CGRect screenRect = [[UIScreen mainScreen] bounds];
	self.elementView =[self.elementView initWithFrame: screenRect withSketchIndex: destIndex inProject: self.project];
}

- (void) addStepToTask : (ADSUIElement *) element{
	if (self.recordingSwitch.on) {
		ADSButtonClick * action = [[ADSButtonClick alloc] initWithUIElementId: element.elementId project: self.project];
		[self.currentTask addAction: action ];
		self.transitionCountLabel.text = [NSString stringWithFormat : @"%lu",(unsigned long) self.currentTask.actions.count];
		self.resetRecordingButton.enabled = YES;
	}
}

- (IBAction) recordSwitchTapped : (id) sender{
    if (!self.currentTask) {
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle : @"Error" message :@"Please give this task a name before you start recording." delegate : nil cancelButtonTitle : @"OK" otherButtonTitles : nil];
        [alert show];
        self.recordingSwitch.on = NO;
        return;
    }
	if (self.recordingSwitch.on) {
		self.toolbar.backgroundColor = [UIColor redColor];
		self.toolbar.barTintColor = Nil;
        self.resetRecordingButton.enabled = YES;
        [self.elementView setHidden:NO];
        [self jumpToDestionation:0];
	}
	else {
		self.toolbar.backgroundColor = [UIColor whiteColor];
		self.toolbar.barTintColor = [UIColor whiteColor];
        self.resetRecordingButton.enabled = NO;
        [self.elementView setHidden:YES];
        [self updateTaskOnServer];
	}
}

- (IBAction) resetRecording: (id) sender{
	[self.currentTask deleteAllActions];
	[self.transitionCountLabel setText:@"0"];
	[self jumpToDestionation : 0];
}

#pragma mark - Task Loeschen
- (IBAction) DeleteTask : (id) sender{
    if (self.recordingSwitch.on == YES) {
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle : @"Error" message :@"You cannot delete a task while recording transitions. Please stop the record session first." delegate : nil cancelButtonTitle : @"OK" otherButtonTitles : nil];
        [alert show];
        return;
    }
    //wenn wir gerade einen task bearbeiten und keinen neuen erstellen koennen wir ihn loeschen
    if (self.currentTask) {
        [self.project.tasksBoard deleteTask : self.currentTask];
        
        [self.serverRequestHandler deleteTask : self.currentTask project : self.project success :^ {
            NSLog(@"Delete task success!");
        } failure :^ (NSError * error) {
            NSLog(@"Error to delete task %@", error);
        }];
        
        self.currentTask = nil;
    }
    //wir waehlen wieder die Add new task Zeile aus und updaten die Anzeige
    [self updateTableView];
    [self autoselectAddNewTaskRow];
    [self updateTaskInfosDisplay];
    
	[self viewDidLoad];
}


#pragma mark - UITextField/UITextView Delegate
- (void) textViewDidBeginEditing : (UITextView *) textView{}

- (void) textViewDidEndEditing : (UITextView *) textView{
    if (self.currentTask) {
        self.currentTask.description = textView.text;
        [self updateTaskOnServer];
    }
    [self updateTableView];
    [self.view endEditing: YES];
}

-(void) textFieldDidEndEditing:(UITextField *)textField{
    //erstmal abfragen ob es den Tasknamen schon gibt. Wenn ja Textfeld zuruecksetzen und Fehlermeldung anzeigen.
    if ([ [self.project.tasksBoard getTasksFromName:textField.text] count]>0 && ![self.currentTask.name isEqualToString:textField.text]){
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle : @"Error" message :@"There already is a task with this name. Please select a different name." delegate : nil cancelButtonTitle : @"OK" otherButtonTitles : nil];
        [alert show];
        [self.taskTitleTextField setText:@""];
        return;
    }
    //entweder vorhandene Taskdaten aendern wenn wir gerade einen task editieren
    if (self.currentTask) {
        self.currentTask.name = self.taskTitleTextField.text;
        self.currentTask.description = self.taskDescriptionTextView.text;
        [self updateTaskOnServer];
    }
    //oder neuen Task anlegen
    else{
        if (textField.text.length > 0){
            self.currentTask =
            [[ADSTask alloc] initWithName: textField.text description: self.taskDescriptionTextView.text firstSketch: nil];
            [self.project.tasksBoard addTask: self.currentTask];
            [self sendNewTaskToServer];
        }
    }
    [self updateTableView];
    [self.view endEditing: YES];
}

#pragma mark - Server Kommunikation
-(void)sendNewTaskToServer{
    [self.serverRequestHandler createTask:self.currentTask project:self.project success:^ {
        NSLog(@"Neuer Task erstellt.");
    }  failure:^ (NSError * error) {
        NSLog(@"Fehler beim Erstellen von neuem Task: %@", error);
    }];
}

-(void)updateTaskOnServer{
    [self.serverRequestHandler updateTask:self.currentTask project:self.project success:^ {
        NSLog(@"Task wurde upgedated.");
    } failure:^ (NSError * error) {
        NSLog(@"Fehler beim Updaten des Task: %@", error);
    }];
}

#pragma mark - Heatmap
- (IBAction) showTestReport : (id) sender{
    NSString * reportURL =[NSString stringWithFormat :@"http://ios14adesso-bruegge.in.tum.de:7001/data/heatmap/%@/%@.html",
                           self.project.projectId, self.currentTask.taskId];
    WebViewController *aVC = [[WebViewController alloc]initWithString:reportURL];
    
	[self presentViewController:aVC animated:YES completion:nil];
}


@end
