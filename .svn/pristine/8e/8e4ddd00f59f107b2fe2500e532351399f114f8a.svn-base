//
//  NDUIElementViewTasks.m
//  iNappkinDesigner
//
//  Created by Luc Gaasch on 26/06/14.
//  Copyright (c) 2014 Adesso. All rights reserved.
//

#import "NDUIElementViewTasks.h"
#import "NDTasksViewController.h"
#import "ADSProject.h"
#import "ADSStoryboard.h"
#import "ADSSketch.h"
#import "ADSButton.h"
#import "ADSTransition.h"

@interface NDUIElementViewTasks ()
@end

@implementation NDUIElementViewTasks

const int invalidSketchIndex = -1;
CGContextRef context;

- (id) initWithFrame: (CGRect)frame withSketchIndex: (NSInteger)index inProject: (ADSProject *)project{
	self = [super initWithFrame : [[UIScreen mainScreen] bounds]];
	if (self) {
		self.project = project;
        
		// Set background color
		self.backgroundColor = [UIColor clearColor];
		self.selectedSketchIndex = index;
		if (index > invalidSketchIndex) {
			self.selectedSketch =
            [[self.project storyboard]getSketchAtIndex : self.selectedSketchIndex];
		}
		else {
			self.selectedSketch = nil;
		}
		[self setNeedsDisplay];
	}
	return self;
}

- (void) touchesBegan : (NSSet *) touches withEvent : (UIEvent *) event{
    [super touchesBegan:touches withEvent:event];
    
	UITouch * touch = [[event allTouches] anyObject];
	self.startPoint = [touch locationInView : touch.view];
	// Get the specific point that was touched
	CGPoint point = self.startPoint;
    
	// See if the point touched is within these rectangular bounds
	if (self.selectedSketch != nil) {
		if ([self.selectedSketch elementsCount] > 0) {
			for (int i = 0; i < [self.selectedSketch elementsCount]; i++) {
				ADSButton * element = (ADSButton *) [self.selectedSketch elementAtIndex : i];
				// Tester Mode is Enabled
                
				// detect if user touch is inside the UIElement -> jump to destination Sketch
				if ( CGRectContainsPoint(element.location, point) ) {
					ADSSketch * destinationSketch = [[element transition]destinationSketch];
					NSInteger sketchIndex =
                    [[self.project storyboard]getSketchIndex : destinationSketch];
					[self.delegate jumpToDestionation : sketchIndex]; // byLuc
					[self.delegate addStepToTask : element];
					break;
				}
			}
		}
	}
}

#pragma mark - Adding Buttons
- (void) drawRect : (CGRect) rect{
	[super drawRect : [[UIScreen mainScreen] bounds]];
	context = UIGraphicsGetCurrentContext();
	[self drawButton];
}

- (void) drawButton{
	// Set the rectangle outerline-colour
	CGContextSetStrokeColorWithColor(context, [UIColor blueColor].CGColor);
    
	// set the rectangle fill color
	UIColor * color = [UIColor colorWithRed : 0.041 green : 0.375 blue : 0.998 alpha : 0.4];
    
	if (self.selectedSketch != nil) {
		for (ADSUIElement * button in self.selectedSketch.elements) {
			CGContextSaveGState(context);{
				CGContextSetLineWidth(context, 1.0);
				[color setFill];
				CGContextBeginPath(context);
				CGContextAddRect(context, button.location);
				CGContextStrokePath(context);
                
				CGContextStrokeRect(context, button.location);
				UIRectFill(button.location);
			}
			CGContextRestoreGState(context);
		}
	}
}

- (void) refereshUIElements
{
	[self setNeedsDisplay];
}

@end
