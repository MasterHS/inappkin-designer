//
//  NDTaskSuccessViewController.h
//  iNappkinDesigner
//
//  Created by Safetli, Hazem on 29/12/14.
//  Copyright (c) 2014 Adesso. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "CorePlot-CocoaTouch.h"
#import "Task.h"
#import "Project.h"

@interface NDTaskSuccessViewController : UIViewController<CPTBarPlotDataSource,CPTBarPlotDelegate>

@property (nonatomic, retain) NSMutableArray *data;
@property (nonatomic, retain) CPTGraphHostingView *hostingView;
@property (nonatomic, retain) CPTXYGraph *graph;
@property (weak, nonatomic) IBOutlet UIView *statsView;
@property (nonatomic, retain) Project* currentProject;
@property  NSInteger TabBarHeight;
- (void) generateBarPlot;
@end
