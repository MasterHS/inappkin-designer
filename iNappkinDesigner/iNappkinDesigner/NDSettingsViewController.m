//
//  NDSettingsViewController.m
//  iNappkinDesigner
//
//  Created by Safetli, Hazem on 31/10/14.
//  Copyright (c) 2014 Adesso. All rights reserved.
//

#import "NDSettingsViewController.h"
#import "SlimServerRequestHandler.h"
#import "NDHelpers.h"
#import "CoreDataManager.h"

@interface NDSettingsViewController ()

@end

@implementation NDSettingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackTranslucent;
    self.serverTextField.text = [NDHelpers getBaseServerName];
}

-(BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
 if(sender==self.saveSettingsButton)
 {
     [self saveAndBack];
 }
    
    return YES;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)testAddressInput:(id)sender{
    SlimServerRequestHandler *serverRequesthandler = [[SlimServerRequestHandler alloc]initWithBaseURLStringForGeneralFunctionality:self.serverTextField.text];
    [serverRequesthandler testServerURL];
}

-(void)saveAndBack{
    [[NSUserDefaults standardUserDefaults] setObject:self.serverTextField.text forKey:@"baseServerName"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(IBAction)resetToFactoryDefaults:(id)sender{
    self.serverTextField.text = BaseURLString;
    [[NSUserDefaults standardUserDefaults] setObject:BaseURLString forKey:@"baseServerName"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

//-(IBAction)resetLocalDataStorage:(id)sender{
//    [[CoreDataManager sharedManager]resetDatastore];
//}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/



@end
