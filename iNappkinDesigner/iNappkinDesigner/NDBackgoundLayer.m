//
//  NDBackgoundLayer.m
//  iNappkinDesigner
//
//  Created by Fei Pan on 05/06/14.
//  Copyright (c) 2014 Adesso. All rights reserved.
//

#import "NDBackgoundLayer.h"

@implementation NDBackgroundLayer


// Metallic grey gradient background
+ (CAGradientLayer *) greyGradient
{

	UIColor * colorOne = [UIColor colorWithWhite : 0.9 alpha : 1.0];
	UIColor * colorTwo =
		[UIColor colorWithHue : 0.625 saturation : 0.0 brightness : 0.85 alpha : 1.0];
	UIColor * colorThree     =
		[UIColor colorWithHue : 0.625 saturation : 0.0 brightness : 0.7 alpha : 1.0];
	UIColor * colorFour =
		[UIColor colorWithHue : 0.625 saturation : 0.0 brightness : 0.4 alpha : 1.0];

	NSArray * colors =
		[NSArray arrayWithObjects : (id) colorOne.CGColor, colorTwo.CGColor, colorThree.CGColor,
		 colorFour.CGColor, nil];

	NSNumber * stopOne = [NSNumber numberWithFloat : 0.0];
	NSNumber * stopTwo = [NSNumber numberWithFloat : 0.02];
	NSNumber * stopThree     = [NSNumber numberWithFloat : 0.99];
	NSNumber * stopFour = [NSNumber numberWithFloat : 1.0];

	NSArray * locations = [NSArray arrayWithObjects : stopOne, stopTwo, stopThree, stopFour, nil];
	CAGradientLayer * headerLayer = [CAGradientLayer layer];
	headerLayer.colors = colors;
	headerLayer.locations = locations;

	return headerLayer;

}

// Blue gradient background
+ (CAGradientLayer *) blueGradient
{


	UIColor * colorOne =
		[UIColor colorWithRed : 240 / 255.0 green : 240 / 255.0 blue : 240 / 255.0 alpha : 1.0];
	UIColor * colorTwo =
		[UIColor colorWithRed : (207 / 255.0)  green : (207 / 255.0)  blue : (207 /
																			  255.0)  alpha : 1.0];

	NSArray * colors = [NSArray arrayWithObjects : (id) colorOne.CGColor, colorTwo.CGColor, nil];
	NSNumber * stopOne = [NSNumber numberWithFloat : 0.6];
	NSNumber * stopTwo = [NSNumber numberWithFloat : 1.0];

	NSArray * locations = [NSArray arrayWithObjects : stopOne, stopTwo, nil];

	CAGradientLayer * headerLayer = [CAGradientLayer layer];
	headerLayer.colors = colors;
	headerLayer.locations = locations;

	return headerLayer;

}

@end
