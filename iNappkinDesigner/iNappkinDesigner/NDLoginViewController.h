
//
//  NDLoginViewController.h
//  iNappkinDesigner
//
//  Created by Safetli, Hazem on 01/11/14.
//  Copyright (c) 2014 Adesso. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SlimServerRequestHandler.h"

@class  NDProjectsViewController;

@interface NDLoginViewController : UIViewController<UIPopoverControllerDelegate,ServerRequestsDelegate>

@property (strong, nonatomic) IBOutlet UIView *loginView;

@property (weak, nonatomic) IBOutlet UITextField *userNameTextfield;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextfield;
@property (weak, nonatomic) IBOutlet UIButton *registerButton;
@property (weak, nonatomic) IBOutlet UIButton *loginButton;
@property (strong,nonatomic) UIPopoverController* popover;
@property (weak,nonatomic) id delegate;

@end
