//
//  ServerSettings.h
//  iNappkinDesigner
//
//  Created by Julian-Lennart Weigand on 20.08.14.
//  Copyright (c) 2014 Adesso. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface ServerSettings : NSManagedObject

@property (nonatomic, retain) NSString * hostName;

@end
