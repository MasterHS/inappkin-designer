//
//  NDLoginViewController.m
//  iNappkinDesigner
//
//  Created by Safetli, Hazem on 01/11/14.
//  Copyright (c) 2014 Adesso. All rights reserved.
//

#import "NDLoginViewController.h"

#import "CoreDataManager.h"
#import "NDHelpers.h"
#import "User.h"
#import "NDProjectsViewController.h"


@interface NDLoginViewController ()
//sdsd
@end

@implementation NDLoginViewController
bool userAuthenticated=false;
- (void)viewDidLoad {
    //[super viewDidLoad];
    	self.navigationController.navigationBar.barStyle = UIBarStyleBlackTranslucent;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    //[super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)registerUser:(id)sender {
    if([self.userNameTextfield.text isEqual:@""] || [self.passwordTextfield.text isEqual:@""])
    {
        NSLog(@"please fill both textboxes");
        return;
        
    }
    SlimServerRequestHandler *requestHandler= [[SlimServerRequestHandler alloc] initWithBaseURLStringForGeneralFunctionality:[NDHelpers getBaseServerName]];
    requestHandler.delegate=self;
    [requestHandler createUser:self.userNameTextfield.text withPassword:self.passwordTextfield.text];
    
    
}
-(void)createResponse:(NSDictionary *) responseObject
{
    if(responseObject==nil)
    {
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle : @"Register Error" message : @"User already registered!"                                                                                                              delegate : nil                                                                                                          cancelButtonTitle : @"OK"                                                                                                          otherButtonTitles : nil];
            [alert show];
        NSLog(@"User already registered");
        return;
    }
    if([responseObject objectForKey:@"error"])
    {
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle : @"Register Error" message : [responseObject objectForKey:@"error"]                                                                                                              delegate : nil                                                                                                          cancelButtonTitle : @"OK"                                                                                                          otherButtonTitles : nil];
        [alert show];
        NSLog(@"%@",[responseObject objectForKey:@"error"]);
        return;
    }
    if([responseObject count]==4)
    {
        
        NSString* userName=[[CoreDataManager sharedManager]addUser:[responseObject objectForKey:@"userName"] password:[responseObject objectForKey:@"password"] withId:[responseObject objectForKey:@"_id"]];
        [NDHelpers setAppUser:userName];
        NSLog(@"user created!");
        [self.delegate changeLoginButtonName: [NSString stringWithFormat:@"%@%@",userName,@" (signout)"]];
        [self.popover dismissPopoverAnimated:YES];
        return;
    }
    else
    {
        NSLog(@"error");
    }
    
}

-(void)loginResponse:(NSDictionary *) responseObject
{
    if(responseObject==nil)
    {
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle : @"Login Error" message : @"User is not registerd yet"                                                                                                             delegate : nil                                                                                                          cancelButtonTitle : @"OK"                                                                                                          otherButtonTitles : nil];
        [alert show];
        NSLog(@"User is not registerd");
        return;
    }
    if([responseObject count]==0)
    {
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle : @"Login Error" message : @"User is not registerd yet"                                                                                                             delegate : nil                                                                                                          cancelButtonTitle : @"OK"                                                                                                          otherButtonTitles : nil];
        [alert show];
        NSLog(@"User is not registerd");
        [self.popover dismissPopoverAnimated:YES];
        return;
    }
    
    if([[responseObject objectForKey:@"userName"] isEqualToString:self.userNameTextfield.text])
    {
        if([[responseObject objectForKey:@"password"] isEqualToString:self.passwordTextfield.text])
        {
            NSLog(@"user logged in");
            
            NSString* userName=[[CoreDataManager sharedManager]addUser:[responseObject objectForKey:@"userName"] password:[responseObject objectForKey:@"password"] withId:[responseObject objectForKey:@"_id"]];
            [NDHelpers setAppUser:userName];
            [self.delegate changeLoginButtonName: [NSString stringWithFormat:@"%@%@",userName,@" (signout)"]];
            [self.popover dismissPopoverAnimated:YES];
        }
        else
        {
            UIAlertView * alert = [[UIAlertView alloc] initWithTitle : @"Login Error" message : @"Password or username is not correct!"                                                                                                             delegate : nil                                                                                                          cancelButtonTitle : @"OK"                                                                                                          otherButtonTitles : nil];
            [alert show];
            NSLog(@"Password or username is not correct!");
        }
    }
    
}

-(void)loginFailed:(NSError *)error
{
    NSLog(@"%@", error.description);
    userAuthenticated=false;
}
- (IBAction)loginUser:(id)sender {
    if([self.userNameTextfield.text isEqual:@""] || [self.passwordTextfield.text isEqual:@""])
    {
        NSLog(@"please fill both textboxes");
        return;
        
    }
    SlimServerRequestHandler *requestHandler= [[SlimServerRequestHandler alloc] initWithBaseURLStringForGeneralFunctionality:[NDHelpers getBaseServerName]];
    requestHandler.delegate=self;
    [requestHandler getUser:self.userNameTextfield.text];
}

-(BOOL)popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController
{
    return NO;
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
