//
//  NDStatisticsViewController.h
//  iNappkinDesigner
//
//  Created by Safetli, Hazem on 29/12/14.
//  Copyright (c) 2014 Adesso. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Project.h"
@interface NDStatisticsViewController : UITabBarController

@property (strong, nonatomic) Project * currentProject;
@end
