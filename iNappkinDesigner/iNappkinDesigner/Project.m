//
//  Project.m
//  iNappkinDesigner
//
//  Created by Julian-Lennart Weigand on 20.08.14.
//  Copyright (c) 2014 Adesso. All rights reserved.
//

#import "Project.h"
#import "Sketch.h"
#import "Task.h"
#import "CoreDataManager.h"

@implementation Project

@dynamic deviceType;
@dynamic projectName;
@dynamic projectDescription;
@dynamic userName;
@dynamic sync_status;
@dynamic is_deleted;
@dynamic guid;
@dynamic last_modified;
@dynamic deviceId;


@end
