//
//  NDTransitionButtonOverlayViewController.m
//  iNappkinDesigner
//
//  Created by Julian-Lennart Weigand on 18.08.14.
//  Copyright (c) 2014 Adesso. All rights reserved.
//

#import "NDTransitionButtonOverlayView.h"
#import "CoreDataManager.h"
#import "NDHelpers.h"
#import "NDSketchViewController.h"
#import "SlimServerRequestHandler.h"

@interface NDTransitionButtonOverlayView ()

@end

@implementation NDTransitionButtonOverlayView

-(void)setNewActiveSketch:(Sketch*)sketch{
    self.currentSketch=sketch;
    [self drawAllButtons];
}
CGRect *frameSize;
- (void) drawAllButtons{
    //erstmal alle Buttons von der View entfernen
    // first remove all the buttons from the view
    for (UIButton *TButton in [self subviews]) {
        [TButton removeFromSuperview];
    }
    NSInteger counter=0;
    NSArray* transitionButtons=[[CoreDataManager sharedManager]getTransitionButtons:self.currentSketch.guid];
    for (TransitionButton *tButton in  transitionButtons) {
        
        NSLog(@"frame w/h: %f,%f",self.frame.size.width,self.frame.size.height);
        float absolutX= ([tButton.xAxis floatValue]*self.frame.size.width)/100;
        float absolutY= ([tButton.yAxis floatValue]*self.frame.size.height)/100;
        float absolutWidth = ([tButton.width floatValue]/100)*self.frame.size.width;
        float absolutHeight = ([tButton.height floatValue]/100)*self.frame.size.height;
        CGRect absolutRect= CGRectMake(absolutX, absolutY, absolutWidth, absolutHeight);
        
//        float absolutX= ([tButton.x floatValue]/100)*self.frame.size.width;
//        float absolutY= ([tButton.y floatValue]/100)*self.frame.size.height;
//        float absolutWidth = ([tButton.width floatValue]/100)*self.frame.size.width;
//        float absolutHeight = ([tButton.height floatValue]/100)*self.frame.size.height;
//        CGRect absolutRect= CGRectMake(absolutX, absolutY, absolutWidth, absolutHeight);
        
       UIButton *rButton = [[UIButton alloc]initWithFrame:absolutRect];

        rButton.backgroundColor = [NDHelpers blueButtonColor];
        [rButton addTarget:self.delegate action:@selector(transitionButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        rButton.tag= counter;
        counter++;
        [self addSubview:rButton];
    }
}

-(void)setAllButtonBackgroundcolorsToBlue{
    for (UIView *tView in [self subviews]) {
        if ([tView isKindOfClass:[UIButton class]]) {
            tView.backgroundColor = [NDHelpers blueButtonColor];
        }
    }
}

#pragma mark - View Delegate
- (void) touchesBegan : (NSSet *) touches withEvent : (UIEvent *) event{
    [super touchesBegan:touches withEvent:event];
	UITouch * touch = [[event allTouches] anyObject];
	self.startPoint = [touch locationInView: touch.view];
    
    self.buttonSizeIndicatorLabel= [[UILabel alloc]initWithFrame: CGRectMake(self.startPoint.x, self.startPoint.y, 5.0f, 5.0f)];
    [self.buttonSizeIndicatorLabel setBackgroundColor:[NDHelpers purpleButtonColor]];
    [self addSubview:self.buttonSizeIndicatorLabel];
}

- (void) touchesMoved : (NSSet *) touches withEvent : (UIEvent *) event{
	for (UITouch * touch in touches) { // Get location of Touch
		CGPoint location = [touch locationInView : self];
		self.endPoint = location;
	}
    
    CGRect indicatorLabelFrame = CGRectMake(MIN(self.startPoint.x, self.endPoint.x),
                                            MIN(self.startPoint.y, self.endPoint.y),
                                            fabs(self.startPoint.x - self.endPoint.x),
                                            fabs(self.startPoint.y - self.endPoint.y));
    [self.buttonSizeIndicatorLabel setFrame:indicatorLabelFrame];
}

- (void) touchesEnded : (NSSet *) touches withEvent : (UIEvent *) event{
    if ([self.delegate enableTest].on) {
        return;
    }
    
    UITouch * touch = [[event allTouches] anyObject];
    self.endPoint = [touch locationInView : self];
    
    CGRect newButtonRect = CGRectMake(MIN(self.startPoint.x, self.endPoint.x),
                                      MIN(self.startPoint.y, self.endPoint.y),
                                      fabs(self.startPoint.x - self.endPoint.x),
                                      fabs(self.startPoint.y - self.endPoint.y));
    
    self.startPoint = CGPointMake(0, 0);
    self.endPoint = CGPointMake(0, 0);
    [self.buttonSizeIndicatorLabel removeFromSuperview];
    self.buttonSizeIndicatorLabel = nil;
    
    //Anpassungen des Buttonframes damit er immer in der View liegt und nicht darueber hinaus geht
    if (newButtonRect.origin.y<0.0) {
        newButtonRect.size.height= newButtonRect.origin.y+newButtonRect.size.height;
        newButtonRect.origin.y=0.0f;
    }
    if (newButtonRect.origin.x<0.0f) {
        newButtonRect.size.width= newButtonRect.origin.x+newButtonRect.size.width;
        newButtonRect.origin.x = 0.0f;
    }
    if (newButtonRect.origin.x+newButtonRect.size.width>self.frame.size.width) {
        newButtonRect.size.width= self.frame.size.width-newButtonRect.origin.x;
    }
    if (newButtonRect.origin.y+newButtonRect.size.height>self.frame.size.height) {
        newButtonRect.size.height= self.frame.size.height-newButtonRect.origin.y;
    }
    
    
    //Umrechnung der Absolutwerte in Prozentwerte um die Darstellung auf verschiedenen Devices zu beguenstigen
    // To favor conversion of the absolute values ​​in percent values ​​to display on different devices
    CGFloat percentX = (newButtonRect.origin.x/self.frame.size.width)*100;
    CGFloat percentY= (newButtonRect.origin.y/self.frame.size.height)*100;
    CGFloat percentWidth= (newButtonRect.size.width/self.frame.size.width)*100;
    CGFloat percentHeight= (newButtonRect.size.height/self.frame.size.height)*100;
    CGRect percentRect = CGRectMake(percentX, percentY, percentWidth, percentHeight);
    
    // Falls der Button sich mit einem anderen ueberschneiden wuerde erstellen wir ihn nicht
    // If the button itself would with another by cut we do not create it
    NSArray* transitionButtons=[[CoreDataManager sharedManager]getTransitionButtons:self.currentSketch.guid];
    if ([transitionButtons count] > 0) {
        for (TransitionButton *tButton in  transitionButtons) {
            CGRect tRect= CGRectMake([tButton.xAxis floatValue], [tButton.yAxis floatValue], [tButton.width floatValue], [tButton.height floatValue]);
            if ( CGRectIntersectsRect(percentRect, tRect) ) {
                return;
            }
        }
    }
    
    //Falls das Rect genuegend gross ist wird der Knopf erstellt
    if (newButtonRect.size.width > 20 && newButtonRect.size.height > 20) {
        

        NSDictionary* buttonData=@{@"xAxis":[NSNumber numberWithFloat:percentX],@"yAxis":[NSNumber numberWithFloat:percentY],@"width":[NSNumber numberWithFloat:percentWidth],
                                                                                              @"height":[NSNumber numberWithFloat:percentHeight],@"destinationSketchGuid":@""};
        [[CoreDataManager sharedManager]createOrModifyTransitionButton:buttonData transButtonGuid:nil sketchGuid:self.currentSketch.guid syncStatus:@"1" deleted:@"0"];


        [self drawAllButtons];
    }
}

#pragma mark - TOOLS



@end
