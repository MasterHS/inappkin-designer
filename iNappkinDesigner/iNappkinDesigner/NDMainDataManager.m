//
//  NDSingletonSafe.m
//  iNappkinDesigner
//
//  Created by Julian-Lennart Weigand on 14.08.14.
//  Copyright (c) 2014 Adesso. All rights reserved.
//

#import "NDMainDataManager.h"
#import "Project.h"
#import "ADSServerRequestHandler.h"
#import "NDHelpers.h"
#import "CoreDataManager.h"
#import <CoreData/CoreData.h>

@implementation NDMainDataManager


int indexOfCurrentProject;
NSMutableArray *allProjects;
ADSServerRequestHandler *serverRequestHandler;

static NDMainDataManager *sharedDataManager = nil;

+(NDMainDataManager *)sharedMainDataManager{
	@synchronized(self) {
		if (sharedDataManager == nil) {
			sharedDataManager= [[self alloc] init];
            serverRequestHandler = [NDHelpers generateServerRequestHandler];
		}
	}
	return sharedDataManager;
}

//prüft ob ein Projektname vergeben werden kann: Gibt YES zurueck falls der Name passt und NO falls er nicht passt
+(BOOL) checkNameValidityWithProjectName:(NSString*)projektName{
    if ([projektName isEqualToString:@""] || projektName == nil) {
        return NO;
    }
    NSManagedObjectContext *managedObjectContext = [[CoreDataManager sharedManager] managedObjectContext];
	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
	
	NSEntityDescription *entity = [NSEntityDescription entityForName:@"Project" inManagedObjectContext:managedObjectContext];
	[fetchRequest setEntity:entity];
	
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObjects: sortDescriptor, nil];
   	[fetchRequest setSortDescriptors:sortDescriptors];
	NSFetchedResultsController *fetchedExhibitorController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:managedObjectContext sectionNameKeyPath:nil cacheName:nil];
	[fetchedExhibitorController performFetch:nil];
    NSArray *currentProjects =  [fetchedExhibitorController fetchedObjects];
    
    for (Project *tProject in currentProjects) {
        if ([tProject.name isEqualToString: projektName]) {
            return NO;
        }
    }
    return YES;
}


@end
