//
//  NDLandscapeImagePickerController.h
//  iNappkinDesigner
//
//  Created by Fei Pan on 04/06/14.
//  Copyright (c) 2014 Adesso. All rights reserved.
//

/*
 Erlaubt Auswahl von Photos aus der Camera Roll.
 Delegate ist im NDSketchViewCointroller.
 */

@import UIKit;

@interface NDLandscapeImagePickerController : UIImagePickerController

@end
