//
//  NDPropertiesViewController.h
//  iNappkinDesigner
//
//  Created by Orest on 05/06/14.
//  Copyright (c) 2014 Adesso. All rights reserved.
//

/*
 Wird im NDProjectController angezeigt.
 Mit dem VC koennen die Properties eines Projekts (name, description) bearbeitet werden.
 Das Projekt kann geloescht werden.
 
 */

@import UIKit;
@class Project;

@interface NDPropertiesViewController : UIViewController <UICollectionViewDelegate>

@property (weak, nonatomic) IBOutlet UITextField * projectName;
@property (weak, nonatomic) IBOutlet UITextField * projectDescription;
@property (weak, nonatomic) IBOutlet UITextField * deviceType;
@property (weak, nonatomic) IBOutlet UITextField *projectOwnerTextField;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *deleteProjectButton;

@property Project *currentProject;


@end
