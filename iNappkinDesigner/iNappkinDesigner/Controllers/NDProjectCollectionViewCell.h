//
//  NDProjectView.h
//  iNappkinDesigner
//
//  Created by Fei Pan on 03/06/14.
//  Copyright (c) 2014 Adesso. All rights reserved.
//

/*
 Wird im NDSketchViewController verwendet.
 Repraesentiert einen Sketch in der linken Asuwahlleiste der View.
 */

@import UIKit;
@class Sketch;

@interface NDProjectCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView * projectSidebarThumbnail;
@property Sketch *sketch;

@end
