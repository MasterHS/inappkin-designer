//
//  NDTasksViewController.h
//  iNappkinDesigner
//
//  Created by Orest on 11/06/14.
//  Copyright (c) 2014 Adesso. All rights reserved.
//

/*
 Wird im NDProjectController verwendet.
 Wird verwendet um Tasks aufzuzeichnen oder aufgezeichnete anzuzeigen.
 
 
 ##das Recording (alles ab Pragma mark Task Transition & Recording) sollte man nochmal schritt fuer Schritt durchgehen. Wenn es eine andere Datenbasis gibt sollte man das evtl ganz umschreiben.
 */

@import UIKit;
@class Project;
@class ADSServerRequestHandler;
#import "Task.h"
@class NDUIElementViewTasks;

#import "TransitionButton.h"
#import "NDCreateTaskViewController.h"
#import <MapKit/MapKit.h>



@interface NDTasksViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, UITextViewDelegate, UITextFieldDelegate,SaveTaskDelegate>

// TODO: Remove these properties after making the segue setters more specific...
@property (nonatomic, strong) Project * currentProject;
@property (nonatomic, retain) Task * currentTask;
@property (nonatomic, retain) Sketch * currentShownSketch;
@property (strong, nonatomic) ADSServerRequestHandler * serverRequestHandler;

//die View, die alle DetailInfos zum Task beinhaltet (TV, Labels, Textfields etc)
@property (nonatomic, retain) IBOutlet UIView * taskDetailView;

@property (weak, nonatomic) IBOutlet UIButton *nextSketchButton;
@property (weak, nonatomic) IBOutlet UIButton *previousSketchButton;

@property (weak, nonatomic) IBOutlet UISwitch *heatmapSwitch;
@property (nonatomic, retain) IBOutlet UITableView *sideTableView;
@property (nonatomic, retain) IBOutlet UILabel * transitionCountLabel;
@property (weak, nonatomic) IBOutlet UIImageView * currentSketchView;
//Das ist die View mit der die Buttons fuer das Transitionrecording angezeigt werden
@property (strong, nonatomic) IBOutlet NDUIElementViewTasks * elementView;

@property (weak, nonatomic) IBOutlet UIToolbar * toolbar;
@property (weak, nonatomic) IBOutlet UISwitch * recordingSwitch;
- (IBAction)resetStat:(id)sender;

@property (weak, nonatomic) IBOutlet UIBarButtonItem * resetStatsButton;
//@property (nonatomic, weak) IBOutlet MKMapView *mapView;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *addTaskButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *deleteTaskButton;


- (void) jumpToDestionation : (NSInteger) destIndex;
- (void) addStepToTask : (TransitionButton *) element;
- (IBAction) recordSwitchTapped : (id) sender;
- (IBAction) resetRecording : (id) sender;
@end
