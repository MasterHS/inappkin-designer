//
//  NDProjectViewController.h
//  iNappkinDesigner
//
//  Created by Dan on 20/5/14.
//  Copyright (c) 2014 Adesso. All rights reserved.
//

/*
 Uebersicht zu einem Projekt. ISt im Prinzip eine Containerklasse mit Seitenleiste und Platz für einen weiteren Viewcontroller.
 
 Aufbau ist:
 Die Seitenleiste ist die Hauptview.
 Ueber Interaktion mit der Seitenleiste wird die passende Unterview hinzugefuegt. (Sketches/Tasks/Properties)
 
 */

@import UIKit;
@class Project;
@class ADSServerRequestHandler;
@class NDProjectViewController;
@class ADSFileHandler;
#import "NDProjectSideBarViewControllerDelegate.h"

@protocol NDProjectControllerDelegate <NSObject>
- (void) NDProjectControllerDidDone : (NDProjectViewController *) projectController;
@end

@interface NDProjectViewController : UIViewController <NDProjectSideBarViewControllerDelegate,
UIImagePickerControllerDelegate, UINavigationControllerDelegate>


@property (nonatomic, strong) UIImage * image;
@property (nonatomic, weak) id <NDProjectControllerDelegate> delegate;
@property (weak, nonatomic) IBOutlet UILabel * descriptionLabel;
@property (weak, nonatomic) IBOutlet UIView * sideTabBar;
@property (weak, nonatomic) IBOutlet UIView * contentView;

@property IBOutlet UIView *updateIndicatorView;

@property Project *currentProject;

- (void) changeNavigationItemTo : (NSString *) string;

@end
