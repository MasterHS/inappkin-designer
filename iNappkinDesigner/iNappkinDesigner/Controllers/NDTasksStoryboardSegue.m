//
//  NDTaskStoryboardSegue.m
//  iNappkinDesigner
//
//  Created by Fei Pan on 15/06/14.
//  Copyright (c) 2014 Adesso. All rights reserved.
//

#import "NDTasksStoryboardSegue.h"
#import "NDTasksViewController.h"
#import "NDProjectSideBarViewController.h"

@implementation NDTasksStoryboardSegue

- (void) perform{
	NDProjectSideBarViewController * src =(NDProjectSideBarViewController *) self.sourceViewController;
	NDTasksViewController * dst = (NDTasksViewController *) self.destinationViewController;
	[src.delegate didSelectViewController : dst selectedIndex : 1];
}

@end
