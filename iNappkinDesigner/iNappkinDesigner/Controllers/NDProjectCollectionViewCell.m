//
//  NDProjectView.m
//  iNappkinDesigner
//
//  Created by Fei Pan on 03/06/14.
//  Copyright (c) 2014 Adesso. All rights reserved.
//

#import "NDProjectCollectionViewCell.h"
#import "NDHelpers.h"
@implementation NDProjectCollectionViewCell

-(void)setSelected:(BOOL)selected
{
    if(selected)
    {
        
        self.alpha = 1.0;
        self.backgroundColor = [UIColor redColor];
        
        
    }
    else
    {
        
        self.alpha = 0.6;
        self.backgroundColor = [UIColor clearColor];
    }
}
@end
