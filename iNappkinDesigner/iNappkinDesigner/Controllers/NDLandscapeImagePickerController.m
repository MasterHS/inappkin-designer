//
//  NDLandscapeImagePickerController.m
//  iNappkinDesigner
//
//  Created by Fei Pan on 04/06/14.
//  Copyright (c) 2014 Adesso. All rights reserved.
//

#import "NDLandscapeImagePickerController.h"

@implementation NDLandscapeImagePickerController

- (NSUInteger) supportedInterfaceOrientations
{
	return UIInterfaceOrientationMaskLandscape;
}

@end
