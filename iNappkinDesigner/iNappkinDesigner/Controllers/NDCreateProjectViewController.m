//
//  NDCreateProjectController.m
//  iNappkinDesigner
//
//  Created by Luc Gaasch on 03/06/14.
//  Copyright (c) 2014 Adesso. All rights reserved.
//

#import "NDCreateProjectViewController.h"
#import "NDProjectsViewController.h"
#import "NDHelpers.h"
#import "CoreDataManager.h"
#import "SlimServerRequestHandler.h"
#import "Project.h"


@interface NDCreateProjectViewController ()


@end

@implementation NDCreateProjectViewController
NSMutableArray* buttons;
NSString* deviceType;
- (void) viewDidLoad{
    [super viewDidLoad];
    [self prepareButtons];
    
    
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackTranslucent;
}

-(void)prepareButtons
{
    self.projectName.leftViewMode = UITextFieldViewModeAlways;
    self.projectName.leftView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Edit-26.png"]];
    self.projectDescription.leftViewMode = UITextFieldViewModeAlways;
    self.projectDescription.leftView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Edit-26.png"]];
    deviceType=@"";
    
    buttons=[[NSMutableArray alloc]init];
    UIImage *uncheckedImage = [UIImage imageNamed: @"ipad_mini.png"];
    UIImage *checkedImage = [UIImage imageNamed: @"ipad_mini_clicked.png"];
    [self.ipadMiniButton setImage:uncheckedImage forState:UIControlStateNormal];
    [self.ipadMiniButton setImage:checkedImage forState:UIControlStateSelected];
    
    uncheckedImage = [UIImage imageNamed: @"ipad_retina_air.png"];
    checkedImage = [UIImage imageNamed: @"ipad_retina_air_clicked.png"];
    [self.ipadRetinaButton setImage:uncheckedImage forState:UIControlStateNormal];
    [self.ipadRetinaButton setImage:checkedImage forState:UIControlStateSelected];
    
    uncheckedImage = [UIImage imageNamed: @"iphone5_5s_5c.png"];
    checkedImage = [UIImage imageNamed: @"iphone5_5s_5c_clicked.png"];
    [self.iphone5Button setImage:uncheckedImage forState:UIControlStateNormal];
    [self.iphone5Button setImage:checkedImage forState:UIControlStateSelected];
    
    uncheckedImage = [UIImage imageNamed: @"iphone6.png"];
    checkedImage = [UIImage imageNamed: @"iphone6_clicked.png"];
    [self.iphone6Button setImage:uncheckedImage forState:UIControlStateNormal];
    [self.iphone6Button setImage:checkedImage forState:UIControlStateSelected];
    
    uncheckedImage = [UIImage imageNamed: @"iphone4_4s.png"];
    checkedImage = [UIImage imageNamed: @"iphone4_4s_clicked.png"];
    [self.iphone4Button setImage:uncheckedImage forState:UIControlStateNormal];
    [self.iphone4Button setImage:checkedImage forState:UIControlStateSelected];
    
    uncheckedImage = [UIImage imageNamed: @"iphone6plus.png"];
    checkedImage = [UIImage imageNamed: @"iphone6plus_clicked.png"];
    [self.iphone6plusButton setImage:uncheckedImage forState:UIControlStateNormal];
    [self.iphone6plusButton setImage:checkedImage forState:UIControlStateSelected];
    
    [buttons addObject:self.iphone4Button];
    [buttons addObject:self.iphone5Button];
    [buttons addObject:self.iphone6Button];
    [buttons addObject:self.iphone6plusButton];
    [buttons addObject:self.ipadMiniButton];
    [buttons addObject:self.ipadRetinaButton];
}

#pragma mark - Create Project
- (BOOL) shouldPerformSegueWithIdentifier: (NSString *) identifier sender: (id) sender{
    if (sender == self.createButton) {
        if ([deviceType isEqualToString:@""]) {
            UIAlertView * alert = [[UIAlertView alloc] initWithTitle : @"Error" message :@"Please choose the deivce model"delegate : nil cancelButtonTitle : @"OK" otherButtonTitles : nil];
            
            [alert show];
                        return NO;
        }
        if (![NDHelpers checkNameValidityWithProjectName:self.projectName.text]) {
            UIAlertView * alert = [[UIAlertView alloc] initWithTitle : @"Error" message :@"Project name already exists or is invalid. Please change it."delegate : nil cancelButtonTitle : @"OK" otherButtonTitles : nil];
            [alert show];
            return NO;
        }
        else{
            //create project
            NSDictionary *data=@{@"projectName":self.projectName.text,@"projectDescription":self.projectDescription.text,
                                 @"deviceType":deviceType,@"userName":[NDHelpers getAppUser]};
            [[CoreDataManager sharedManager]createOrModifyProject:data projectGuid:nil syncStatus:@"1" deleted:@"0"];
            
        }
    }
    [self dismissViewControllerAnimated:YES completion:nil];
    return YES;
}



- (IBAction)buttonClicked:(id)sender {
    if(sender==self.iphone4Button && ![(UIButton *)sender isSelected])
    {
        deviceType=@"iPhone 4";
    }
    else if(sender==self.iphone5Button && ![(UIButton *)sender isSelected])
    {
        deviceType=@"iPhone 5";
    }
    else if(sender==self.iphone6Button && ![(UIButton *)sender isSelected])
    {
        deviceType=@"iPhone 6";
    }
    else if(sender==self.iphone6plusButton && ![(UIButton *)sender isSelected])
    {
        deviceType=@"iPhone 6+";
    }
    else if(sender==self.ipadMiniButton && ![(UIButton *)sender isSelected])
    {
        deviceType=@"iPad Mini";
    }
    else if(sender==self.ipadRetinaButton && ![(UIButton *)sender isSelected])
    {
        deviceType=@"iPad Retina";
    }
    else
    {
        deviceType=@"";
    }
    for (UIButton* button in buttons) {
        if(button == (UIButton *)sender)
            [button setSelected:![(UIButton *)sender isSelected]];
        else
            [button setSelected:false];
    }
    
}
@end
