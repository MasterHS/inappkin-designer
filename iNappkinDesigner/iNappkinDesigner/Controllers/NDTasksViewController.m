//
//  NDTasksViewController.m
//  iNappkinDesigner
//
//  Created by Orest on 11/06/14.
//  Copyright (c) 2014 Adesso. All rights reserved.
//

#import "NDTasksViewController.h"
#import "Project.h"
#import "NDTasksTableViewCell.h"
#import "NDUIElementViewTasks.h"
#import "NDHelpers.h"
#import "Sketch.h"
#import "NDCreateTaskViewController.h"
#import "CoreDataManager.h"
#import "SlimServerRequestHandler.h"
//#import "IBHeatMap.h"
#import "LFHeatMap.h"
@interface NDTasksViewController ()//<IBHeatMapDelegate>
@property (nonatomic) NSMutableArray *heatmapPoints;

//@property (nonatomic, strong) IBHeatMap *heatMap;

@end

//Wenn hier Werte angepasst werden: Auch die Werte im Storyboard fürs Textfield und Textview anpassen
//static NSString* placeHolderTaskTitle= @"Add New Task";


@implementation NDTasksViewController
NSArray* tapEvents;
NSTimer* timer;
UILabel* tipLabel;
UIBarButtonItem *firstSketchButton;
UIBarButtonItem *destinationSketchButton;
UIToolbar *imageToolbar;
NSArray* sketches;
NSArray* tasks;
- (void) viewDidLoad{
    [super viewDidLoad];
    [self loadData];
    imageToolbar=nil;
   
    [self createToolbar];
    if(![[NDHelpers getAppUser]isEqualToString:self.currentProject.userName])
    {
        self.resetStatsButton.enabled=NO;
        self.addTaskButton.enabled=NO;
        self.deleteTaskButton.enabled=NO;
        //imageToolbar.hidden=YES;
    }

 
    self.elementView.delegate = self;
    
    self.currentTask = nil;
    [self updateTaskInfosDisplay];
    
    self.currentSketchView.layer.cornerRadius = 20.0;
    self.currentSketchView.layer.masksToBounds = YES;
    self.currentSketchView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.currentSketchView.layer.borderWidth = 2.0;
    


    if([tasks count]!=0)
    {
        //select first task
        NSIndexPath *tIndexPath= [NSIndexPath indexPathForItem:0 inSection:0];
        [self.sideTableView.delegate tableView:self.sideTableView didSelectRowAtIndexPath:tIndexPath];

        //CGPoint newPOs=[self.view convertPoint:CGPointMake(self.currentSketchView.frame.origin.x, self.currentSketchView.frame.origin.y) toView:self.currentSketchView];
        [self displayFirstSketch];
        
        
    }
//    else
//    {
//        imageToolbar.hidden=YES;
//    }

    [self showHeatMap];
    
}
-(void)loadData
{
    tasks=[[CoreDataManager sharedManager]getTasks:self.currentProject.guid];
    sketches=[[CoreDataManager sharedManager]getSketches:self.currentProject.guid];
}
- (IBAction)switchHeatmap:(id)sender {
    if(![self.heatmapSwitch isOn])
    {
        [self hideHeatMap];
    }
    else
    {
        [self showHeatMap];
    }
}


-(void)createToolbar
{
    CGRect frame=CGRectMake(self.currentSketchView.frame.origin.x,
                            self.currentSketchView.frame.origin.y+self.currentSketchView.frame.size.height-50,
                            self.currentSketchView.frame.size.width, 50);
    imageToolbar= [[UIToolbar alloc] initWithFrame:frame];
    imageToolbar.layer.cornerRadius=20.0;
    imageToolbar.layer.masksToBounds=YES;
    
    imageToolbar.barStyle=-1;
    imageToolbar.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:0.2];
    firstSketchButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"one_finger_un.png"] landscapeImagePhone:[UIImage imageNamed:@"one_finger_un.png"] style:UIBarButtonItemStyleBordered target:self action:@selector(selectFirstSketch:)];
    
    UIBarButtonItem *flex = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil] ;
    
    destinationSketchButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"whole_hand_un.png"] landscapeImagePhone:[UIImage imageNamed:@"whole_hand_un.png"] style:UIBarButtonItemStyleBordered target:self action:@selector(selectDestinationSketch:)];
    
    [imageToolbar setItems: [NSArray arrayWithObjects: firstSketchButton, flex, destinationSketchButton, nil]];
    [self.taskDetailView addSubview:imageToolbar];
    if(![[NDHelpers getAppUser]isEqualToString:self.currentProject.userName])
    {
        imageToolbar.hidden=YES;
    }
    else
        imageToolbar.hidden=NO;
    
}
-(void)viewWillAppear:(BOOL)animated{
    [self autoselectAddNewTaskRow];
}

//resigned firstresponder auf Textview und TextField wenn außerhalb getapt wird
//resigned first responder is taped on TextView and text field if outside
//- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
//    UITouch *touch = [[event allTouches] anyObject];
//    if ([self.taskDescriptionTextView isFirstResponder] && [touch view] != self.taskDescriptionTextView) {
//        [self.taskDescriptionTextView resignFirstResponder];
//    }
//    else if ([self.taskTitleTextField isFirstResponder] && [touch view] != self.taskTitleTextField) {
//        [self.taskTitleTextField resignFirstResponder];
//    }
//    [super touchesBegan:touches withEvent:event];
//}

-(void)autoselectAddNewTaskRow{
    if([tasks count]>0)
    {
        NSIndexPath *rowToSelect =[NSIndexPath indexPathForRow:0 inSection:0];
        [self.sideTableView selectRowAtIndexPath:rowToSelect animated:YES scrollPosition:UITableViewScrollPositionTop];
    }
}


#pragma mark - Table View DataSource + Delegate
- (NSInteger) tableView : (UITableView *) tableView numberOfRowsInSection : (NSInteger) section{
    return [tasks count];
}

- (CGFloat) tableView : (UITableView *) tableView heightForRowAtIndexPath : (NSIndexPath *)indexPath{
    return 100.0;
}

//Add new task if it is the first line, otherwise the task name and description
- (UITableViewCell *) tableView : (UITableView *) tableView cellForRowAtIndexPath : (NSIndexPath *)indexPath{
    NDTasksTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier : @"NDTasksTableViewCell"];
    if (cell == nil) {
        cell =[[NDTasksTableViewCell alloc] initWithStyle : UITableViewCellStyleDefault reuseIdentifier :@"NDTasksTableViewCell"];
    }
    //NSLog(@"indexPath: %i",indexPath.row);
    //if (indexPath.row == 0) {
    //cell.primaryLabel.text = placeHolderTaskTitle;
    //cell.primaryLabel.textColor = [UIColor blueColor];
    //}
    //else{
    //    NSLog(@"[self.currentProject tasks]:%i",[[self.currentProject tasks]count]);
    //    NSLog(@"[self.currentProject tasks.actions]:%i",[[[[self.currentProject tasks]objectAtIndex:0 ]actions] count]);
    if([tasks count]>0)
    {
        Task * task = [tasks objectAtIndex: indexPath.row];
        cell.primaryLabel.text = task.taskName;
        cell.secondaryLabel.text = task.taskDescription;
    }
    //}
    return cell;
}

- (void) tableView : (UITableView *) tableView didSelectRowAtIndexPath : (NSIndexPath *) indexPath{
    //so that the focus is taken from the text entry
    [self.view endEditing:NO];
    if([ sketches count]>0)
    {
        self.currentTask = [ tasks objectAtIndex : indexPath.row];
    }
    else
    {
        self.currentTask = nil;
    }
    
    // add first sketch & buttons to imageView
    [self displayFirstSketch];
    [self updateTaskInfosDisplay];
    [self showHeatMap];
}

#pragma mark - Bau der Anzeige von Task Infos
-(void)updateTaskInfosDisplay{
    if (![sketches count]) {
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle : @"Error" message :@"No sketches added! Please add sketches before you create tasks." delegate : nil cancelButtonTitle : @"OK" otherButtonTitles : nil];
        [alert show];
        return;
    }
    self.recordingSwitch.on = NO;
    self.resetStatsButton.enabled = NO;
    self.toolbar.backgroundColor = [UIColor whiteColor];
    self.taskDetailView.hidden =NO;
    self.sideTableView.hidden = NO;
    //self.toolbar.hidden = NO;
    
    if (self.currentTask) {
        [self showInfosofCurrentTask];
    }
    else [self showInfosofNewTask];
}

-(void)showInfosofNewTask{
    //    self.taskTitleTextField.text = @"";
    //	self.taskDescriptionTextView.text = @"";
    self.transitionCountLabel.text = @"0";
}

- (void) showInfosofCurrentTask{
    //self.taskTitleTextField.text = self.currentTask.taskName;
    //self.taskDescriptionTextView.text = self.currentTask.taskDescritopn;
    self.transitionCountLabel.text = @"0";
    //TODO:
    //if (self.currentTask.actions.count) {
    //   self.transitionCountLabel.text =[NSString stringWithFormat: @"%lu",(unsigned long) self.currentTask.actions.count];
    //}
}

- (void) displayFirstSketch{
    if(imageToolbar==nil)
        [self createToolbar];
    if([ tasks count]==0)
        return;
    if(self.currentTask.firstSketchGuid==nil)
        self.currentShownSketch = [ sketches objectAtIndex:0];
    else
        self.currentShownSketch = [[CoreDataManager sharedManager]getDataById:self.currentTask.firstSketchGuid inTable:@"Sketch"];
    if([ tasks count]>0)
    {
        // NSInteger index=[self.currentProject getSketchIndex:self.currentShownSketch];
        //        CGRect screenRect = [[UIScreen mainScreen] bounds];
        //        self.elementView =[self.elementView initWithFrame: screenRect withSketchIndex: index inProject: self.currentProject];
    }

    self.currentSketchView.image = [UIImage imageWithData:self.currentShownSketch.imageFile];;

    
    [self chechFirstAndDistenationSketches];
}

-(void)updateTableView{
    NSUInteger row=[tasks indexOfObject:self.currentTask];
    NSIndexPath *iPath = [NSIndexPath indexPathForRow:row inSection:0];
    [self.sideTableView reloadData];
    if(tasks!=nil)
    [self.sideTableView selectRowAtIndexPath:iPath animated:NO scrollPosition:UITableViewScrollPositionBottom];
    if([ tasks count]==0)
    {
        
        self.currentSketchView.image=nil;
        
        //imageToolbar.hidden=YES;
        self.currentTask=nil;
        self.currentShownSketch=nil;
        //[self.elementView setHidden:YES];
    }
    
}

# pragma mark - Task Transitions & Recording
//wird aufgerufen wenn Transitions recorded werden und man dafuer auf einen Button in der UIElementView tippt
// is called when transitions are recorded and you for it typed on a button in the UIElementView
- (void) jumpToDestionation : (NSInteger) destIndex{
    self.currentShownSketch=[ sketches objectAtIndex: destIndex];
    self.currentSketchView.image = [UIImage imageWithData:self.currentShownSketch.imageFile];
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    self.elementView =[self.elementView initWithFrame: screenRect withSketchIndex: destIndex inProject: self.currentProject];
    
    if(self.recordingSwitch.on)
        self.elementView.hidden=NO;
    else
        self.elementView.hidden=YES;
    [self chechFirstAndDistenationSketches];
    [self showHeatMap];
    
}
-(void)chechFirstAndDistenationSketches
{
    if(self.currentShownSketch==[[CoreDataManager sharedManager]getDataById:self.currentTask.firstSketchGuid inTable:@"Sketch"])
    {
        [firstSketchButton setTintColor:[UIColor greenColor]];
        [destinationSketchButton setTintColor:[UIColor blackColor]];
    }
    else if(self.currentShownSketch==[[CoreDataManager sharedManager]getDataById: self.currentTask.destinationSketchGuid inTable:@"Sketch" ])
    {
        [destinationSketchButton setTintColor:[UIColor greenColor]];
        [firstSketchButton setTintColor:[UIColor blackColor]];
    }
    else
    {
        [firstSketchButton setTintColor:[UIColor blackColor]];
        [destinationSketchButton setTintColor:[UIColor blackColor]];
    }
}
- (IBAction)jumpToNextSketch:(id)sender {
    if([sketches count]>0)
    {
        NSInteger index=[sketches indexOfObject:self.currentShownSketch];
        
        if(index+1<[ sketches count])
            [self jumpToDestionation:index+1];
        
    }
    
    
}

- (IBAction)jumptToPreviousSketch:(id)sender {
    if([ tasks count]>0)
    {
        NSInteger index=[sketches indexOfObject:self.currentShownSketch];
        if(index-1>=0)
            [self jumpToDestionation:index-1];
    }
}

- (void) addStepToTask : (TransitionButton *) element{
    if (self.recordingSwitch.on) {
        //TODO
        //        Action * action = (Action*)[[CoreDataManager sharedManager]createClickAction:element withTask:self.currentTask];
        //
        //        [self.currentTask addActionsObject: action ];
        //        NSLog(@"count: %i",self.currentTask.actions.count);
        //        self.transitionCountLabel.text = [NSString stringWithFormat : @"%lu",(unsigned long) self.currentTask.actions.count];
        //        self.resetStatsButton.enabled = YES;
    }
}

- (IBAction) recordSwitchTapped : (id) sender{
    
    //    if (!self.currentTask) {
    //        UIAlertView * alert = [[UIAlertView alloc] initWithTitle : @"Error" message :@"Please give this task a name before you start recording." delegate : nil cancelButtonTitle : @"OK" otherButtonTitles : nil];
    //        [alert show];
    //        self.recordingSwitch.on = NO;
    //        return;
    //    }
    //    if (self.recordingSwitch.on) {
    //        //remove the action if the task already has actions
    //        if(self.currentTask.actions.count>0)
    //        {
    //            //for (Action* ac in self.currentTask.actions){
    //            [[CoreDataManager sharedManager]deleteActions];
    //
    //
    //            [self.currentTask removeAllActions];
    //            self.transitionCountLabel.text = [NSString stringWithFormat : @"%lu",(unsigned long) self.currentTask.actions.count];
    //        }
    //        self.toolbar.backgroundColor = [UIColor redColor];
    //        self.toolbar.barTintColor = Nil;
    //        self.resetStatsButton.enabled = YES;
    //        [self.elementView setHidden:NO];
    //        [self jumpToDestionation:[self.currentProject getSketchIndex:self.currentTask.firstSketch]];
    //    }
    //    else {
    //
    //        self.toolbar.backgroundColor = [UIColor whiteColor];
    //        self.toolbar.barTintColor = [UIColor whiteColor];
    //        self.resetStatsButton.enabled = NO;
    //        [self.elementView setHidden:YES];
    //        //[self updateTaskOnServer];
    //    }
    //update task actions on the server
    //    SlimServerRequestHandler *requestHandler= [[SlimServerRequestHandler alloc] initWithBaseURLStringForGeneralFunctionality:[NDHelpers getBaseServerName] baseURLStringForImageDownload:[NDHelpers getImageServerName]];
    //    [requestHandler updateTaskActions:self.currentTask];
    
    //update changes locally
    //[[CoreDataManager sharedManager]saveData];
}

- (IBAction) resetRecording: (id) sender{
    //TODO:
    //    [self.currentTask  ];
    //	[self.transitionCountLabel setText:@"0"];
    //	[self jumpToDestionation : 0];
}

#pragma mark - Delete Task
- (IBAction) DeleteTask : (id) sender{

    //wenn wir gerade einen task bearbeiten und keinen neuen erstellen koennen wir ihn loeschen
    //if we are editing a task and create a new we can delete it
    if (self.currentTask) {
        //delete task
        self.currentTask= [[CoreDataManager sharedManager]createOrModifyTask:nil taskGuid:self.currentTask.guid projectGuid:self.currentProject.guid syncStatus:@"1" deleted:@"1"];
        self.currentTask = nil;
        [self loadData];
    }
    //wir waehlen wieder die Add new task Zeile aus und updaten die Anzeige
    [self updateTableView];
    [self autoselectAddNewTaskRow];
    //[self updateTaskInfosDisplay];
    
    //[self viewDidLoad];
}


#pragma mark - UITextField/UITextView Delegate

-(void)updateTask:(NSString*)taskName taskDescription:(NSString*)desc{
    //update task
    if (self.currentTask) {
        
        self.currentTask= [[CoreDataManager sharedManager]createOrModifyTask:@{@"taskName":taskName,@"taskDescription":desc} taskGuid:self.currentTask.guid projectGuid:self.currentProject.guid syncStatus:@"1" deleted:@"0"];
                [self loadData];
    }
    [self updateTableView];
    [self autoselectAddNewTaskRow];
}

- (void) textViewDidEndEditing : (UITextView *) textView{
    if (self.currentTask) {
        //update task
        self.currentTask= [[CoreDataManager sharedManager]createOrModifyTask:@{@"taskDescription":textView.text} taskGuid:self.currentTask.guid projectGuid:self.currentProject.guid syncStatus:@"1" deleted:@"0"];
                [self loadData];
    }
    [self updateTableView];
    [self.view endEditing: YES];
}

-(void)selectFirstSketch:(id)sender {
    CGRect ref = CGRectMake(350, 350, 400, 50);
    tipLabel= [[UILabel alloc]initWithFrame:ref];
    tipLabel.backgroundColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.6];
    tipLabel.layer.cornerRadius=20;
    tipLabel.layer.masksToBounds=true;
    tipLabel.font=[tipLabel.font fontWithSize:30];
    tipLabel.textColor = [UIColor whiteColor];
    tipLabel.textAlignment = NSTextAlignmentCenter;
    //update task
    if(self.currentShownSketch!=[[CoreDataManager sharedManager]getDataById: self.currentTask.destinationSketchGuid inTable:@"Sketch"])
    {
        if(self.currentShownSketch==[[CoreDataManager sharedManager]getDataById: self.currentTask.firstSketchGuid inTable:@"Sketch"])
        {
            [tipLabel setText:@"   first sketch removed   "];
            self.currentTask.firstSketchGuid=nil;
            self.currentTask= [[CoreDataManager sharedManager]createOrModifyTask:@{@"firstSketchGuid":@""} taskGuid:self.currentTask.guid projectGuid:self.currentProject.guid syncStatus:@"1" deleted:@"0"];
                    [self loadData];
        }
        else
        {[tipLabel setText:@"   first sketch selected   "];
            self.currentTask.firstSketchGuid=self.currentShownSketch.guid;
            self.currentTask= [[CoreDataManager sharedManager]createOrModifyTask:@{@"firstSketchGuid":self.currentShownSketch.guid} taskGuid:self.currentTask.guid projectGuid:self.currentProject.guid syncStatus:@"1" deleted:@"0"];
                    [self loadData];
        }
    }
    else
    {
        [tipLabel setText:@"   already selected!   "];
    }
    
    [self.view addSubview:tipLabel];
    [self chechFirstAndDistenationSketches];
    [self startTimer:tipLabel];
    
    
}
-(void)selectDestinationSketch:(id)sender {
    
    CGRect ref = CGRectMake(350, 350, 400, 50);
    tipLabel= [[UILabel alloc]initWithFrame:ref];
    tipLabel.backgroundColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.6];
    tipLabel.layer.cornerRadius=20;
    tipLabel.layer.masksToBounds=true;
    tipLabel.font=[tipLabel.font fontWithSize:30];
    tipLabel.textColor = [UIColor whiteColor];
    tipLabel.textAlignment = NSTextAlignmentCenter;
    //update task
    if(self.currentShownSketch!=[[CoreDataManager sharedManager]getDataById: self.currentTask.firstSketchGuid inTable:@"Sketch"])
    {
        if(self.currentShownSketch==[[CoreDataManager sharedManager]getDataById: self.currentTask.destinationSketchGuid inTable:@"Sketch"])
        {
            [tipLabel setText:@"   destination sketch removed   "];
            self.currentTask.destinationSketchGuid=nil;
            self.currentTask= [[CoreDataManager sharedManager]createOrModifyTask:@{@"destinationSketchGuid":@""} taskGuid:self.currentTask.guid projectGuid:self.currentProject.guid syncStatus:@"1" deleted:@"0"];
                    [self loadData];
        }
        else
        {
            
            [tipLabel setText:@"   destination sketch selected  "];
            self.currentTask.destinationSketchGuid=self.currentShownSketch.guid;
            self.currentTask= [[CoreDataManager sharedManager]createOrModifyTask:@{@"destinationSketchGuid":self.currentShownSketch.guid} taskGuid:self.currentTask.guid projectGuid:self.currentProject.guid syncStatus:@"1" deleted:@"0"];
                    [self loadData];
        }
    }
    else
    {
        [tipLabel setText:@"   already selected!   "];
    }
    [self.view addSubview:tipLabel];
    [self chechFirstAndDistenationSketches];
    [self startTimer:tipLabel];

}
- (void)startTimer:(UILabel*)label
{
    firstSketchButton.enabled=false;
    destinationSketchButton.enabled=false;
    timer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                             target:self
                                           selector:@selector(timerCallback)
                                           userInfo:nil
                                            repeats:NO];
}


- (void)timerCallback
{
    tipLabel.hidden=YES;
    firstSketchButton.enabled=true;
    destinationSketchButton.enabled=true;
}

#pragma mark - Heatmap
- (IBAction) showTestReport : (id) sender{
    //    //TODO:
    //    //NSString * reportURL =[NSString stringWithFormat :@"http://ios14adesso-bruegge.in.tum.de:7001/data/heatmap/%@/%@.html",
    //      //                     self.currentProject.projectId, self.currentTask];
    //    WebViewController *aVC = [[WebViewController alloc]initWithString:reportURL];
    //
    //    [self presentViewController:aVC animated:YES completion:nil];
}
-(BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    if([ sketches count]==0)
        return NO;
    if([identifier isEqualToString:@"viewTask"])
    {
        if([tasks count]==0)
            return NO;
    }
    return YES;
}
- (IBAction)unwindToTasks:(UIStoryboardSegue *)unwindSegue
{
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    //
    //    UIStoryboard *modalStoryboard = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
    //    UINavigationController *navController = [modalStoryboard instantiateViewControllerWithIdentifier:@"taskNavicontroller"];
    //    NDCreateTaskViewController *destinationViewController = [navController viewControllers][0];
    
    //
    
    UINavigationController *navController = (UINavigationController*)[segue destinationViewController];
    NSLog(@"%@",self.navigationController);
    NDCreateTaskViewController *destinationViewController = (NDCreateTaskViewController*)[navController topViewController];
    destinationViewController.currentProject=self.currentProject;
    
    destinationViewController.delegate = self;
    if([segue.identifier isEqualToString:@"addTask"])
    {
        destinationViewController.currentTask=nil;
        destinationViewController.recentSegue=@"addTask";
        
    }
    else if([segue.identifier isEqualToString:@"viewTask"])
    {
        if(!self.currentTask)
        {
            UIAlertView * alert = [[UIAlertView alloc] initWithTitle : @"Error" message :@"Please select a task first!." delegate : nil cancelButtonTitle : @"OK" otherButtonTitles : nil];
            [alert show];
            return;
            
        }
        destinationViewController.currentTask=self.currentTask;
        destinationViewController.recentSegue=@"viewTask";
    }
    
    
}
-(void)createTask:(NSString*)taskName taskDescription:(NSString*)desc
{
        //Check first whether there is the task name already. If so put back text field and display error message.
        if (taskName.length > 0){
            //create task
            Sketch* firstsketch=[sketches objectAtIndex:0];
            Sketch* destsketch=[sketches objectAtIndex:[sketches count]-1];
            NSDictionary *taskData=@{@"taskName":taskName,@"taskDescription":desc,@"firstSketchGuid":firstsketch.guid,@"destinationSketchGuid":destsketch.guid};

            self.currentTask=[[CoreDataManager sharedManager]createOrModifyTask:taskData taskGuid:nil projectGuid:self.currentProject.guid syncStatus:@"1" deleted:@"0"];
                    [self loadData];
        }
        [self updateTableView];
        [self autoselectAddNewTaskRow];
    
    [self.view endEditing: YES];
}
- (void)showHeatMap{

        if([self.heatmapSwitch isOn])
        {
            tapEvents=[[CoreDataManager sharedManager]getTapEvents:self.currentTask.guid sketchGuid:self.currentShownSketch.guid];
            if([ tapEvents count]>0)
            {
                [self initHeatMapView2];
            }
            else
                [self hideHeatMap];
            }
        else
        {
            [self hideHeatMap];
        }
    
}
-(void)hideHeatMap{
    NSArray *viewsToRemove = [self.currentSketchView subviews];
    for (UIView *v in viewsToRemove) {
        [v removeFromSuperview];
    }
    //self.heatMap=nil;
    tapEvents=nil;
    
}
//bool mapInit=false;



-(void)initHeatMapView2
{

        self.heatmapPoints = [@[] mutableCopy];
    
        //self.weights = [[NSMutableArray alloc] initWithCapacity:[quakeData count]];
        for (TapEvent* tap in  tapEvents) {
            if([tap.sketchGuid isEqualToString:self.currentShownSketch.guid])
            {
                NSDictionary *valueDict= [NDHelpers getAspectRatioAndScreensizeForDeviceType:tap.deviceType];
                int scale;
                if([tap.deviceType isEqualToString:@"iPhone 6+"])
                    scale=3;
                else
                    scale=2;
    
                CGFloat heightAspect= [[valueDict objectForKey:@"height"]floatValue]/scale;
                CGFloat widthAspect= [[valueDict objectForKey:@"width"]floatValue]/scale;
    
                CGFloat percentX = [tap.xAxis floatValue]*self.currentSketchView.frame.size.width/widthAspect;
                CGFloat percentY= [tap.yAxis floatValue]*self.currentSketchView.frame.size.height/heightAspect;

                CGPoint point= CGPointMake(percentX,
                                           percentY);
                [self.heatmapPoints addObject:[NSValue valueWithCGPoint:point]];
    
            }
        }
        UIImageView *mapImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.currentSketchView.frame.size.width, self.currentSketchView.frame.size.height) ];
    
        NSMutableArray *weights = [[NSMutableArray alloc] initWithCapacity:[self.heatmapPoints count] ];
        for (int i=0;i<[self.heatmapPoints count];i++)
        {
            [weights addObject:[NSNumber numberWithInteger:(1)]];
        }
        UIImage *heatmap = [LFHeatMap heatMapForMapView:mapImageView boost:1 locations:self.heatmapPoints weights:weights];
    
        mapImageView.image = heatmap;
    
        NSArray *viewsToRemove = [self.currentSketchView subviews];
        for (UIView *v in viewsToRemove) {
            [v removeFromSuperview];
        }
        [self.currentSketchView addSubview:mapImageView];
}

-(void)heatMapFinishedLoading {
    NSLog(@"FinishedLoadingHeatMap");
}

- (IBAction)resetStat:(id)sender {
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle : @"Reset Statistics" message : @"All statistics and heat map for current task will be deleted permanently. Do you want to proceed?"                 delegate:self
                                            cancelButtonTitle:@"Cancel"
                                            otherButtonTitles:@"Proceed", nil];
    
    [alert show];
    
    
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    //TODO
    //SlimServerRequestHandler *requestHandler= [[SlimServerRequestHandler alloc] initWithBaseURLStringForGeneralFunctionality:[NDHelpers getBaseServerName]];
    switch(buttonIndex) {
        case 0: //"No" pressed
            //do something?
            break;
        case 1: //"Yes" pressed
            //here you pop the viewController
            //[requestHandler deleteTaps:self.currentProject andTask:self.currentTask];
            //[requestHandler deleteStats:self.currentProject andTask:self.currentTask];
            break;
    }
}
@end
