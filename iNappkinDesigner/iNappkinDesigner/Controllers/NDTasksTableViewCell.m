//
//  NDTasksTableViewCell.m
//  iNappkinDesigner
//
//  Created by Fei Pan on 20/06/14.
//  Copyright (c) 2014 Adesso. All rights reserved.
//

#import "NDTasksTableViewCell.h"

@interface NDTasksTableViewCell ()

@end

@implementation NDTasksTableViewCell

- (id) initWithStyle : (UITableViewCellStyle) style reuseIdentifier : (NSString *) reuseIdentifier
{
	self = [super initWithStyle : style reuseIdentifier : reuseIdentifier];
	if (self) {
		// Initialization code

		// title
		CGRect myFrame = CGRectMake(10, 0.0, 170, 25.0);
		self.primaryLabel = [[UILabel alloc] initWithFrame : myFrame];
		self.primaryLabel.font = [UIFont boldSystemFontOfSize : 17.0];
		self.primaryLabel.backgroundColor = [UIColor clearColor];
		[self.contentView addSubview : self.primaryLabel];

		// discription

		myFrame.origin.y += 25.0;
		myFrame.size.height = 75.0;

		self.secondaryLabel = [[UILabel  alloc] initWithFrame : myFrame];
		self.secondaryLabel.font = [UIFont systemFontOfSize : 15.0];
		self.secondaryLabel.backgroundColor = [UIColor clearColor];
		self.secondaryLabel.numberOfLines = 3;
		[self.contentView addSubview : self.secondaryLabel];

	}
	return self;
}

- (void) awakeFromNib
{
	// Initialization code
}

- (void) setSelected : (BOOL) selected animated : (BOOL) animated
{
	[super setSelected : selected animated : animated];

	// Configure the view for the selected state
}

@end
