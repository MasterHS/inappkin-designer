//
//  NDTransitionsCollectionController.h
//  iNappkinDesigner
//
//  Created by Hazem Safetli on 04/06/14.
//  Copyright (c) 2014 Adesso. All rights reserved.
//

/*
 Zeigt alle Sketches eines Projekts in einer scrollbaren View in einem Popover an.
 durch Auswahl eines Sketch wird die View geschlossen und die Auswahl weitergegeben.
 */

#import <UIKit/UIKit.h>
#import "Project.h"

@interface NDTransitionDestinationSelectViewController : UICollectionViewController

@property (nonatomic,strong) Project * project;
@property (nonatomic,strong) id delegate;

@end
