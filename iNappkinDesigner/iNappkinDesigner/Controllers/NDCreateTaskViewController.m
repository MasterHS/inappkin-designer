//
//  NDCreateTaskViewController.m
//  iNappkinDesigner
//
//  Created by Luc Gaasch on 26/06/14.
//  Copyright (c) 2014 Adesso. All rights reserved.
//

#import "NDCreateTaskViewController.h"

@interface NDCreateTaskViewController ()

@end

@implementation NDCreateTaskViewController

- (id) initWithNibName : (NSString *) nibNameOrNil bundle : (NSBundle *) nibBundleOrNil
{
	self = [super initWithNibName : nibNameOrNil bundle : nibBundleOrNil];
	if (self) {
		// Custom initialization
	}
	return self;
}

- (void) prepareForSegue : (UIStoryboardSegue *) segue sender : (id) sender
{
	if (sender != self.createButton) {
		return;
	}
	if (self.nameField.text.length > 0) {
		self.task =
			[[ADSTask alloc] initWithName : self.nameField.text description : self.descriptionField.text firstSketch : nil];
	}
}

- (IBAction) nameChange : (id) sender
{
}

@end
