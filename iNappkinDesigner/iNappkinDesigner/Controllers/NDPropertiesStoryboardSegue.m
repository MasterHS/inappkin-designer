//
//  NDPropertiesStoryboardSegue.m
//  iNappkinDesigner
//
//  Created by Fei Pan on 15/06/14.
//  Copyright (c) 2014 Adesso. All rights reserved.
//

#import "NDPropertiesStoryboardSegue.h"
#import "NDPropertiesViewController.h"
#import "NDProjectSideBarViewController.h"

@implementation NDPropertiesStoryboardSegue

- (void) perform{
	NDProjectSideBarViewController * src =(NDProjectSideBarViewController *) self.sourceViewController;
	NDPropertiesViewController * dst =(NDPropertiesViewController *) self.destinationViewController;
	[src.delegate didSelectViewController : dst selectedIndex : 2];
}

@end
