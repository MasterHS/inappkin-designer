//
//  NDProjectSideBarViewController.m
//  iNappkinDesigner
//
//  Created by Fei Pan on 03/06/14.
//  Copyright (c) 2014 Adesso. All rights reserved.
//

#import "NDProjectSideBarViewController.h"
#import "NDPropertiesViewController.h"
#import "NDSketchViewController.h"
#import "NDTasksViewController.h"
#import "NDProjectViewController.h"
#import "NDHelpers.h"
#import "NDStatisticsViewController.h"

@interface NDProjectSideBarViewController ()
@end

@implementation NDProjectSideBarViewController

- (void) viewDidLoad{
	[super viewDidLoad];
    
    UIColor *tColor = [NDHelpers normalTextBoarderColor];
	self.view.layer.borderColor = [tColor CGColor];
	self.view.layer.borderWidth = 1.0f;
    
	// populate tabbar
	self.buttonArray  =[[NSArray alloc] initWithObjects : self.sketchesButton, self.tasksButton, self.ConfigurationButton, nil];
    
    //als erstesn Tab die Sketches anzeigen
    [self selectTab:0];
}

#pragma mark - Actions der Sidebarbuttons

- (IBAction) sketch : (id) sender{
	[self selectTab : 0];
}

- (IBAction) tasks : (id) sender{
	[self selectTab : 1];
}

- (IBAction) option : (id) sender{
	[self selectTab : 2];
}

- (void) selectTab : (int) tabID{
	switch (tabID) {
        case 0:
            [self.buttonArray[0] setSelected : true];
            [self.buttonArray[1] setSelected : false];
            [self.buttonArray[2] setSelected : false];
            break;
        case 1:
            [self.buttonArray[0] setSelected : false];
            [self.buttonArray[1] setSelected : true];
            [self.buttonArray[2] setSelected : false];
            break;
        case 2:
            [self.buttonArray[0] setSelected : false];
            [self.buttonArray[1] setSelected : false];
            [self.buttonArray[2] setSelected : true];
            break;
	}
	self.selectedIndex = tabID;
}

#pragma mark - Navigation
- (void) prepareForSegue : (UIStoryboardSegue *)segue sender : (UIButton*)sender{
    NSString *segueName = @"";
	if ([segue.identifier isEqualToString : @"propertiesSegue"]) {
		segueName = @": Info";
	}
    if ([segue.identifier isEqualToString : @"sketchSegue"]) {
		segueName = @": Sketches";
	}
	if ([segue.identifier isEqualToString : @"tasksSegue"]) {
		segueName = @": Tasks";
	}
    if ([segue.identifier isEqualToString : @"statsSegue"]) {
        segueName = @": Stats";
    }
    
    //### wo oder von wo wird die aufgerufen?
	if ([segue.identifier isEqualToString : @"tasksSegue"]) {
		NDTasksViewController * childViewController = (NDTasksViewController *) [segue destinationViewController];
		childViewController.currentProject = self.project;
	}
    if ([segue.identifier isEqualToString : @"statsSegue"]) {
        NDStatisticsViewController *tabBar = [segue destinationViewController];
        tabBar.currentProject = self.project;

        //NDStatsViewController * childViewController = (NDStatsViewController *) [segue destinationViewController];
        //childViewController.currentProject = self.project;
    }
    NDProjectViewController * projectVC = (NDProjectViewController *) self.parentViewController;
    [projectVC changeNavigationItemTo: segueName];
}


@end
