//
//  NDSketchViewController.h
//  iNappkinDesigner
//
//  Created by Fei Pan on 03/06/14.
//  Copyright (c) 2014 Adesso. All rights reserved.
//

/*
 Mit dieser Klasse wird ein Storyboard aufgebaut. Dazu koennen einer UIcollectionView Bilder aus der Photoroll oder frisch aufgenommene hinzugefuegt werden.
 
 In der klasse werden 2 UICollectionViews verwendet, die beide auch diese Klasse als Delegate und Data source verwenden:
 
 sideCollectionView ist die Collection auf der linken Seite, in der immer alle Sketches des Projekts angezeigt werden.
 transCollectionView wird beim Erstellen/Bearbeiten eines Transitionbuttons aufgerufen und dient der Auswahl des Zielsketch.
 
 ### die Verknüpfung von NDLandscapeImagePickerController und dem delegate dafuer hier mal genau ueberpruefen
 ### den ganzen Teil mit Aspect Ratio (z.B. bei takeSketchPicture) sollte man auslagern und enums benutzen
 ### den image cropping und scaling auslagern (Methoden cropImage und scaleImage) in eigene Klasse
 */

@import UIKit;
@class ADSServerRequestHandler;
@class NDLandscapeImagePickerController;
@class NDTransitionButtonOverlayView;
@class Project, Sketch,TransitionButton;
@class NDTransitionsListViewController;

@interface NDSketchViewController : UIViewController <UICollectionViewDataSource, UICollectionViewDelegate,
UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (weak, nonatomic) IBOutlet UICollectionView * sideCollectionView;
//@property (weak, nonatomic) IBOutlet UICollectionView * transCollectionView;

@property (weak, nonatomic) IBOutlet UISwitch * enableTest;
@property (weak, nonatomic) IBOutlet UIImageView * sketchDetailedImage;
@property (strong, nonatomic) NSString * pictureSource;
@property (strong, nonatomic) NSIndexPath * selectedSketchIndexPath;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *addSketchFromCameraButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *addSketchFromGalleryButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *deleteSketchButton;


@property Project *currentProject;
@property Sketch *currentlySelectedSketch;
@property TransitionButton *currentlySelectedTransitionButton;
@property IBOutlet NDTransitionButtonOverlayView *transitionButtonOverlay;
@property (strong, nonatomic) NDTransitionsListViewController* popoverContent;
@property IBOutlet UIBarButtonItem *deleteTransitionButton;
//@property IBOutlet UIView *transitionDestinationSelectView;
-(IBAction)transitionButtonPressed:(UIButton*)sender;
-(void)transitionButtonCreated;


- (IBAction) testerModeChanged : (id) sender;


@end
