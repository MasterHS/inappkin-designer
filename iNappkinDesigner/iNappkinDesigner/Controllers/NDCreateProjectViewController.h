//
//  NDCreateProjectController.h
//  iNappkinDesigner
//
//  Created by Luc Gaasch on 03/06/14.
//  Copyright (c) 2014 Adesso. All rights reserved.
//

/*
 Wird auf der NDProjectsController View in einem Overlay angezeigt.
 Erstellt ueber einen ADSServerRequestHandler ein Projekt.
 */

@import UIKit;
@class NDProjectsViewController;
@class ADSProject;
@class ADSServerRequestHandler;


@interface NDCreateProjectViewController : UIViewController<UINavigationControllerDelegate>

@property (weak, nonatomic) IBOutlet UIBarButtonItem * createButton;

@property (weak, nonatomic) IBOutlet UITextField * projectName;
@property (weak, nonatomic) IBOutlet UITextField * projectDescription;

@property (weak, nonatomic) IBOutlet UIButton *ipadRetinaButton;
@property (weak, nonatomic) IBOutlet UIButton *ipadMiniButton;
@property (weak, nonatomic) IBOutlet UIButton *iphone4Button;
@property (weak, nonatomic) IBOutlet UIButton *iphone5Button;
@property (weak, nonatomic) IBOutlet UIButton *iphone6Button;
@property (weak, nonatomic) IBOutlet UIButton *iphone6plusButton;
- (IBAction)buttonClicked:(id)sender;

@end
