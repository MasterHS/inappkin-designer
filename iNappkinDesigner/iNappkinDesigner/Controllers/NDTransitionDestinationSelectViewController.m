//
//  NDTransitionsCollectionController.m
//  iNappkinDesigner
//
//  Created by Hazem Safetli on 04/06/14.
//  Copyright (c) 2014 Adesso. All rights reserved.
//

#import "NDTransitionDestinationSelectViewController.h"
#import "NDTransitionDestinationCell.h"
#import "NDUIElementView.h"
#import "CoreDataManager.h"
@interface NDTransitionDestinationSelectViewController ()
{
	NSMutableArray * collectionSketches;
	NSInteger lastElementCreatedIndex;
}
@end

@implementation NDTransitionDestinationSelectViewController
NSArray* sketches;
- (UICollectionViewCell *) collectionView: (UICollectionView *) collectionView cellForItemAtIndexPath: (NSIndexPath *) indexPath{
	NDTransitionDestinationCell* cell =[collectionView dequeueReusableCellWithReuseIdentifier: @"reusableId"forIndexPath: indexPath];
	cell.cellImage.image = [collectionSketches objectAtIndex : indexPath.item];
	cell.cellImage.contentMode = UIViewContentModeScaleAspectFit;
	return cell;
}

- (NSInteger) collectionView : (UICollectionView *) collectionView numberOfItemsInSection : (NSInteger) section{
	return collectionSketches.count;
}

- (void) collectionView : (UICollectionView *) collectionView didSelectItemAtIndexPath : (NSIndexPath *) indexPath{
	NDUIElementView * uiElement = (NDUIElementView *) self.delegate;
	if (indexPath.item >= 0){
		Sketch * selectedDestenationSketch =[sketches objectAtIndex: indexPath.item];
		[uiElement createTransition : selectedDestenationSketch];
	}
	[uiElement.popover dismissPopoverAnimated : NO];
	uiElement.popover = nil;
}

- (void) viewDidLoad{
	[super viewDidLoad];

	// load the sketches from the project into the popoverview/UICollectionController
	lastElementCreatedIndex = -1;
	collectionSketches = [[NSMutableArray alloc]init];
    sketches=[[CoreDataManager sharedManager]getSketches:self.project.guid];
	for (int i = 0; i < [sketches count]; i++)
	{
		if (![sketches count]) {
			return;
		}
		Sketch * currentSketch = [ sketches  objectAtIndex : i];
		if (currentSketch.imageFile) {
			[collectionSketches addObject : currentSketch.imageFile];
		}
	}
}


@end
