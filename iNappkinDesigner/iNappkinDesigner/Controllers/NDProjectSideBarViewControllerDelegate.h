//
//  NDProjectSideBarViewControllerDelegate.h
//  iNappkinDesigner
//
//  Created by Fei Pan on 03/06/14.
//  Copyright (c) 2014 Adesso. All rights reserved.
//

@import Foundation;

@protocol NDProjectSideBarViewControllerDelegate <NSObject>

- (void) didSelectViewController : (UIViewController *) selectedViewController selectedIndex : (int)
	index;

@end
