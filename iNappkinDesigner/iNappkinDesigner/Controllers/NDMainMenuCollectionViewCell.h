//
//  NDMainMenuCollectionViewCell.h
//  iNappkinDesigner
//
//  Created by Fei Pan on 02/06/14.
//  Copyright (c) 2014 Adesso. All rights reserved.
//

/*
 Wird im NDProjectscontroller verwendet um Projekte zu repraesentieren.
 */

@import UIKit;

@interface NDMainMenuCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView * projectThumbnail;
@property (weak, nonatomic) IBOutlet UILabel * projectNameLabel;

@end
