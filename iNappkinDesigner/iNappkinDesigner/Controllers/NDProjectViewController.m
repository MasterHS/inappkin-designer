//
//  NDProjectViewController.m
//  iNappkinDesigner
//
//  Created by Dan on 20/5/14.
//  Copyright (c) 2014 Adesso. All rights reserved.
//

#import "NDProjectViewController.h"
#import "NDProjectSideBarViewController.h"
#import "NDPropertiesViewController.h"
#import "NDTasksViewController.h"
#import "NDSketchViewController.h"
#import "NDHelpers.h"
#import "Project.h"
#import "SlimServerRequestHandler.h"

@interface NDProjectViewController ()

@end

@implementation NDProjectViewController

- (void) viewDidLoad{
	[super viewDidLoad];
    
	// open first tab (sketches)
	NDProjectSideBarViewController * sideBarViewController = self.childViewControllers.firstObject;
	[sideBarViewController performSegueWithIdentifier: @"sketchSegue" sender : self];
	if (self.currentProject.projectName) {
		self.navigationItem.title = [self.currentProject.projectName stringByAppendingString: @": Sketches"];
		self.descriptionLabel.text = self.currentProject.projectDescription;
	} else {
		self.navigationItem.title = @"cannot get project name";
	}
    //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(removeUpdateView) name:@"ProjectUploadFinished" object:nil];
}


- (void) changeNavigationItemTo : (NSString *) string{
	self.navigationItem.title = [self.currentProject.projectName stringByAppendingString : string];
}

#pragma mark - sidebar
// Used for pass property data from project controller to it's sub controllers(sketch, tasts, properties)
- (void) didSelectViewController : (UIViewController *) selectedViewController selectedIndex : (int)index{
	for (UIView * view in self.contentView.subviews) {
		[view removeFromSuperview];
	}
    
	// pass the property data
	switch (index) {
        case 0:{
            NDSketchViewController * viewController = (NDSketchViewController *) selectedViewController;
            viewController.sketchDetailedImage.image = self.image;
            viewController.currentProject = self.currentProject;
            break;
        }
        case 1:{
            break;
        }
        case 2:{
            NDPropertiesViewController * viewController = (NDPropertiesViewController *) selectedViewController;
            viewController.currentProject = self.currentProject;
            break;
        }
        default:
            break;
	}
    
	[self.contentView addSubview : selectedViewController.view];
	[self addChildViewController : selectedViewController];
}

#pragma mark - IBActions

-(IBAction)saveToServer:(id)sender{
//    self.updateIndicatorView.hidden = NO;
//    [self.view bringSubviewToFront:self.updateIndicatorView];
//    self.navigationController.navigationBar.userInteractionEnabled = NO;
//    SlimServerRequestHandler *requestHandler= [[SlimServerRequestHandler alloc] initWithBaseURLStringForGeneralFunctionality:[NDHelpers getBaseServerName] baseURLStringForImageDownload:[NDHelpers getImageServerName]];
//    [requestHandler recreateProjectOnServer:self.currentProject];
}

#pragma mark - transition
- (void) prepareForSegue : (UIStoryboardSegue *) segue sender : (id) sender{
	if ([segue.identifier isEqualToString : @"sidebar_embed"]) {
		NDProjectSideBarViewController* childViewController=(NDProjectSideBarViewController*) [segue destinationViewController];
		childViewController.delegate = self;
        childViewController.project=_currentProject;
	}
}

#pragma mark - Notification Methoden
//-(void)removeUpdateView{
//    self.navigationController.navigationBar.userInteractionEnabled = YES;
//    self.updateIndicatorView.hidden = YES;
//}


@end
