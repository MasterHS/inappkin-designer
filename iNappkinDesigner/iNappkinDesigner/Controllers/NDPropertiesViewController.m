//
//  NDPropertiesViewController.m
//  iNappkinDesigner
//
//  Created by Orest on 05/06/14.
//  Copyright (c) 2014 Adesso. All rights reserved.
//

#import "NDPropertiesViewController.h"
#import "Project.h"
#import "CoreDataManager.h"

#import "SlimServerRequestHandler.h"
#import "NDHelpers.h"
#import "TransitionButton.h"
#import "Sketch.h"


@interface NDPropertiesViewController ()
@end

@implementation NDPropertiesViewController

- (void) viewDidLoad{
    [super viewDidLoad];
    if(![[NDHelpers getAppUser]isEqualToString:self.currentProject.userName])
    {
        self.projectName.enabled=NO;
        self.projectDescription.enabled=NO;
        self.deleteProjectButton.enabled=NO;
    }
    
    self.projectName.text = self.currentProject.projectName;
    self.projectDescription.text = self.currentProject.projectDescription;
    self.deviceType.text = self.currentProject.deviceType;
    self.projectOwnerTextField.text=self.currentProject.userName;
}

- (IBAction) deleteButtonClick : (id) sender{
    //delete project
    [[CoreDataManager sharedManager]createOrModifyProject:@{@"is_deleted":@"1"} projectGuid:self.currentProject.guid syncStatus:@"1" deleted:@"1"];
    [self.navigationController popToRootViewControllerAnimated:YES];
}
//Update project
-(void) textFieldDidEndEditing:(UITextField *)textField{
    NSMutableDictionary *data=[[NSMutableDictionary alloc]init];
    if ([textField.text isEqualToString:self.projectName.text]) {
        // do not do anything if the name hasn't changed
        if ([textField.text isEqualToString : self.currentProject.projectName]) {
            return;
        }
        if([NDHelpers checkNameValidityWithProjectName:textField.text]){
            //self.currentProject.name = textField.text;
            data[@"projectName"]=textField.text;
        }
    else{
            UIAlertView * alert = [[UIAlertView alloc] initWithTitle : @"Error" message : @"Project name is invalid or already in use. Please change your input to a valid name."
                                                            delegate : nil cancelButtonTitle : @"OK" otherButtonTitles : nil];
            [alert show];
            //self.projectName.text = self.currentProject.name;
            data[@"projectName"]=self.currentProject.projectName;
        }
    }
    if (textField== self.projectDescription) {
        //        self.currentProject.projectDescription = self.projectDescription.text;
        data[@"projectDescription"]=self.projectDescription.text;
    }
    //update project
    
    [[CoreDataManager sharedManager]createOrModifyProject:data projectGuid:self.currentProject.guid syncStatus:@"1" deleted:@"0"];
    //    [[CoreDataManager sharedManager]updateProject:self.currentProject syncStats:@"1"];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self.view endEditing:YES];
    return YES;
}

- (void) touchesBegan: (NSSet *) touches withEvent: (UIEvent *) event{
    [self.view endEditing:YES];
}


// BA hier alles Test des SlimServerRequestHandlers
//-(IBAction)uploadCurrentProject:(id)sender{
//        NSString* serverURL = [NDHelpers getBaseServerName];
//    SlimServerRequestHandler *requestHandler= [[SlimServerRequestHandler alloc]initWithBaseURLStringForGeneralFunctionality:serverURL baseURLStringForImageDownload:BaseURLStringForImage];
//
//    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//   [requestHandler deleteProject:self.currentProject];
//    });
//    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//          [requestHandler createProject:self.currentProject];
//    });
//}

-(IBAction)downloadAllProjectsFromServer:(id)sender{
    //    NSString* serverURL = [NDHelpers getBaseServerName];
    //    SlimServerRequestHandler *requestHandler= [[SlimServerRequestHandler alloc]initWithBaseURLStringForGeneralFunctionality:serverURL baseURLStringForImageDownload:BaseURLStringForImage];
    //
    //    [requestHandler downloadAllProjectsWithAllSubdata];
}


@end
