//
//  NDTransactionCellController.h
//  iNappkinDesigner
//
//  Created by Hazem Safetli on 04/06/14.
//  Copyright (c) 2014 Adesso. All rights reserved.
//

/*
 Wird im NDTransitionsCollectionController verwendet.
 Repraesentiert hier einen Sketch zu dem man eine transition machen kann.
 */

#import <UIKit/UIKit.h>

@interface NDTransitionDestinationCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView * cellImage;

@end
