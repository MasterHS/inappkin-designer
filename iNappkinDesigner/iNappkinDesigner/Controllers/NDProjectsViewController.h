//
//  NDMenuViewController.h
//  iNappkinDesigner
//
//  Created by Fei Pan on 02/06/14.
//  Copyright (c) 2014 Adesso. All rights reserved.
//

/*
 RootView der Navigation.
 Hier werden synchronisierte Projekte angezeigt.
 Durch Antippen eines Projekts kommt man zur Edit
 Durch Antippen des +Buttons kommt man zur NDCreateProjectController.
 Alle Projekte werden in einer UICollectionView angezeigt. Wenn man da was aendern moechte muss man also die delegates dafuer anfassen.
 
 BUGFIX: Der Aufruf und das überschreiben des UINAvigationController in der Segue ist sehr merkwürdig. 
 
 */

#import <CoreData/CoreData.h>
#import "User.h"
@import UIKit;
#import "NDLoginViewController.h"

@interface NDProjectsViewController : UIViewController<UICollectionViewDelegate,UICollectionViewDataSource, UIAlertViewDelegate>{
    NSArray *allProjects;
}
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;

@property (weak, nonatomic) IBOutlet UICollectionView * projectsCollectionView;
@property (strong, nonatomic) NDLoginViewController* popoverContent;
@property IBOutlet UIView *updateIndicatorView;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *signinButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *versionButton;

-(void)changeLoginButtonName:(NSString*)name;
@end
