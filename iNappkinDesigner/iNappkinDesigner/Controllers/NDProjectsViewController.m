//
//  NDMenuViewController.m
//  iNappkinDesigner
//
//  Created by Fei Pan on 02/06/14.
//  Copyright (c) 2014 Adesso. All rights reserved.
//

#import "NDProjectsViewController.h"
#import "NDMainMenuCollectionViewCell.h"
#import "NDCreateProjectViewController.h"
#import "NDProjectViewController.h"
#import "NDBackgoundLayer.h"
#import "NDHelpers.h"
#import "CoreDataManager.h"
#import "Project.h"
#import "Sketch.h"
#import "SlimServerRequestHandler.h"
#import "TransitionButton.h"

@interface NDProjectsViewController ()

@end
BOOL windowLoaded=false;
@implementation NDProjectsViewController

- (void) viewDidLoad{
    [super viewDidLoad];
    
    UIBarButtonItem * refreshBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"sync.png"] landscapeImagePhone:[UIImage imageNamed:@"sync.png"] style:UIBarButtonItemStylePlain target:self action:@selector(synchronizeWithServer:)];
    
    
    self.navigationItem.leftBarButtonItems = [[NSArray alloc] initWithObjects : self.navigationItem.leftBarButtonItems[0],refreshBarButtonItem, nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(removeUpdateView) name:@"ProjectsFinshedSyncing" object:nil];
    //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(startUploadingSketchesChanges) name:@"ProjectFinshedUploading" object:nil];
    //    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(startUploadingTransitionButtonsChanges) name:@"SketchFinshedUploading" object:nil];
    //    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(startUploadingTasksChanges) name:@"TransitionButtonFinshedUploading" object:nil];
    //    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(downloadProjectsFromServer) name:@"TaskFinshedUploading" object:nil];
    
    [self setNeedsStatusBarAppearanceUpdate];
    [self.versionButton setTitle:[NSString stringWithFormat:@"v%@", [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"]]];
//    if(self.updatesCount!=0)
//    {
//        //        self.numberBadge = [[MKNumberBadgeView alloc] initWithFrame:CGRectMake(200, 10, 40, 40)];
//        //
//        //
//        //        self.numberBadge.value = self.updatesCount;
//        //        [self.navigationController.navigationBar addSubview:self.numberBadge];
//    }
    
}

- (void) viewDidAppear:(BOOL)animated
{
    if(!windowLoaded)
    {
        NSString* userName=[[CoreDataManager sharedManager]getAvailableUser];
        if([userName isEqualToString:@""])
        {
            [self presentSignupViewController];
        }
        else
        {
            [self changeLoginButtonName: [NSString stringWithFormat:@"%@%@",userName,@" (signout)"]];
            [NDHelpers setAppUser:userName];
        }
        windowLoaded=true;
    }
    //[self checkDatabaseChanges];
}
- (IBAction)whatsNew:(id)sender {
    NSString *info = [NSString stringWithFormat:@"\nNew features\n"
                      "-------\n"
                      "1. Report is not generated from the collected data\n"
                      "     1.1 Report can be viewed as PDF file\n"
                      "     1.2 Report can be sent via email\n"
                      "2. Reset Statistics: will delete the collected data and the heat map\n"
                      ];
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle : @"What's new?" message :info
                                                    delegate : nil                                                                                                          cancelButtonTitle : @"OK"                                                                                                          otherButtonTitles : nil];
    [alert show];
}

- (void)presentSignupViewController {
    UIStoryboard *modalStoryboard = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
    UINavigationController *navController = [modalStoryboard instantiateViewControllerWithIdentifier:@"loginController"];
    self.popoverContent = [navController viewControllers][0];
    self.popoverContent.popover=[[UIPopoverController alloc] initWithContentViewController:navController];
    self.popoverContent.title=@"Login/Register";
    self.popoverContent.delegate=self;
    self.popoverContent.popover.delegate = self.popoverContent;
    CGRect loc=CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height/2, 0, 0);
    [self.popoverContent.popover presentPopoverFromRect:loc inView:self.view permittedArrowDirections:0 animated:YES];
    
}

- (void) viewWillAppear : (BOOL) animated{
    CAGradientLayer * bgLayer = [NDBackgroundLayer blueGradient];
    bgLayer.frame = self.view.bounds;
    [self.view.layer insertSublayer: bgLayer atIndex : 0];
    [self getAllProjects];
    [self.projectsCollectionView reloadData];
}

#pragma mark - Button Actions

-(IBAction)synchronizeWithServer:(id)sender{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Proceed?" message:@"Start syncing with the server?" delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES", nil];
    [alert show];
}

-(IBAction)manageSignin:(id)sender{
    self.signinButton.title=@"";
    [[CoreDataManager sharedManager]deleteUser:[NDHelpers getAppUser]];
    [NDHelpers setAppUser:nil];
    [self presentSignupViewController];
}
-(void)changeLoginButtonName:(NSString*)name
{
    self.signinButton.title=name;
}

//-(IBAction)showServerSettings:(id)sender{
//    ServerSettingsViewController *aVC= [[ServerSettingsViewController alloc]init];
//    [self presentViewController:aVC animated:YES completion:nil];
//}

#pragma mark - UICollectionView Data Source
- (NSInteger) numberOfSectionsInCollectionView : (UICollectionView *) collectionView{
    return 1;
}

- (NSInteger) collectionView : (UICollectionView *) collectionView numberOfItemsInSection : (NSInteger) section{
    return [allProjects count];
}

- (UICollectionViewCell *) collectionView: (UICollectionView *)collectionView cellForItemAtIndexPath: (NSIndexPath *)indexPath{
    NDMainMenuCollectionViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier: @"NDMenuCollectionViewCell" forIndexPath :indexPath];
    
    Project *project= [allProjects objectAtIndex:indexPath.row];
    cell.projectNameLabel.text = project.projectName;
    cell.projectNameLabel.numberOfLines = 0; // allow line breaks
    cell.projectNameLabel.textAlignment = NSTextAlignmentCenter; // center align
    
    // Project thumbnail
    UIImage * image;
    NSArray* sketches=[[CoreDataManager sharedManager]getSketches:project.guid];
    if ([sketches count]> 0) {
        Sketch *tSketch = [sketches objectAtIndex:0];
        image= [UIImage imageWithData: tSketch.imageFile];
    }
    else image = [UIImage imageNamed : @"ProjectDummyThumbnail"];
    
    cell.projectThumbnail.image = image;
    sketches=nil;
    return cell;
}

#pragma mark - TOOLS

-(void)getAllProjects{
    allProjects=[[CoreDataManager sharedManager]getAvailableProjects];
    [self.projectsCollectionView reloadData];
}

#pragma mark - Navigation
// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void) prepareForSegue : (UIStoryboardSegue *) segue sender : (id) sender{
    if ([sender isKindOfClass : [UICollectionViewCell class]]) {
        NDMainMenuCollectionViewCell * cell = (NDMainMenuCollectionViewCell *) sender;
        NSIndexPath * indexPath = [self.projectsCollectionView indexPathForCell : sender];
        if (indexPath) {
            if ([segue.identifier isEqualToString : @"OpenProject"]) {
                NDProjectViewController * projectController =(NDProjectViewController *) segue.destinationViewController;
                projectController.image = cell.projectThumbnail.image;
                projectController.currentProject = [allProjects objectAtIndex:indexPath.row];
            }
        }
    }
}

- (IBAction) unwindToList : (UIStoryboardSegue *) segue{
    [self getAllProjects];
}

#pragma mark - UIALertView Delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 1) {
        
        //[self checkDatabaseChanges];
        //[self uploadNewUpdates];
        //[[CoreDataManager sharedManager] resetDatastore];
        self.navigationController.navigationBar.userInteractionEnabled = NO;
        self.updateIndicatorView.hidden= NO;
        [self.view bringSubviewToFront:self.updateIndicatorView];
        
        SlimServerRequestHandler *requestHandler= [[SlimServerRequestHandler alloc] initWithBaseURLStringForGeneralFunctionality:[NDHelpers getBaseServerName]];
        
        [requestHandler startSyncingProcess];
        [self.projectsCollectionView reloadData];
        
    }
}

#pragma mark - Notification Methoden
-(void)removeUpdateView{
    self.navigationController.navigationBar.userInteractionEnabled = YES;
    self.updateIndicatorView.hidden = YES;
    [self getAllProjects];
}

- (NSArray *)arrayByRemovingObject:(id)obj array:(NSArray*)data
{
    if (!obj) return [data copy]; // copy because all array* methods return new arrays
    NSMutableArray *mutableArray = [NSMutableArray arrayWithArray:data];
    [mutableArray removeObject:obj];
    return [NSArray arrayWithArray:mutableArray];
}




@end
