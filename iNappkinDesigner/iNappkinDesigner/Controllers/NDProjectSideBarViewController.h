//
//  NDProjectSideBarViewController.h
//  iNappkinDesigner
//
//  Created by Fei Pan on 03/06/14.
//  Copyright (c) 2014 Adesso. All rights reserved.
//

/*
 Die Sidebar im NDProjectController.
 Wird als Navigationswerkzeug verwendet. 
 Ueber Buttons kann zwischen der Anzeige der allgeimenen Infos, der Tasks und der Bearbeitungsview gewechselt werden.
 
 ### kann man durch Highlighted Buttonproperties sauberer implementieren. Evtl. kann man die Klasse auch durch einen SplitViewController ersetzen?
 */

@import UIKit;
@class Project;
#import "NDProjectSideBarViewControllerDelegate.h"
#import "NDProjectViewController.h"

@interface NDProjectSideBarViewController : UIViewController

@property (weak, nonatomic) id <NDProjectSideBarViewControllerDelegate> delegate;
@property (strong, nonatomic) UICollectionView * SBCollectionView;
@property (strong, nonatomic) NSMutableArray * projectsArray;
@property (strong, nonatomic) Project * project;

@property (weak, nonatomic) IBOutlet UIButton * sketchesButton;
@property (weak, nonatomic) IBOutlet UIButton * tasksButton;
@property (weak, nonatomic) IBOutlet UIButton * ConfigurationButton;
@property (nonatomic) int selectedIndex;
@property (strong, nonatomic) NSArray * buttonArray;


@end

