//
//  NDSketchViewController.m
//  iNappkinDesigner
//
//  Created by Fei Pan on 03/06/14.
//  Copyright (c) 2014 Adesso. All rights reserved.
//

#import "NDSketchViewController.h"
#import "NDProjectCollectionViewCell.h"
#import "NDLandscapeImagePickerController.h"
#import "NDHelpers.h"
//#import "ADSFileHandler.h"
#import "Project.h"
#import "Sketch.h"
#import "CoreDataManager.h"
#import "NDTransitionButtonOverlayView.h"
#import "SlimServerRequestHandler.h"
#import "SlimServerRequestHandler.h"
#import "NDTransitionsListViewController.h"
@interface NDSketchViewController ()

@end

@implementation NDSketchViewController{
}
NSArray* sketches;
- (void) viewDidLoad{
    [super viewDidLoad];
    [self loadData];
    if(![[NDHelpers getAppUser]isEqualToString:self.currentProject.userName])
    {
        self.addSketchFromCameraButton.enabled=NO;
        self.addSketchFromGalleryButton.enabled=NO;
        self.deleteSketchButton.enabled=NO;
        self.deleteTransitionButton.enabled=NO;
        
    }
    self.transitionButtonOverlay.delegate = self;
    self.sideCollectionView.allowsMultipleSelection = NO;
    
    self.deleteTransitionButton.enabled=NO;
    //self.transitionDestinationSelectView.hidden=YES;
    self.sketchDetailedImage.layer.cornerRadius = 20.0;
    self.sketchDetailedImage.layer.masksToBounds = YES;
    self.sketchDetailedImage.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.sketchDetailedImage.layer.borderWidth = 2.0;
    [self autoSelectFirstSketch];
}


-(void)loadData
{
    sketches=[[CoreDataManager sharedManager]getSketches:self.currentProject.guid];
    
}

-(void) viewDidAppear:(BOOL)animated{
}

-(void) setCustomSketchDetailedImage:(UIImage*)image
{
    if(image!=nil)
    {
        [self.sketchDetailedImage setImage:image];
        image=nil;
        //            UIImage *watermarkImage = [UIImage imageNamed:@"one_finger_un.png"];
        //
        //        UIGraphicsBeginImageContext(image.size);
        //        [image drawInRect:CGRectMake(0, 0, image.size.width, image.size.height)];
        //        [watermarkImage drawInRect:CGRectMake(image.size.width - watermarkImage.size.width, image.size.height - watermarkImage.size.height, watermarkImage.size.width, watermarkImage.size.height)];
        //        UIImage *result = UIGraphicsGetImageFromCurrentImageContext();
        
        //
        //        UIGraphicsEndImageContext();
        //create toolbar and set origin and dimensions
        
        
    }
}


-(void)autoSelectFirstSketch{
    
    if ([sketches count]>0) {
        self.currentlySelectedSketch=[sketches objectAtIndex:0];        //[[self.currentProject sketches]objectAtIndex:0];
        self.selectedSketchIndexPath = [NSIndexPath indexPathForItem:0 inSection:0];
        UIImage *tImage=[UIImage imageWithData:self.currentlySelectedSketch.imageFile];
        [self setCustomSketchDetailedImage:tImage];
        //[self.sketchDetailedImage setImage:];
        [self.sideCollectionView selectItemAtIndexPath:self.selectedSketchIndexPath animated:YES scrollPosition:UICollectionViewScrollPositionBottom];
        [self.sideCollectionView scrollToItemAtIndexPath: self.selectedSketchIndexPath atScrollPosition: UICollectionViewScrollPositionBottom animated: NO];
    }
    //wenns keinen Sketch gibt
    else{
        self.currentlySelectedSketch= nil;
        self.sketchDetailedImage.image= nil;
        [self setCustomSketchDetailedImage:nil];
    }
    if ([sketches count]>0) {
        
        [self.transitionButtonOverlay setNewActiveSketch:self.currentlySelectedSketch];
        //self.transitionButtonOverlay.frame= [self getDisplayRectForCurrentImage];
    }
}

#pragma mark - UICollectionView Data Source
- (NSInteger) numberOfSectionsInCollectionView : (UICollectionView *) collectionView{
    return 1;
}

- (NSInteger) collectionView : (UICollectionView *) collectionView numberOfItemsInSection : (NSInteger) section{
    return [sketches count];
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 30.0f;
}
- (UICollectionViewCell *) collectionView: (UICollectionView *) collectionView cellForItemAtIndexPath: (NSIndexPath *)indexPath{
    NDProjectCollectionViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier : @"projectSidebarThumbnail" forIndexPath: indexPath];
    if ([sketches objectAtIndex:indexPath.row] != nil){
        
        Sketch * sketch = [sketches objectAtIndex:indexPath.row];
        cell.projectSidebarThumbnail.image = [UIImage imageWithData:sketch.imageFile];
        cell.backgroundColor = [UIColor clearColor];
        cell.sketch= sketch;
        //if(collectionView == self.sideCollectionView){
        if ([indexPath isEqual: self.selectedSketchIndexPath]) {
            cell.alpha = 1.0;
            cell.backgroundColor = [UIColor redColor];
        } else {
            cell.alpha = 0.6;
            cell.backgroundColor = [UIColor clearColor];
        }
    }
    return cell;
}


#pragma mark - CollectionView Delegate
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    //self.transitionDestinationSelectView.hidden = YES;
    self.deleteTransitionButton.enabled= NO;
    for (UICollectionViewCell * visibleCell in [collectionView visibleCells]) {
        visibleCell.backgroundColor = [UIColor clearColor];
        visibleCell.alpha = 0.6;
    }
    //UICollectionViewCell * datasetCell = [collectionView cellForItemAtIndexPath : indexPath];
    
    
    if ([collectionView isEqual : self.sideCollectionView]) {
        self.selectedSketchIndexPath = indexPath;
        self.sketchDetailedImage.alpha = 1;
        
        
        //		self.sketchDetailedImage.image = cCell.projectSidebarThumbnail.image;
        Sketch* sketch=[sketches objectAtIndex:indexPath.row];
        [self setCustomSketchDetailedImage:[UIImage imageWithData:sketch.imageFile]];
        sketch=nil;
        self.currentlySelectedSketch= [sketches objectAtIndex:indexPath.row];
        
        [self.transitionButtonOverlay setNewActiveSketch: self.currentlySelectedSketch];
        //self.transitionButtonOverlay.frame= [self getDisplayRectForCurrentImage];
    }
    
}


#pragma mark - Bilder Aufnehmen/aus der roll hinzufuegen
#pragma mark IBActions fuer Toolbar
- (IBAction) chooseSketchFromRoll : (id) sender{
    self.pictureSource = @"roll";
    NDLandscapeImagePickerController* picker = [[NDLandscapeImagePickerController alloc] init];
    picker.delegate = self;
    [picker setSourceType : UIImagePickerControllerSourceTypeSavedPhotosAlbum];
    [self presentViewController: picker animated: YES completion: NULL];
}

- (UIImage *)compressImage:(UIImage *)image{
    float actualHeight = image.size.height;
    float actualWidth = image.size.width;
    float maxHeight = 600.0;
    float maxWidth = 800.0;
    float imgRatio = actualWidth/actualHeight;
    float maxRatio = maxWidth/maxHeight;
    float compressionQuality = 0.5;//50 percent compression
    
    if (actualHeight > maxHeight || actualWidth > maxWidth) {
        if(imgRatio < maxRatio){
            //adjust width according to maxHeight
            imgRatio = maxHeight / actualHeight;
            actualWidth = imgRatio * actualWidth;
            actualHeight = maxHeight;
        }
        else if(imgRatio > maxRatio){
            //adjust height according to maxWidth
            imgRatio = maxWidth / actualWidth;
            actualHeight = imgRatio * actualHeight;
            actualWidth = maxWidth;
        }else{
            actualHeight = maxHeight;
            actualWidth = maxWidth;
        }
    }
    
    CGRect rect = CGRectMake(0.0, 0.0, actualWidth, actualHeight);
    UIGraphicsBeginImageContext(rect.size);
    [image drawInRect:rect];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    NSData *imageData = UIImageJPEGRepresentation(img, compressionQuality);
    UIGraphicsEndImageContext();
    
    return [UIImage imageWithData:imageData];
}

- (IBAction) takeSketchPicture : (id) sender{
    NDLandscapeImagePickerController* picker = [[NDLandscapeImagePickerController alloc]init];
    picker.delegate = self;
    [picker setSourceType : UIImagePickerControllerSourceTypeCamera];
    self.pictureSource = @"camera";
    
    //    NSDictionary *valueDict= [NDHelpers getAspectRatioAndScreensizeForDeviceType:self.currentProject.deviceType];
    //
    //    CGFloat heightAspect= [[valueDict objectForKey:@"aspectRatioHeight"]floatValue];
    //    CGFloat widthAspect= [[valueDict objectForKey:@"aspectRatioWidth"]floatValue];
    //
    //    CGRect newFrame;
    //    newFrame.size.height = picker.view.frame.size.width;
    //    newFrame.size.width = newFrame.size.height * (widthAspect / heightAspect);
    //    newFrame.origin.x = (picker.view.frame.size.height - newFrame.size.width) / 2;
    //    UIView * overlayView = [[UIView alloc] initWithFrame : newFrame];
    //    [overlayView setFrame : newFrame];
    //    [overlayView.layer setBorderWidth : 2.0];
    //    [overlayView.layer setBorderColor : [[UIColor colorWithRed : 0.10 green : 0.45 blue : 0.73 alpha: 1.0]CGColor]];
    //    [overlayView.layer setCornerRadius : 2.0];
    //    [overlayView.layer setShadowOffset : CGSizeMake(-2, -2)];
    //    [overlayView.layer setShadowColor : [[UIColor lightGrayColor] CGColor]];
    //    [overlayView.layer setShadowOpacity : 0.5];
    //    picker.cameraOverlayView = overlayView;
    //
    //
    [self presentViewController: picker animated: YES completion: NULL];
}

# pragma  mark - Delete Operations
-(IBAction)deleteCurrentlySelectedTransitionButton:(id)sender{
    //delete transition
    //[[CoreDataManager sharedManager] deleteTransitionButton:self.currentlySelectedTransitionButton];
    [[CoreDataManager sharedManager]createOrModifyTransitionButton:nil transButtonGuid:self.currentlySelectedTransitionButton.guid sketchGuid:self.currentlySelectedSketch.guid syncStatus:@"1" deleted:@"1"];
    [self loadData];
    [self.transitionButtonOverlay setNewActiveSketch:self.currentlySelectedSketch];
    self.deleteTransitionButton.enabled= NO;
    //self.transitionDestinationSelectView.hidden=YES;
}

- (IBAction) deleteSketch: (id)sender{
    if (self.currentlySelectedSketch != nil){
        
        Sketch* sketch=[[CoreDataManager sharedManager]createOrModifySketch:@{@" is_deleted":@"1"} sketchGuid:self.currentlySelectedSketch.guid projectGuid:self.currentlySelectedSketch.projectGuid syncStatus:@"1" deleted:@"1"];
        sketch=nil;
        [self loadData];
        [self.sideCollectionView reloadData];
        [self autoSelectFirstSketch];
    }
}

#pragma mark - Delegates
#pragma mark Delegate fuer TransitionButtonOverlayView
-(void)jumpToDestinationSketchWithIdentifier:(NSInteger)identifier{
    NSIndexPath * selectionDest = [NSIndexPath indexPathForItem : identifier inSection : 0];
    [self.sideCollectionView scrollToItemAtIndexPath : selectionDest atScrollPosition: UICollectionViewScrollPositionBottom animated : NO];
    [self collectionView : self.sideCollectionView didSelectItemAtIndexPath : selectionDest];
}

-(IBAction)transitionButtonPressed:(UIButton*)sender{
    NSInteger selectedButtonIndex=sender.tag;
    NSArray* transitionButtons=[[CoreDataManager sharedManager]getTransitionButtons:self.currentlySelectedSketch.guid];
    self.currentlySelectedTransitionButton = [ transitionButtons objectAtIndex:selectedButtonIndex];
    //wir sind im Testmode -> die Transition ausfuehren
    if (self.enableTest.on) {
        Sketch* toSketch=[[CoreDataManager sharedManager]getDataById:self.currentlySelectedTransitionButton.destinationSketchGuid inTable:@"Sketch"];
        NSUInteger toSketchIndex=  [sketches indexOfObject:toSketch];
        if (toSketchIndex != NSNotFound) {
            self.selectedSketchIndexPath = [NSIndexPath indexPathForItem: toSketchIndex inSection: 0];
            self.currentlySelectedSketch= toSketch;
            [self.sketchDetailedImage setImage:[UIImage imageWithData:toSketch.imageFile]];
            [self.transitionButtonOverlay setNewActiveSketch:self.currentlySelectedSketch];
            //self.transitionButtonOverlay.frame= [self getDisplayRectForCurrentImage];
            [self.sideCollectionView selectItemAtIndexPath:self.selectedSketchIndexPath animated:YES scrollPosition:UICollectionViewScrollPositionTop];
            [self.sideCollectionView reloadData];
        }
        return;
    }
    if([[NDHelpers getAppUser]isEqualToString:self.currentProject.userName])
    {
        //wir sind im DesignerMode-> die TransitionView anzeigen damit man einen Zielsketch auswaehlen kann
        Sketch* destSketch=[[CoreDataManager sharedManager]getDataById:self.currentlySelectedTransitionButton.destinationSketchGuid inTable:@"Sketch"];
        NSInteger indexOfCurrentDestinationSketch= [sketches indexOfObject:destSketch];
        // [self.transCollectionView reloadData];
        
        
        UIStoryboard *modalStoryboard = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
        self.popoverContent = [modalStoryboard instantiateViewControllerWithIdentifier:@"transViewpopup"];
        
        //self.popoverContent = [navController viewControllers][0];
        self.popoverContent.delegate=self;
        self.popoverContent.popover=[[UIPopoverController alloc] initWithContentViewController:self.popoverContent];
        self.popoverContent.title=@"Select Destenation";
        
        self.popoverContent.currentProject=self.currentProject;
        self.popoverContent.currentlySelectedTransitionButton=self.currentlySelectedTransitionButton;
        CGPoint pnt=[self.sketchDetailedImage convertPoint:sender.frame.origin toView:self.view];
        
        self.popoverContent.indexOfCurrentDestinationSketch=indexOfCurrentDestinationSketch;
        self.popoverContent.transitionButtonOverlay= self.transitionButtonOverlay;
        self.popoverContent.sender=sender;
        self.popoverContent.selectedButtonIndex=selectedButtonIndex;
        self.popoverContent.popover.delegate = self.popoverContent;
        CGRect loc=CGRectMake(pnt.x+sender.frame.size.width,
                              pnt.y+sender.frame.size.height/2, 0, 0);
        [self.popoverContent.popover presentPopoverFromRect:loc inView:self.view permittedArrowDirections:UIPopoverArrowDirectionLeft animated:YES];
        
        self.deleteTransitionButton.enabled = YES;
        transitionButtons=nil;
    }
}
-(NSIndexPath*) getSketchIndextPath{
    return self.selectedSketchIndexPath;
}

-(void)transitionButtonCreated{
    //self.transitionDestinationSelectView.hidden=NO;
}

#pragma mark Image Picker Delegate
- (void) imagePickerController: (UIImagePickerController *) picker didFinishPickingMediaWithInfo: (NSDictionary *) info{
    UIImage *image = [info objectForKey : UIImagePickerControllerOriginalImage];
    //image=[NDHelpers cropScaleAndCompressImage:image forDeviceType:self.currentProject.deviceType];
    image=[self compressImage:image];
    if ([self.pictureSource isEqualToString : @"camera"]) {
        UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil);
    }
    //NSString *uniqueImageName = [[NSProcessInfo processInfo] globallyUniqueString];
    //NSString *filePath =[NSString stringWithFormat:@"%@/%@",self.currentProject.name,uniqueImageName];
    NSData *tData=  UIImagePNGRepresentation(image);
    NSString *encoded =[tData base64EncodedStringWithOptions:0];
    //create sketch
    Sketch* sketch=[[CoreDataManager sharedManager]createOrModifySketch:@{@"imageFile":encoded}
                                                             sketchGuid:nil projectGuid:self.currentProject.guid syncStatus:@"1" deleted:@"0"];
    sketch=nil;
    [self loadData];
    
    [self.sideCollectionView reloadData];
    //[self.transCollectionView reloadData];
    [self.sideCollectionView selectItemAtIndexPath:self.selectedSketchIndexPath animated:NO scrollPosition:UICollectionViewScrollPositionTop];
    
    [self dismissViewControllerAnimated : YES completion : NULL];
    
    // Set light status bar theme
    [[UIApplication sharedApplication] setStatusBarStyle : UIStatusBarStyleLightContent];
}

- (void) imagePickerControllerDidCancel : (UIImagePickerController *) picker{
    [[UIApplication sharedApplication] setStatusBarStyle : UIStatusBarStyleLightContent];
    [self dismissViewControllerAnimated : YES completion : NULL];
}

#pragma mark - View Funktionen
//- (void) touchesBegan: (NSSet *) touches withEvent: (UIEvent *) event{
//    UITouch *touch = [[event allTouches] anyObject];
//    if ([touch view] != self.transCollectionView) {
//        self.transitionDestinationSelectView.hidden= YES;
//        //alle buttons wieder auf blau setzen
//        self.deleteTransitionButton.enabled = NO;
//        [self.transitionButtonOverlay setAllButtonBackgroundcolorsToBlue];
//    }
//}

#pragma mark - Toolbar Funktionen
- (IBAction) testerModeChanged : (id) sender{
    if([self.enableTest isOn])
    {
        self.sketchDetailedImage.userInteractionEnabled = YES;
    }
    else
    {
        self.sketchDetailedImage.userInteractionEnabled = NO;
    }
    self.deleteTransitionButton.enabled= NO;
    //self.transitionDestinationSelectView.hidden=YES;
    [self.transitionButtonOverlay setAllButtonBackgroundcolorsToBlue];
}


#pragma mark - TOOLS
-(CGRect)getDisplayRectForCurrentImage{
    UIImageView *iv = self.sketchDetailedImage;
    CGSize imageSize = iv.image.size;
    CGFloat imageScale = fminf(CGRectGetWidth(iv.bounds)/imageSize.width, CGRectGetHeight(iv.bounds)/imageSize.height);
    CGSize scaledImageSize = CGSizeMake(imageSize.width*imageScale, imageSize.height*imageScale);
    CGRect imageFrame = CGRectMake(roundf(0.5f*(CGRectGetWidth(iv.bounds)-scaledImageSize.width))+self.sketchDetailedImage.frame.origin.x, roundf(0.5f*(CGRectGetHeight(iv.bounds)-scaledImageSize.height))+self.sketchDetailedImage.frame.origin.y, roundf(scaledImageSize.width), roundf(scaledImageSize.height));
    return imageFrame;
}

@end
