//
//  NDUIElementController.h
//  Add_button_to_sketch
//
//  Created by Hazem Safetli on 04/06/14.
//  Copyright (c) 2014 Hazem Safetli. All rights reserved.
//

/*
 Die UI-Elemente, die Interaktionspunkte auf einem Sketch represaentieren.
 Werden per Drag erzeugt.
 Werden durch blaue, semitransparente Views repräsentiert.
 Reagieren auf Touch. (mit Transition Message an den SketchVC)
 
 */

@import UIKit;
//#import "ADSServerRequestHandler.h"
//#import "ADSProject.h"
//#import "ADSSketch.h"
//#import "ADSUIElement.h"
//#import "ADSButton.h"
#import "NDTransitionDestinationSelectViewController.h"
//#import "ADSProject.h"
//#import "ADSStoryboard.h"
//#import "ADSSketch.h"
//#import "ADSTransition.h"
#import "NDSketchViewController.h"
#import "NDHelpers.h"

#import "Project.h"
#import "Sketch.h"

@interface NDUIElementView : UIView<UIPopoverControllerDelegate,UIGestureRecognizerDelegate>

@property (readwrite) CGPoint startPoint;
@property (readwrite) CGPoint endPoint;

@property (nonatomic,strong) Project * currentPoject;
@property (nonatomic, strong) Sketch * selectedSketch;

@property (nonatomic,strong) UIPopoverController * popover;
@property (nonatomic,strong) ADSProject * destenationSketch;
@property (nonatomic,strong) id delegate;

@property (strong, nonatomic) ADSServerRequestHandler * serverRequestHandler;

@property CGRect drawingRect;
@property CGContextRef context;
@property NSInteger selectedSketchIndex;


- (id) initWithFrame : (CGRect) frame withSketchIndex : (NSInteger) index inProject : (ADSProject *)project;
- (void) createTransition : (Sketch *) destinationSketch;
- (void) refereshUIElements;

@end
