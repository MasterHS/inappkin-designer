////
////  NDUIElementView
////  Add_button_to_sketch
////
////  Created by Hazem Safetli on 04/06/14.
////  Copyright (c) 2014 Hazem Safetli. All rights reserved.
////
//
//#import "NDUIElementView.h"
//
//@interface NDUIElementView ()
//@end
//
//@implementation NDUIElementView
//
//bool startDrawig = NO;
//bool buttonClicked = NO; // ---
//int const invalidSKetchIndex = -1;
//int j = 0;
//int selectedElementToMove = -1;
//
//CGPoint initialPoint;
//CGContextRef context;
//
//
//- (id) initWithFrame: (CGRect) frame withSketch: (Sketch*)sketch inProject: (Project *)project{
//	self = [super initWithFrame : [[UIScreen mainScreen] bounds]];
//	if (self) {
//		self.currentPoject = project;
//		self.backgroundColor = [UIColor clearColor];
//        self.selectedSketch =sketch;
//        //		[self initLongpressGesture];
//	}
//	return self;
//}
//
//#pragma mark - Buttons verschieben
////- (void) initLongpressGesture{
////	// attach long press gesture to collectionView
////	UILongPressGestureRecognizer * lpgr= [[UILongPressGestureRecognizer alloc]initWithTarget: self action: @selector(handleLongPress:)];
////	lpgr.minimumPressDuration = 0.2f;
////	lpgr.delegate = self;
////	[self addGestureRecognizer: lpgr];
////}
////
////- (void) handleLongPress : (UILongPressGestureRecognizer *) gesture{
////    //### sollte komplett umgebaut werden
////    return;
////	CGPoint gesturePoint = [gesture locationInView : self];
////	if (gesture.state == UIGestureRecognizerStateBegan) {
////		initialPoint = [gesture locationInView : self];
////	}
////	else if (gesture.state == UIGestureRecognizerStateChanged) {
////		CGPoint p = [gesture locationInView : self];
////		double dx = p.x - initialPoint.x;
////		double dy = p.y - initialPoint.y;
////		// move your views here.
////		if ([self.selectedSketch elementsCount] > 0) {
////			ADSButton * element;
////			if (selectedElementToMove == -1){
////				// get the index of the selected element
////				for (int i = 0; i < [self.selectedSketch elementsCount]; i++) {
////					element = (ADSButton *) [self.selectedSketch elementAtIndex : i];
////					if (!( (NDSketchViewController *) self.delegate ).enableTest.on) { // Designer Mode is Enabled
////						// detect the if the user gesture is inside the UIElement
////						if ( CGRectContainsPoint(element.location, gesturePoint) ) {
////							selectedElementToMove = i;
////							break;
////						}
////					}
////				}
////			}
////			else element = (ADSButton *) [self.selectedSketch elementAtIndex : selectedElementToMove];
////
////			CGRect newLocation = CGRectMake(element.location.origin.x + dx, element.location.origin.y + dy,
////											element.location.size.width, element.location.size.height);
////
////			// check the intersection when moving the UIElement
////			for (int i = 0; i < [self.selectedSketch elementsCount]; i++) {
////				ADSButton * elementToCheck = (ADSButton *) [self.selectedSketch elementAtIndex : i];
////				if (i != selectedElementToMove){
////					if ( !CGRectIntersectsRect(newLocation,elementToCheck.location) ){
////						element.location = newLocation;
////					}
////				}
////				else if ([self.selectedSketch elementsCount] == 1){
////					element.location = newLocation;
////				}
////			}
////			[self setNeedsDisplay];
////			initialPoint = p;
////		}
////	}
////	else if (gesture.state == UIGestureRecognizerStateEnded) {
////		selectedElementToMove = -1;
////	}
////	startDrawig = NO;
////}
//
//
//- (void) popoverControllerDidDismissPopover : (UIPopoverController *) popoverController{
//	NSLog(@"dismissPopover");
//	[self setNeedsDisplay];
//}
//
//- (BOOL) addElement : (ADSUIElement *) element{
//	NSLog(@"addElement");
//	if (element == nil) {
//		return NO;
//	}
//    //	[[self.project.storyboard getSketchAtIndex : self.selectedSketchIndex]addElement : element];
//	return YES;
//}
//
//- (void) drawRect : (CGRect) rect{
//	NSLog(@"drawRect");
//	[super drawRect : [[UIScreen mainScreen] bounds]];
//	context = UIGraphicsGetCurrentContext();
//	[self drawAllButtons];
//}
//
//- (void) drawAllButtons{
//	NSLog(@"drawButton");
//	bool deleteTransitionImage = YES; // ---
//	// Set the rectangle outerline-colour
//	CGContextSetStrokeColorWithColor(context, [UIColor blackColor].CGColor);
//    
//	// set the rectangle fill color
//	UIColor * blueColor = [UIColor colorWithRed : 0.041 green : 0.375 blue : 0.998 alpha : 0.4];
//	UIColor * purpleColor = [UIColor colorWithRed : 0.541 green : 0.075 blue : 0.498 alpha : 0.4];
//    
//	if (self.selectedSketch != nil) {
//		int k = 0;
//		for (ADSButton * button in self.selectedSketch.elements) { // --- changed ADSUIElement to ADSButton
//			if (button.elementClicked) {
//				ADSSketch * destinationSketch = [[button transition]destinationSketch]; // tansluc
//				[self.delegate displayTransition : destinationSketch];
//				[self.delegate showTransitionsCollection : YES]; // -.-
//				NSLog(@"PopOver ?");
//			}
//            
//			NSLog(@"Button %d is drawn",k);
//			k++;
//			CGContextSaveGState(context);
//            {
//				CGContextSetLineWidth(context, 1.0);
//				if (button.elementClicked) {        // transluc
//					[purpleColor setFill];
//					button.elementClicked = NO;
//					deleteTransitionImage = NO;
//					NSLog(@"PURPLE");
//                    
//				} else {
//					[blueColor setFill];            // ---
//				}
//				CGContextBeginPath(context);
//				CGContextAddRect(context, button.location);
//				CGContextStrokePath(context);
//				CGContextStrokeRect(context, button.location);
//				UIRectFill(button.location);
//                
//				if (!( (NDSketchViewController *) self.delegate ).enableTest.on) {
//					// if the test mode is enabled, don't show the delete button on the UIElement
//					NSDictionary * textAttributes =
//                    @{ NSFontAttributeName : [UIFont systemFontOfSize : 18.0],
//                       NSForegroundColorAttributeName : [UIColor whiteColor] };
//					int fontX = button.location.origin.x + button.location.size.width - 15;
//					int fontY = button.location.origin.y;
//					[@"X" drawAtPoint : CGPointMake(fontX, fontY) withAttributes : textAttributes];
//				}
//			}
//			CGContextRestoreGState(context);
//		}
//	}
//    
//	if (deleteTransitionImage) {        // --- transluc
//		[self.delegate displayTransition : nil];
//		[self.delegate showTransitionsCollection : NO]; // -.-
//	}
//    
//	// draw the rectangle which is under the user touch event
//	if (startDrawig) {
//		double height = abs(self.startPoint.x - self.endPoint.x);
//		double width = abs(self.startPoint.y - self.endPoint.y);
//		self.drawingRect = CGRectMake(fmin(self.startPoint.x, self.endPoint.x),
//									  fmin(self.startPoint.y, self.endPoint.y),
//									  height, width);
//		CGContextSaveGState(context);
//        {
//			CGContextSetLineWidth(context, 1.0);
//			[purpleColor setFill];
//			CGContextBeginPath(context);
//			CGContextAddRect(context, self.drawingRect);
//			UIRectFill(self.drawingRect);
//			CGContextStrokeRect(context, self.drawingRect);
//			CGContextStrokePath(context);
//		}
//		CGContextRestoreGState(context);
//	}
//	else {
//		[self setNeedsDisplay];
//	}
//}
//
//- (void) touchesBegan : (NSSet *) touches withEvent : (UIEvent *) event{
//	NSLog(@"touchesBegan: %d",j);
//	j++;
//	if (!self.project.drawOK) {
//		if (self.project.storyboard.getSketchesCount == 0) {
//			UIAlertView * alert = [[UIAlertView alloc]initWithTitle : @"Hint"
//															message : @"Please add a sketch first"
//														   delegate : nil
//												  cancelButtonTitle : @"OK"
//												  otherButtonTitles : nil];
//			[alert show];
//			return;
//		}
//        
//		UIAlertView * alert = [[UIAlertView alloc]initWithTitle : @"Hint"
//														message : @"Please select a sketch first"
//													   delegate : nil
//											  cancelButtonTitle : @"OK"
//											  otherButtonTitles : nil];
//		[alert show];
//		return;
//	}
//    
//	UITouch * touch = [[event allTouches] anyObject];
//	self.startPoint = [touch locationInView : touch.view];
//	// Get the specific point that was touched
//	CGPoint point = self.startPoint;
//    
//	// See if the point touched is within these rectangular bounds
//	if (self.selectedSketch != nil) {
//		if ([self.selectedSketch elementsCount] > 0) {
//			for (int i = 0; i < [self.selectedSketch elementsCount]; i++) {
//				ADSButton * element = (ADSButton *) [self.selectedSketch elementAtIndex : i];
//                
//				NSLog(@"button no: %d",i);
//                
//				CGRect closeRect = CGRectMake(
//                                              element.location.origin.x + element.location.size.width - 20,
//                                              element.location.origin.y,
//                                              20, 20);
//                
//				if (!( (NDSketchViewController *) self.delegate ).enableTest.on) { // Designer Mode is Enabled
//					// detect the if the user touch is pointed to the close button in the UIElement
//					if ( CGRectContainsPoint(closeRect, point) ) {
//						// delete UIElement
//						[self deleteUIElement : i];
//						startDrawig = NO;
//						return;
//					}
//					// detect if the user wants to create a new UIElement inside an existing UIElement => disable
//					if ( CGRectContainsPoint(element.location, point) ) {
//						element.elementClicked = YES; // ---
//						[self.delegate saveButtonIndex : i]; // -.-
//                        //						return;
//					}
//				}
//				else { // Tester Mode is Enabled
//                    // detect if user touch is inside the UIElement -> jump to destination Sketch
//					if ( CGRectContainsPoint(element.location, point) ) {
//						ADSSketch * destinationSketch = [[element transition]destinationSketch];
//						NSInteger sketchIndex =
//                        [[self.project storyboard]getSketchIndex : destinationSketch];
//						if ( ( (NDSketchViewController *) self.delegate ).enableTest.on )
//							[self.delegate jumpToDestinationSketch : sketchIndex sourceIndex : self.
//							 selectedSketchIndex];
//						break;
//					}
//				}
//			}
//		}
//	}
//	if (!( (NDSketchViewController *) self.delegate ).enableTest.on)
//		startDrawig = YES;
//	else
//		startDrawig = NO;
//}
//
//- (void) deleteUIElement : (NSInteger) elementIndex{
//	NSLog(@"delete");
//	ADSUIElement * element = [self.selectedSketch elementAtIndex : elementIndex];
//	[self.serverRequestHandler deleteUIElement : element parentSketch : self.selectedSketch
//								 parentproject : self.project success :^ {
//                                     NSLog(@"Delete UIlement Success!");
//                                 } failure :^ (NSError * error) {
//                                     NSLog(@"Error to delete UIElement %@", element);
//                                 }];
//	[self.selectedSketch deleteElement : element];
//	[self setNeedsDisplay];
//}
//
//- (void) touchesEnded : (NSSet *) touches withEvent : (UIEvent *) event{
//	NSLog(@"TouchesEnded: %d",j);
//	if (startDrawig) {
//		UITouch * touch = [[event allTouches] anyObject];
//		self.endPoint = [touch locationInView : touch.view];
//		if (abs(self.endPoint.x -
//				self.startPoint.x) < 10 | abs(self.endPoint.y - self.startPoint.y) < 10) {
//			[self setNeedsDisplay];
//			startDrawig = NO;
//			return;
//		}
//		// diable the creation of new UIElement if it intersects with another UIElement
//		if ([self.selectedSketch elementsCount] > 0) {
//			for (int i = 0; i < [self.selectedSketch elementsCount]; i++) {
//				ADSButton * element = (ADSButton *) [self.selectedSketch elementAtIndex : i];
//                
//				if ( CGRectIntersectsRect(self.drawingRect, element.location) ) {
//					startDrawig = NO;
//					[self setNeedsDisplay];
//					return;
//				}
//			}
//		}
//		if (!( (NDSketchViewController *) self.delegate ).enableTest.on)
//			[self showPopover];
//		else
//			[self setNeedsDisplay];
//	}
//	startDrawig = NO;
//}
//
//- (void) showPopover{
//	NSLog(@"showPopover");
//	UIStoryboard * firstStoryboard = [UIStoryboard storyboardWithName : @"Main_iPad" bundle : nil];
//	NDTransitionDestinationSelectViewController * transitionController =
//    [firstStoryboard instantiateViewControllerWithIdentifier : @"TransitionNavController"];
//	transitionController.project = self.project;
//    
//	self.popover =
//    [[UIPopoverController alloc] initWithContentViewController : transitionController];
//	self.popover.delegate = self;
//	CGSize size = CGSizeMake(212, 640);
//	self.popover.popoverContentSize = size;
//    
//	[self.popover presentPopoverFromRect : CGRectMake(488, 318, 0, 0)
//								  inView : self
//				permittedArrowDirections : UIPopoverArrowDirectionAny animated : YES];
//    
//	// TODO: Make this background (including the collection!) color fit the rest of the app better
//	self.popover.backgroundColor = [UIColor blackColor];
//    
//	transitionController.delegate = self;
//}
//
//- (void) touchesMoved : (NSSet *) touches withEvent : (UIEvent *) event{
//	NSLog(@"touchesMoved");
//	for (UITouch * touch in touches) { // Get location of Touch
//		CGPoint location = [touch locationInView : self];
//		self.endPoint = location;
//		[self setNeedsDisplay];
//	}
//}
//
//- (void) createTransition : (ADSSketch *) destinationSketch{
//	NSLog(@"createTrans");
//	if (self.selectedSketch.elements != nil & destinationSketch != nil) {
//		ADSButton * newButton = [[ADSButton alloc] init];
//		newButton.location = self.drawingRect;
//		ADSTransition * newTransition = [[ADSTransition alloc]initWithSketch : destinationSketch];
//		[newButton addTransition : newTransition];
//		[self.selectedSketch addElement : newButton];
//		[self setNeedsDisplay];
//	}
//}
//
//- (void) refereshUIElements{
//	NSLog(@"refresh");
//	[self setNeedsDisplay];
//}
//
//@end
