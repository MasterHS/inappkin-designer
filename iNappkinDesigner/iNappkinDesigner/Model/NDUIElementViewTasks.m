//
//  NDUIElementViewTasks.m
//  iNappkinDesigner
//
//  Created by Luc Gaasch on 26/06/14.
//  Copyright (c) 2014 Adesso. All rights reserved.
//

#import "NDUIElementViewTasks.h"
@class TransitionButton;
#import "NDTasksViewController.h"
#import "CoreDataManager.h"
#import "NDHelpers.h"
@interface NDUIElementViewTasks ()
@end

@implementation NDUIElementViewTasks

const int invalidSketchIndex = -1;
CGContextRef context;
NSArray* sketches;
- (id) initWithFrame: (CGRect)frame withSketchIndex: (NSInteger)index inProject: (Project *)project{
	self = [super initWithFrame : [[UIScreen mainScreen] bounds]];
	if (self) {
		self.currentProject = project;
        
		// Set background color
		self.backgroundColor = [UIColor clearColor];
		self.selectedSketchIndex = index;
		if (index > invalidSketchIndex) {
            sketches=[[CoreDataManager sharedManager]getSketches:self.currentProject.guid];
            self.selectedSketch = [sketches objectAtIndex:self.selectedSketchIndex];
		}
		else {
			self.selectedSketch = nil;
		}
		[self setNeedsDisplay];
	}
	return self;
}

- (void) touchesBegan : (NSSet *) touches withEvent : (UIEvent *) event{
    [super touchesBegan:touches withEvent:event];
    
	UITouch * touch = [[event allTouches] anyObject];
	self.startPoint = [touch locationInView : touch.view];
	// Get the specific point that was touched
	CGPoint point = self.startPoint;
    
	// See if the point touched is within these rectangular bounds
    NSArray* transitionButtons=[[CoreDataManager sharedManager]getTransitionButtons:self.selectedSketch.guid];
	if (self.selectedSketch != nil) {
		if ([transitionButtons count] > 0) {
			for (int i = 0; i < [transitionButtons count] ; i++) {
                TransitionButton * element = [ transitionButtons objectAtIndex:i];
				// Tester Mode is Enabled
                
				// detect if user touch is inside the UIElement -> jump to destination Sketch
                //CGRect location=CGRectMake([element.x doubleValue],[element.y doubleValue],[ element.width doubleValue],[ element.height doubleValue]);
                float absolutX= ([element.xAxis floatValue]/100)*self.frame.size.width;
                float absolutY= ([element.yAxis floatValue]/100)*self.frame.size.height;
                float absolutWidth = ([element.width floatValue]/100)*self.frame.size.width;
                float absolutHeight = ([element.height floatValue]/100)*self.frame.size.height;
                CGRect absolutRect= CGRectMake(absolutX, absolutY, absolutWidth, absolutHeight);
				if ( CGRectContainsPoint(absolutRect, point) ) {
                    Sketch * destinationSketch = [[CoreDataManager sharedManager]getDataById:element.destinationSketchGuid inTable:@"Sketch"];
                    if(destinationSketch!=nil)
                    {
					NSInteger sketchIndex =[sketches indexOfObject:destinationSketch];
					[self.delegate jumpToDestionation : sketchIndex]; // byLuc
					[self.delegate addStepToTask : element];
                    }
					break;
				}
			}
		}
	}
}

#pragma mark - Adding Buttons
- (void) drawRect : (CGRect) rect{
	[super drawRect : [[UIScreen mainScreen] bounds]];
	context = UIGraphicsGetCurrentContext();
	[self drawButton];
}

- (void) drawButton{
	// Set the rectangle outerline-colour
	CGContextSetStrokeColorWithColor(context, [UIColor blueColor].CGColor);
    
	// set the rectangle fill color
	UIColor * color = [UIColor colorWithRed : 0.041 green : 0.375 blue : 0.998 alpha : 0.4];
    
	if (self.selectedSketch != nil) {
        

NSArray* transitionButtons=[[CoreDataManager sharedManager]getTransitionButtons:self.selectedSketch.guid];
		for (TransitionButton * tButton in transitionButtons) {
            CGContextSaveGState(context);{
				CGContextSetLineWidth(context, 1.0);
				[color setFill];
				CGContextBeginPath(context);
                float absolutX= ([tButton.xAxis floatValue]/100)*self.frame.size.width;
                float absolutY= ([tButton.yAxis floatValue]/100)*self.frame.size.height;
                float absolutWidth = ([tButton.width floatValue]/100)*self.frame.size.width;
                float absolutHeight = ([tButton.height floatValue]/100)*self.frame.size.height;
                CGRect absolutRect= CGRectMake(absolutX, absolutY, absolutWidth, absolutHeight);
				CGContextAddRect(context, absolutRect);
				CGContextStrokePath(context);
                
				CGContextStrokeRect(context, absolutRect);
				UIRectFill(absolutRect);
			}
			CGContextRestoreGState(context);
		}
	}
}

- (void) refereshUIElements
{
	[self setNeedsDisplay];
}

@end
