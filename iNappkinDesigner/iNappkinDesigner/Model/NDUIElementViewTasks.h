//
//  NDUIElementViewTasks.h
//  iNappkinDesigner
//
//  Created by Luc Gaasch on 26/06/14.
//  Copyright (c) 2014 Adesso. All rights reserved.
//

/*
 Dient der Anzeige von Transitions in der Taskview (hier koennen diese z.B. nicht geloescht werden.
 Haben Funktion on Touch(rufen derzeit eine Trasition auf)
 */

@import UIKit;
#import "Project.h"
#import "Sketch.h"
#import "TransitionButton.h"

@interface NDUIElementViewTasks : UIView<UIGestureRecognizerDelegate>

@property (nonatomic, strong) Project *currentProject;
@property (nonatomic,strong) Sketch *destenationSketch;
@property (nonatomic,strong) id delegate;
@property (readwrite) CGPoint startPoint;
@property CGContextRef context;
@property (nonatomic, strong) Sketch * selectedSketch;
@property NSInteger selectedSketchIndex;

- (id) initWithFrame : (CGRect) frame withSketchIndex : (NSInteger) index inProject : (Project *)project;

- (void) refereshUIElements;


// @property (readwrite) CGPoint endPoint;
// @property CGRect drawingRect;
// -(void) createTransition:(ADSSketch*) destinationSketch;
@end

