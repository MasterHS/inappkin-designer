//
//  NDTransitionButtonOverlayViewController.h
//  iNappkinDesigner
//
//  Created by Julian-Lennart Weigand on 18.08.14.
//  Copyright (c) 2014 Adesso. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Sketch.h"
#import "TransitionButton.h"

@interface NDTransitionButtonOverlayView : UIView <UIGestureRecognizerDelegate>

@property (nonatomic, retain) Sketch *currentSketch;

//Start- und Endpunkt eines Touchevents, mit dem der TransitionButton gemalt wird
@property (readwrite) CGPoint startPoint;
@property (readwrite) CGPoint endPoint;
@property UILabel *buttonSizeIndicatorLabel;

@property (nonatomic,strong) id delegate;

//YES falls der User gerade im Tester Mode ist; NO falls er im Designer Mode ist - Wird im SketchVC mit Switch gesetzt
@property BOOL testerMode;

-(void)setNewActiveSketch:(Sketch*)sketch;
- (void)drawAllButtons;
-(void)setAllButtonBackgroundcolorsToBlue;

@end
