//
//  NDSettingsViewController.h
//  iNappkinDesigner
//
//  Created by Safetli, Hazem on 31/10/14.
//  Copyright (c) 2014 Adesso. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NDSettingsViewController : UIViewController<UINavigationControllerDelegate>
@property (weak, nonatomic) IBOutlet UITextField *serverTextField;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *saveSettingsButton;


@end
