//
//  NDCreateTaskViewController.h
//  iNappkinDesigner
//
//  Created by Safetli, Hazem on 05/11/14.
//  Copyright (c) 2014 Adesso. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Task.h"
#import "Project.h"
@class NDTasksViewController;

@protocol SaveTaskDelegate
-(void)createTask:(NSString*)taskName taskDescription:(NSString*)desc;
-(void)updateTask:(NSString*)taskName taskDescription:(NSString*)desc;
@end


@interface NDCreateTaskViewController : UIViewController<UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (weak, nonatomic) IBOutlet UITextField * taskTitleTextbox;
@property (weak, nonatomic) IBOutlet UITextView * taskDescriptionTextbox;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *saveTaskButton;

@property (nonatomic, strong) Project * currentProject;
@property (nonatomic, retain) Task * currentTask;
@property (nonatomic, strong) NSString *recentSegue;
@property (weak,nonatomic) id<SaveTaskDelegate> delegate;
@end
