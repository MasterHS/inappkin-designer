//
//  NDReports.h
//  iNappkinDesigner
//
//  Created by Safetli, Hazem on 10/02/15.
//  Copyright (c) 2015 Adesso. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Task.h"
@import MessageUI;
@interface NDReportsViewController : UIViewController<MFMailComposeViewControllerDelegate>

@property IBOutlet UIWebView* webView;
- (IBAction)resetStatistics:(id)sender;
- (IBAction)sendViaMail:(id)sender;
- (IBAction)exportToPDF:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *restStatsButton;


- (id) initWithProject:(Project*)proj;
-(void)generateReport;

@end
