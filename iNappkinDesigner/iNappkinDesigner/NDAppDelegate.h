//
//  NDAppDelegate.h
//  iNappkinDesigner
//
//  Created by Orest on 01/05/14.
//  Copyright (c) 2014 adesso. All rights reserved.
//

@import UIKit;

@interface NDAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow * window;

@end
