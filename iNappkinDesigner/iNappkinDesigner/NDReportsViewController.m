//
//  NDReports.m
//  iNappkinDesigner
//
//  Created by Safetli, Hazem on 10/02/15.
//  Copyright (c) 2015 Adesso. All rights reserved.
//

#import "NDReportsViewController.h"
#import "Task.h"
#import "TaskStat.h"
#import "Project.h"
#import "SlimServerRequestHandler.h"
#import "NDHelpers.h"
#import "QuartzCore/QuartzCore.h"
#import "Task.h"
#import "CoreDataManager.h"

NSArray* stats;
Project* project;
@implementation NDReportsViewController


- (void)viewDidLoad
{
    
    [super viewDidLoad];
    if(![[NDHelpers getAppUser]isEqualToString:project.userName])
    {
        self.restStatsButton.enabled=NO;
    }
    else
        self.restStatsButton.enabled=YES;
    
    
    NSArray *path = NSSearchPathForDirectoriesInDomains(
                                                        NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDirPath = [path objectAtIndex:0];
    NSString *fileName = @"report.html";
    
    NSString *fileAndPath = [documentDirPath stringByAppendingPathComponent:fileName];
    
    //NSArray *stuff = [[NSArray alloc] initWithContentsOfFile:fileAndPath];
    
    //NSString *htmlFile = [[NSBundle mainBundle] pathForResource:@"report" ofType:@"html"];
    NSString* htmlString = [NSString stringWithContentsOfFile:fileAndPath encoding:NSUTF8StringEncoding error:nil];
    [self.webView loadHTMLString:htmlString baseURL:nil];
}
- (IBAction)resetStatistics:(id)sender {
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle : @"Reset Statistics" message : @"All statistics and heat maps will be deleted permanently. Do you want to proceed?"                 delegate:self
                                            cancelButtonTitle:@"Cancel"
                                            otherButtonTitles:@"Proceed", nil];
    
    [alert show];
    
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
////    SlimServerRequestHandler *requestHandler= [[SlimServerRequestHandler alloc] initWithBaseURLStringForGeneralFunctionality:[NDHelpers getBaseServerName]];
//    NSArray* tasks=[[CoreDataManager sharedManager]getTasks:project.guid];
//    switch(buttonIndex) {
//        case 0: //"No" pressed
//            //do something?
//            break;
//        case 1: //"Yes" pressed
//            //here you pop the viewController
//            
////            for(Task* t in tasks)
////            {
////                //[requestHandler deleteTaps:project andTask:t];
////                //[requestHandler deleteStats:project andTask:t];
////            }
//            break;
//    }
}

- (IBAction)sendViaMail:(id)sender {
    //convert UIWebView to PDF file
    NSError *error = nil;
    
    NSArray *path = NSSearchPathForDirectoriesInDomains(
                                                        NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDirPath = [path objectAtIndex:0];
    NSString *fileName = @"report.pdf";
    
    NSString *fileAndPath = [documentDirPath stringByAppendingPathComponent:fileName];
    [self createPDFfromUIView:self.webView saveToDocumentsWithFileName:@"report.pdf"];
    if ( error )
    {
        // do some stuff
    }
    
    //send the PDF via email
    if ([MFMailComposeViewController canSendMail])
    {
        MFMailComposeViewController *vc = [[MFMailComposeViewController alloc] init];
        vc.mailComposeDelegate = self;
        
        [vc setSubject:@"iNappkin Report"];
        
        
        //NSString *pdfPath = [[NSBundle mainBundle] pathForResource:@"ApacheInstallationSteps" ofType:@"pdf"];
        NSData *pdfData = [NSData dataWithContentsOfFile:fileAndPath];
        [vc addAttachmentData:pdfData mimeType:@"application/pdf" fileName:@"iNappkin_Report"];
        
        [self presentViewController:vc animated:YES completion:nil];
    }
    else
    {
        UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:@"Alert!!!" message:@"Your device can't send emails." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [errorAlert show];
    }
}
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    // Notifies users about errors associated with the interface
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Result: canceled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Result: saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Result: sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Result: failed");
            break;
        default:
            NSLog(@"Result: not sent");
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)exportToPDF:(id)sender {
    
    
    
}

- (id) initWithProject:(Project*)proj
{
    self = [super init];
    
    if (self) {
        project=proj;
    }
    return self;
}


-(NSInteger)numberOfUsers:(Task*)task
{
    stats=[[CoreDataManager sharedManager]getTaskStats:task.guid];
    NSMutableArray *deviceIds =[[NSMutableArray alloc] init];
    for(TaskStat *tStat in stats)
    {
        Task* task=[[CoreDataManager sharedManager]getDataById:tStat.taskGuid inTable:@"Task"];
        if([task.taskName isEqualToString:task.taskName])
        {
            if(![deviceIds containsObject:tStat.deviceIdentifier])
            {
                if(tStat.deviceIdentifier!=nil)
                    [deviceIds addObject:tStat.deviceIdentifier];
            }
            
        }
    }
    
    return [deviceIds count];
}

-(NSInteger)numberOfTries:(Task*)task
{
    NSInteger tries=0;
    stats=[[CoreDataManager sharedManager]getTaskStats:task.guid];
    for(TaskStat *tStat in stats)
    {
        Task* task=[[CoreDataManager sharedManager]getDataById:tStat.taskGuid inTable:@"Task"];
        if([task.taskName isEqualToString:task.taskName])
        {
            tries++;
        }
    }
    return tries;
}

-(NSInteger)numberOfSuccessefulTries:(Task*)task
{
    NSInteger tries=0;
    stats=[[CoreDataManager sharedManager]getTaskStats:task.guid];
    for(TaskStat *tStat in stats)
    {
        Task* task=[[CoreDataManager sharedManager]getDataById:tStat.taskGuid inTable:@"Task"];
        if([task.taskName isEqualToString:task.taskName])
        {
            if([tStat.taskCompleted isEqualToString:@"YES"])
                tries++;
        }
    }
    return tries;
}

-(NSInteger)numberOfFailedTries:(Task*)task
{
    NSInteger tries=0;
    stats=[[CoreDataManager sharedManager]getTaskStats:task.guid];
    for(TaskStat *tStat in stats)
    {
        Task* task=[[CoreDataManager sharedManager]getDataById:tStat.taskGuid inTable:@"Task"];
        if([task.taskName isEqualToString:task.taskName])
        {
            if([tStat.taskCompleted isEqualToString:@"NO"])
                tries++;
        }
    }
    return tries;
}

-(float)averageTimeOnFailedTasks:(Task*)task
{
    NSInteger numberOfTries=0;
    float sumTime=0;
    stats=[[CoreDataManager sharedManager]getTaskStats:task.guid];
    for(TaskStat *tStat in stats)
    {
        Task* task=[[CoreDataManager sharedManager]getDataById:tStat.taskGuid inTable:@"Task"];
        if([task.taskName isEqualToString:task.taskName])
        {
            if([tStat.taskCompleted isEqualToString:@"NO"])
            {
                numberOfTries++;
                sumTime=sumTime+ [tStat.taskCompletionTime floatValue];
            }
        }
    }
    return sumTime/numberOfTries;
}

-(float)averageTimeOnSuccessfullTasks:(Task*)task
{
    NSInteger numberOfTries=0;
    float sumTime=0;
    stats=[[CoreDataManager sharedManager]getTaskStats:task.guid];
    for(TaskStat *tStat in stats)
    {
        Task* task=[[CoreDataManager sharedManager]getDataById:tStat.taskGuid inTable:@"Task"];
        if([task.taskName isEqualToString:task.taskName])
        {
            if([tStat.taskCompleted isEqualToString:@"YES"])
            {
                numberOfTries++;
                sumTime=sumTime+ [tStat.taskCompletionTime floatValue];
            }
        }
    }
    return sumTime/numberOfTries;
}

-(void)generateReport
{
    
    
    NSMutableString *htmlText = [[NSMutableString alloc] init];
    [htmlText appendFormat: @"<html><head><title>iNappkin Report</title></head>"];
    [htmlText appendFormat: @"<body>"];
    [htmlText appendFormat: @"<h1><u>Report | iNappkin</u><h1>"];
    [htmlText appendFormat: @"<h2>Project Name:</h2><h3> %@</h3>",project.projectName];
    [htmlText appendFormat: @"<h2>Project Description:</h2><h3> %@</h3>",project.projectDescription];
    [htmlText appendFormat:@"<p></p><p></p><p></p><p></p><p></p><p></p>"];
    //Html table style
    [htmlText appendFormat:@"<style type=\"text/css\">\
     body {height: 842px;width: 595px;\
     margin-top:70px;margin-left:50px;margin-right:50px;}\
     .tg  {border-collapse:collapse;border-spacing:0;}\
     .tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}\
     .tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}\
     </style>\
     <table class=\"tg\">\
     <colgroup>\
     <col style=\"width: 250px\">\
     <col style=\"width: 150px\">\
     <col style=\"width: 150px\">\
     <col style=\"width: 150px\">\
     </colgroup>\
     <tr>\
     <th class=\"tg-031e\"></th>"];
    //----------------------
    //      First Table
    //---------------------
    //add column headers (task name)
    NSArray* tasks=[[CoreDataManager sharedManager]getTasks:project.guid];
    for (Task* t in tasks){
        [htmlText appendFormat:@"<th class=\"tg-031e\">%@</th>",t.taskName];
    }
    [htmlText appendFormat:@"</tr>"];
    
    
    //add number of users for each task
    [htmlText appendFormat:@"<tr>\
     <td class=\"tg-031e\">Number of Users</td>"];
    for (Task* t in tasks){
        [htmlText appendFormat:@"<th class=\"tg-031e\">%li</th>",(long)[self numberOfUsers:t]];
    }
    [htmlText appendFormat:@"</tr>"];
    
    
    //add number of tries for each task
    [htmlText appendFormat:@"<tr>\
     <td class=\"tg-031e\">Number of Attempts</td>"];
    for (Task* t in tasks){
        [htmlText appendFormat:@"<th class=\"tg-031e\">%li</th>",(long)[self numberOfTries:t]];
    }
    [htmlText appendFormat:@"</tr>"];
    
    
    //add number of successfull tries for each task
    [htmlText appendFormat:@"<tr>\
     <td class=\"tg-031e\">Number of Successful Attempts</td>"];
    for (Task* t in tasks){
        [htmlText appendFormat:@"<th class=\"tg-031e\">%li</th>",(long)[self numberOfSuccessefulTries:t]];
    }
    [htmlText appendFormat:@"</tr>"];
    
    
    //add number of Failed Tries for each task
    [htmlText appendFormat:@"<tr>\
     <td class=\"tg-031e\">Number of Failed Attempts</td>"];
    for (Task* t in tasks){
        [htmlText appendFormat:@"<th class=\"tg-031e\">%li</th>",(long)[self numberOfFailedTries:t]];
    }
    [htmlText appendFormat:@"</tr>"];
    
    
    [htmlText appendFormat:@"</table>"];
    [htmlText appendFormat:@"<p></p><p></p><p></p><p></p><p></p><p></p>"];
    //----------------------
    //      Second Table
    //---------------------
    [htmlText appendFormat:@"<table class=\"tg\">\
     <colgroup>\
     <col style=\"width: 250px\">\
     <col style=\"width: 150px\">\
     <col style=\"width: 150px\">\
     <col style=\"width: 150px\">\
     </colgroup>\
     <tr>\
     <th class=\"tg-031e\"></th>"];
    
    //add column headers (task name)
    for (Task* t in  tasks){
        [htmlText appendFormat:@"<th class=\"tg-031e\">%@</th>",t.taskName];
    }
    [htmlText appendFormat:@"</tr>"];
    
    //Average Time on Failed Tasks
    [htmlText appendFormat:@"<tr>\
     <td class=\"tg-031e\">Average Time on Failed Tasks</td>"];
    for (Task* t in tasks){
        [htmlText appendFormat:@"<th class=\"tg-031e\">%f</th>",[self averageTimeOnFailedTasks:t]];
    }
    [htmlText appendFormat:@"</tr>"];
    
    
    //Average Time on success Tasks
    [htmlText appendFormat:@"<tr>\
     <td class=\"tg-031e\">Average Time on Successful Tasks</td>"];
    for (Task* t in tasks){
        [htmlText appendFormat:@"<th class=\"tg-031e\">%f</th>",[self averageTimeOnSuccessfullTasks:t]];
    }
    [htmlText appendFormat:@"</tr>"];
    
    
    [htmlText appendFormat: @"</body>"];
    [htmlText appendFormat: @"</html>"];
    
    
    
    
    // create the file
    
    NSError *error = nil;
    
    NSArray *path = NSSearchPathForDirectoriesInDomains(
                                                        NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDirPath = [path objectAtIndex:0];
    NSString *fileName = @"report.html";
    
    NSString *fileAndPath = [documentDirPath stringByAppendingPathComponent:fileName];
    
    [htmlText writeToFile:fileAndPath atomically:YES encoding: NSUTF8StringEncoding error:&error];
    
    if ( error )
    {
        // do some stuff
    }
    
}



-(void)createPDFfromUIView:(UIView*)aView saveToDocumentsWithFileName:(NSString*)aFilename
{
    // Creates a mutable data object for updating with binary data, like a byte array
    UIWebView *webView = (UIWebView*)aView;
    NSString *heightStr = [webView stringByEvaluatingJavaScriptFromString:@"document.body.scrollHeight;"];
    
    int height = [heightStr intValue];
    //  CGRect screenRect = [[UIScreen mainScreen] bounds];
    //  CGFloat screenHeight = (self.contentWebView.hidden)?screenRect.size.width:screenRect.size.height;
    CGFloat screenHeight = webView.bounds.size.height;
    int pages = ceil(height / screenHeight);
    
    NSMutableData *pdfData = [NSMutableData data];
    UIGraphicsBeginPDFContextToData(pdfData, webView.bounds, nil);
    CGRect frame = [webView frame];
    for (int i = 0; i < pages; i++) {
        // Check to screenHeight if page draws more than the height of the UIWebView
        if ((i+1) * screenHeight  > height) {
            CGRect f = [webView frame];
            f.size.height -= (((i+1) * screenHeight) - height);
            [webView setFrame: f];
        }
        
        UIGraphicsBeginPDFPage();
        CGContextRef currentContext = UIGraphicsGetCurrentContext();
        //      CGContextTranslateCTM(currentContext, 72, 72); // Translate for 1" margins
        
        [[[webView subviews] lastObject] setContentOffset:CGPointMake(0, screenHeight * i) animated:NO];
        [webView.layer renderInContext:currentContext];
    }
    
    UIGraphicsEndPDFContext();
    // Retrieves the document directories from the iOS device
    NSArray* documentDirectories = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask,YES);
    
    NSString* documentDirectory = [documentDirectories objectAtIndex:0];
    NSString* documentDirectoryFilename = [documentDirectory stringByAppendingPathComponent:aFilename];
    
    // instructs the mutable data object to write its context to a file on disk
    [pdfData writeToFile:documentDirectoryFilename atomically:YES];
    [webView setFrame:frame];
}
@end
