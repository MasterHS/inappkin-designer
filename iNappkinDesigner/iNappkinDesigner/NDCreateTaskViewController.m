//
//  NDCreateTaskViewController.m
//  iNappkinDesigner
//
//  Created by Safetli, Hazem on 05/11/14.
//  Copyright (c) 2014 Adesso. All rights reserved.
//

#import "NDCreateTaskViewController.h"
#import "NDTasksViewController.h"
#import "NDHelpers.h"
@interface NDCreateTaskViewController ()

@end

@implementation NDCreateTaskViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"%@",self.navigationController);
    if(![[NDHelpers getAppUser]isEqualToString:self.currentProject.userName])
    {
        self.taskTitleTextbox.enabled=NO;
        self.taskDescriptionTextbox.editable=NO;
        self.saveTaskButton.enabled=NO;
    }
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackTranslucent;
    [self.taskDescriptionTextbox.layer setBorderColor : [[UIColor darkGrayColor] CGColor]];
    [self.taskDescriptionTextbox.layer setBorderWidth : 1.0];
    if(self.currentTask!=nil)
    {
        self.taskTitleTextbox.text=self.currentTask.taskName;
        self.taskDescriptionTextbox.text=self.currentTask.taskDescription;
    }
    // Do any additional setup after loading the view.
}

-(BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    if( [self.recentSegue isEqualToString:@"viewTask"])//update task info
    {
        [self.delegate updateTask:self.taskTitleTextbox.text taskDescription:self.taskDescriptionTextbox.text];
        
    }
    else// create a new task
    {
        [self.delegate createTask:self.taskTitleTextbox.text taskDescription:self.taskDescriptionTextbox.text];
    }
    [self dismissViewControllerAnimated:YES completion:nil];
    [self.navigationController popViewControllerAnimated:YES];
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)cancelTaskCreationView:(id)sender {
    //[self.navigationController popViewControllerAnimated:YES];
   // [self dismissViewControllerAnimated:YES completion:nil];
    //[self.parentViewController.navigationController popViewControllerAnimated:YES];
   // [self removeFromParentViewController];
}



@end
