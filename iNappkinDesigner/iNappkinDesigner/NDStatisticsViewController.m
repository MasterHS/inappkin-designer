//
//  NDStatisticsViewController.m
//  iNappkinDesigner
//
//  Created by Safetli, Hazem on 29/12/14.
//  Copyright (c) 2014 Adesso. All rights reserved.
//

#import "NDStatisticsViewController.h"
#import "NDTaskTimeViewController.h"
#import "NDTaskSuccessViewController.h"
#import "NDReportsViewController.h"
@implementation NDStatisticsViewController

- (void) viewDidLoad{
    [super viewDidLoad];
    NDTaskTimeViewController* taskView=[[self viewControllers] objectAtIndex:0];
    NDTaskSuccessViewController * taskSuccess=[[self viewControllers] objectAtIndex:1];
    
    taskView.currentProject=self.currentProject;
    taskView.TabBarHeight=self.tabBar.frame.size.height;
    taskSuccess.currentProject=self.currentProject;
    taskSuccess.TabBarHeight=self.tabBar.frame.size.height;
    NDReportsViewController* report=[[NDReportsViewController alloc]initWithProject:self.currentProject];
    [report generateReport];
}

@end
