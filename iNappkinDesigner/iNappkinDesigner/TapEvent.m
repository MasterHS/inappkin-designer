//
//  TapEvent.m
//  iNappkinDesigner
//
//  Created by Safetli, Hazem on 13/11/14.
//  Copyright (c) 2014 Adesso. All rights reserved.
//

#import "TapEvent.h"
#import "Sketch.h"
#import "Task.h"


@implementation TapEvent

@dynamic timeOnSketch;
@dynamic xAxis;
@dynamic yAxis;
@dynamic taskGuid;
@dynamic sketchGuid;
@dynamic sync_status;
@dynamic is_deleted;
@dynamic guid;
@dynamic last_modified;
@dynamic deviceType;
@end
