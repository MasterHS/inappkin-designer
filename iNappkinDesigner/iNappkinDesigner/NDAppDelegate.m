//
//  NDAppDelegate.m
//  iNappkinDesigner
//
//  Created by Orest on 01/05/14.
//  Copyright (c) 2014 adesso. All rights reserved.
//

#import "NDAppDelegate.h"
#import "AFNetworkActivityLogger.h"
#import "NDHelpers.h"
#import "NDMainDataManager.h"
#import "CoreDataManager.h"

@interface NDAppDelegate ()

@end

@implementation NDAppDelegate


- (BOOL) application : (UIApplication *) application didFinishLaunchingWithOptions : (NSDictionary *)launchOptions{
	// AFNetworkingTesting
#ifdef DEBUG
	[[AFNetworkActivityLogger sharedLogger] startLogging];
	[[AFNetworkActivityLogger sharedLogger] setLevel : AFLoggerLevelWarn];
#endif
    
	// Init NDHelpers
	[NDHelpers initHelpers];
    [self setColors];
    NSLog(@"app dir: %@",[[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject]);
	return YES;
}

- (void) setColors{
	// Status bar background color
	UIColor * colorForBarTint =
    [UIColor colorWithRed : 0 / 255.0f green : 118 / 255.0f blue : 255 / 255.0f alpha : 1.0f];
	[[UINavigationBar appearance] setBarTintColor : colorForBarTint];
	// Status bar text color
	UIColor * colorForTint =
    [UIColor colorWithRed : 255.0 / 255.0 green : 255.0 / 255.0 blue : 255.0 /
     255.0          alpha : 1.0];
	[[UINavigationBar appearance] setBarStyle : UIBarStyleBlackTranslucent];
	[[UINavigationBar appearance] setTitleTextAttributes : [NSDictionary
															dictionaryWithObjectsAndKeys :
															colorForTint,
															NSForegroundColorAttributeName, nil] ];
	[[UINavigationBar appearance] setTintColor : colorForTint];
	[[UIApplication sharedApplication] setStatusBarStyle : UIStatusBarStyleLightContent];
}

- (void) applicationWillResignActive : (UIApplication *) application{
}

- (void)applicationDidEnterBackground:(UIApplication *)application{
    
}

- (void) applicationWillEnterBackground : (UIApplication *) application{
    
}

- (void) applicationWillEnterForeground : (UIApplication *) application{
    
}

- (void) applicationDidBecomeActive : (UIApplication *) application{
}

- (void) applicationWillTerminate : (UIApplication *) application{
    
}

@end
