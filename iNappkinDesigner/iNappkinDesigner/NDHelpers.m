//
//  NDHelpers.m
//  iNappkinDesigner
//
//  Created by Orest on 17/05/14.
//  Copyright (c) 2014 Adesso. All rights reserved.
//

#import "NDHelpers.h"
#import "NDAppDelegate.h"

#import <CoreData/CoreData.h>
#import "CoreDataManager.h"
#import "Project.h"


NSString * const BaseURLString = @"http://ios14adesso-bruegge.in.tum.de:7000/";
NSString * const BaseURLStringForImage = @"http://ios14adesso-bruegge.in.tum.de:7001/";


NSString* appUser;
@implementation NDHelpers

+ (void) initHelpers{
	appDelegate = [[UIApplication sharedApplication] delegate];
}

#pragma mark - Server Namen

+(NSString*)getBaseServerName{
    NSString *savedValue = [[NSUserDefaults standardUserDefaults]stringForKey:@"baseServerName"];
    //entweder haben wir schon selber einen Server gespeichert oder wir laden die hardcoded adresse des uni servers
    if (savedValue && ![savedValue isEqualToString:@""]) {
        return savedValue;
    }
    return BaseURLString;
}

+(NSString*)getSyncStatus{
    NSString *savedValue = [[NSUserDefaults standardUserDefaults]stringForKey:@"syncStatus"];
    if (savedValue && ![savedValue isEqualToString:@""]) {
        return savedValue;
    }
    return nil;
}

+(void)setSyncStatus:(NSString*)status{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];//stringForKey:@"lastModified_timestamp"];
    [defaults setValue:status forKey:@"syncStatus"];
    [defaults synchronize];
}


#pragma mark - Device ID
+ (NSString *) getDevId{
	NSString * identifierForVendor = [[UIDevice currentDevice].identifierForVendor UUIDString];
	return identifierForVendor;
}

#pragma mark - Colors
+ (UIColor *)normalTextBoarderColor {
    return[UIColor colorWithRed: 120.0/255.0 green : 120.0 /255.0 blue: 120.0/255.0 alpha : 0.5];
}

+ (UIColor *)tasksBoarderColor {
    return[UIColor colorWithRed: 120.0/255.0 green : 120.0 /255.0 blue: 120.0/255.0 alpha : 0.3];
}

+(UIColor *)blueButtonColor{
    return [UIColor colorWithRed : 0.041 green : 0.375 blue : 0.998 alpha : 0.4];
}

+(UIColor *)purpleButtonColor{
    return[UIColor colorWithRed : 0.541 green : 0.075 blue : 0.498 alpha : 0.4];
}

//Gibt anahnd des Devicetyps Aspect Ratio und die Screengroesse zurueck
+(NSDictionary*) getAspectRatioAndScreensizeForDeviceType: (NSString*)deviceType{
    
    // Display aspect ratios (height:width) and screensize (heightxwidth
	// iPhone 4:        3:2         960  x 640
	// iPhone 5:        16:9        1136 x 640
    // iPhone 6:                    1134 x 750
    // iPhone 6+:                   1920 x 1080
	// iPad Retina:     4:3         2048 x 1536
    // iPad mini                    1024 x 768
    
	// Aspect ratio to use; default 4:3
	CGFloat sheightAspect = 4.0, swidthAspect = 3.0;
    // New resolution; default 2048x1535
	CGFloat sheight= 2048.0f, swidth = 1535.0f;
    
	// Determine which device type the current project is meant for
	if ([deviceType isEqualToString : @"iPhone 4"]) {
		sheight = 960;
		swidth = 640;
        sheightAspect = 3.0;
		swidthAspect = 2.0;
	} else if ([deviceType isEqualToString : @"iPhone 5"]) {
		sheight = 1136;
		swidth = 640;
        sheightAspect = 16.0;
		swidthAspect = 9.0;
    } else if ([deviceType isEqualToString : @"iPhone 6"]) {
        sheight = 1334;
        swidth = 750;
        sheightAspect = 16.0;
        swidthAspect = 9.0;
    }
    else if ([deviceType isEqualToString : @"iPhone 6+"]) {
        sheight = 1920;
        swidth = 1080;
        sheightAspect = 16.0;
        swidthAspect = 9.0;
    }else if ([deviceType isEqualToString : @"iPad Retina"]) {
		sheight = 2048;
		swidth = 1536;
        sheightAspect = 16.0;
		swidthAspect = 9.0;
	}
    else if ([deviceType isEqualToString : @"iPad Mini"]) {
        sheight = 1024;
        swidth = 768;
        sheightAspect = 16.0;
        swidthAspect = 9.0;
    }
    else//customized (delete later) for simulator
    {
        sheight = 960;
        swidth = 640;
        sheightAspect = 3.0;
        swidthAspect = 2.0;
    }
    
    NSNumber *height= [NSNumber numberWithFloat:sheight];
    NSNumber *width= [NSNumber numberWithFloat:swidth];
    NSNumber *aspectRatioHeight= [NSNumber numberWithFloat:sheightAspect];
    NSNumber *aspectRatioWidth= [NSNumber numberWithFloat:swidthAspect];
    
    NSArray *valueArray = [NSArray arrayWithObjects:width,height,aspectRatioWidth,aspectRatioHeight, nil];
    NSArray *keyArray = [NSArray arrayWithObjects:@"width", @"height",@"aspectRatioWidth",@"aspectRatioHeight", nil];
    
    return [NSDictionary dictionaryWithObjects:valueArray forKeys:keyArray];
}

#pragma mark - Bildbearbeitung
//Schneidet und skaliert Bilder damit sie der Screengroesse des anegebenen Device entsprechen
+ (UIImage*)cropScaleAndCompressImage:(UIImage*)image forDeviceType:(NSString*)deviceType{
    
    NSDictionary *valueDict = [self getAspectRatioAndScreensizeForDeviceType:deviceType];
    
    float heightAspect= [[valueDict objectForKey:@"aspectRatioHeight"]floatValue];
    float widthAspect= [[valueDict objectForKey:@"aspectRatioWidth"]floatValue];
    
	// Determine new dimensions and crop area
	CGFloat height, width;
	CGRect clippedRect;
    
	if (image.size.height * (widthAspect / heightAspect) == image.size.width) {
		NSLog(@"Skipping cropping");
		return image;
	} else if (image.size.height * (widthAspect / heightAspect) > image.size.width) {
		width = image.size.width;
		height = width * (heightAspect / widthAspect);
		NSLog(@"Too high, cropping to %.2f x %.2f", width, height);
		clippedRect = CGRectMake(0, (image.size.height - height) / 2, width, height);
	} else {
		height = image.size.height;
		width = height * (widthAspect / heightAspect);
		NSLog(@"Too wide, cropping to %.2f x %.2f", width, height);
		clippedRect = CGRectMake( (image.size.width - width) / 2, 0, width, height );
	}
    
	// Crop logic
	CGImageRef imageRef = CGImageCreateWithImageInRect([image CGImage], clippedRect);
	UIImage * newImage = [UIImage imageWithCGImage : imageRef scale : 1.0 orientation : image.imageOrientation];
	CGImageRelease(imageRef);
    
	// Scale
	CGRect rect = CGRectMake(0,0,width,height);
	UIGraphicsBeginImageContext(rect.size);
	[newImage drawInRect: rect];
	newImage = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
    
	// Compress to save space
	// Either reduce file size to 100 KB or stop trying at compressionQuality = 10%
	CGFloat compressionQuality = 0.9f; // Maximal (starting) compression quality
	CGFloat minimalCompressionQuality = 0.1f;
	int maxFileSize = 100 * 1024; // KB, for raw data only
	NSData * imageData = UIImageJPEGRepresentation(newImage, compressionQuality);
	while ([imageData length] > maxFileSize && compressionQuality > minimalCompressionQuality){
		compressionQuality -= 0.1;
		imageData = UIImageJPEGRepresentation(newImage, compressionQuality);
	}
	// Warning: For a calculated NSData size around 200 KB, UIImage will be ca. 100 KB larger
	return [UIImage imageWithData : imageData];
}

#pragma mark - Wertvalidierung

//prüft ob ein Projektname vergeben werden kann: Gibt YES zurueck falls der Name passt und NO falls er nicht passt
+(BOOL) checkNameValidityWithProjectName:(NSString*)projektName{
    if ([projektName isEqualToString:@""] || projektName == nil) {
        return NO;
    }
    NSManagedObjectContext *managedObjectContext = [[CoreDataManager sharedManager] managedObjectContext];
	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
	
	NSEntityDescription *entity = [NSEntityDescription entityForName:@"Project" inManagedObjectContext:managedObjectContext];
	[fetchRequest setEntity:entity];
	
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"projectName" ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObjects: sortDescriptor, nil];
   	[fetchRequest setSortDescriptors:sortDescriptors];
	NSFetchedResultsController *fetchedExhibitorController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:managedObjectContext sectionNameKeyPath:nil cacheName:nil];
	[fetchedExhibitorController performFetch:nil];
    NSArray *currentProjects =  [fetchedExhibitorController fetchedObjects];
    
    for (Project *tProject in currentProjects) {
        if ([tProject.projectName isEqualToString: projektName]) {
            return NO;
        }
    }
    return YES;
}

+(NSString*)getAppUser
{
    return appUser;
}
+(void)setAppUser:(NSString*)user
{
    appUser=user;
    
}

@end
