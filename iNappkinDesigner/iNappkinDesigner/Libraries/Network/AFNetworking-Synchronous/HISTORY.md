# AFNetworking-Synchronous Release History

## v0.2.0
November 5, 2013

Return `-responseData` for instances of `AFHTTPRequestOperation`.


## v0.1.0
September 29, 2013

Initial release, Cocoapods support.
