//
//  NDTransitionsListViewController.h
//  iNappkinDesigner
//
//  Created by Safetli, Hazem on 05/11/14.
//  Copyright (c) 2014 Adesso. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NDTransitionButtonOverlayView.h"
#import "Project.h"


@interface NDTransitionsListViewController : UIViewController<UICollectionViewDataSource, UICollectionViewDelegate,
UIImagePickerControllerDelegate, UINavigationControllerDelegate>
@property (strong,nonatomic) UIPopoverController* popover;
@property (weak, nonatomic) IBOutlet UICollectionView *transCollectionView;
@property (weak,nonatomic) id delegate;
@property (nonatomic) NSInteger indexOfCurrentDestinationSketch;
@property NSInteger selectedButtonIndex;
@property IBOutlet NDTransitionButtonOverlayView *transitionButtonOverlay;
@property UIButton * sender;
@property (strong,nonatomic) Project * currentProject;
//@property (strong, nonatomic) NSIndexPath * selectedSketchIndexPath;
@property TransitionButton *currentlySelectedTransitionButton;
@end
