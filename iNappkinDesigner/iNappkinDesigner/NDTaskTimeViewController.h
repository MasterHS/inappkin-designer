//
//  NDStatsViewController.h
//  iNappkinDesigner
//
//  Created by Safetli, Hazem on 11/11/14.
//  Copyright (c) 2014 Adesso. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CorePlot-CocoaTouch.h"
#import "Task.h"
#import "Project.h"

@interface NDTaskTimeViewController : UIViewController<CPTBarPlotDataSource,CPTBarPlotDelegate>

@property (nonatomic, retain) NSMutableArray *data;
@property  NSInteger TabBarHeight;
@property (nonatomic, retain) CPTGraphHostingView *hostingView;
@property (nonatomic, retain) CPTXYGraph *graph;
@property (weak, nonatomic) IBOutlet UIView *statsView;
@property (nonatomic, retain) Project* currentProject;

- (void) generateBarPlot;

@end
