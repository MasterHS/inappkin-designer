//
//  NDBackgoundLayer.h
//  iNappkinDesigner
//
//  Created by Fei Pan on 05/06/14.
//  Copyright (c) 2014 Adesso. All rights reserved.
//

/*
 BG Layer mit einem Gradienten (sweet ^^)
 */

#import <Foundation/Foundation.h>
#import <QuartzCore/QuartzCore.h>

@interface NDBackgroundLayer : NSObject

+ (CAGradientLayer *) greyGradient;
+ (CAGradientLayer *) blueGradient;

@end
