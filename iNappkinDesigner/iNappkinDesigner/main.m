//
//  main.m
//  iNappkinDesigner
//
//  Created by Orest on 01/05/14.
//  Copyright (c) 2014 adesso. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NDAppDelegate.h"

int main(int argc, char * argv[])
{
	@autoreleasepool {
		return UIApplicationMain( argc, argv, nil, NSStringFromClass([NDAppDelegate class]) );
	}
}


