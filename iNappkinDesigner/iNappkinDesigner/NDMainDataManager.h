//
//  NDSingletonSafe.h
//  iNappkinDesigner
//
//  Created by Julian-Lennart Weigand on 14.08.14.
//  Copyright (c) 2014 Adesso. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ADSProject;

@interface NDMainDataManager : NSObject

+(NDMainDataManager *)sharedMainDataManager;

+(BOOL) checkNameValidityWithProjectName:(NSString*)projektName;

@end
