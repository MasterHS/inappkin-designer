//
//  NDTransitionsListViewController.m
//  iNappkinDesigner
//
//  Created by Safetli, Hazem on 05/11/14.
//  Copyright (c) 2014 Adesso. All rights reserved.
//

#import "NDTransitionsListViewController.h"
#import "NDHelpers.h"
#import "NDProjectCollectionViewCell.h"
#import "SlimServerRequestHandler.h"
#import "CoreDataManager.h"
#import "NDSketchViewController.h"

@interface NDTransitionsListViewController ()

@end
@protocol TransitionViewDelegate

-(NSIndexPath*) getSketchIndextPath;

@end

@implementation NDTransitionsListViewController
NSArray* sketches;
- (void)viewDidLoad {
    [super viewDidLoad];
    [self loadData];
    self.transCollectionView.allowsMultipleSelection = NO;
    
    for (UIView *tView in [self.transitionButtonOverlay subviews]) {
        if ([tView isKindOfClass:[UIButton class]]) {
            if (tView.tag == self.selectedButtonIndex) {
                self.sender.backgroundColor = [NDHelpers purpleButtonColor];
            }
            else tView.backgroundColor = [NDHelpers blueButtonColor];
        }
    }
    
    // Do any additional setup after loading the view.
}
-(void)loadData
{
    sketches=[[CoreDataManager sharedManager]getSketches:self.currentProject.guid];
}
-(void)viewDidAppear:(BOOL)animated
{
    [self loadUICollectionViewInfo];
}

-(void)loadUICollectionViewInfo
{
    if (self.indexOfCurrentDestinationSketch != NSNotFound) {
        NSIndexPath *tIndexPath= [NSIndexPath indexPathForItem:self.indexOfCurrentDestinationSketch inSection:0];
        //NSIndexPath *tIndexPath=[self.delegate getSketchIndextPath];
        
        [self.transCollectionView selectItemAtIndexPath: tIndexPath animated:YES  scrollPosition: UICollectionViewScrollPositionBottom];
        [self.transCollectionView scrollToItemAtIndexPath: tIndexPath atScrollPosition: UICollectionViewScrollPositionBottom animated : NO];
        //[self collectionView:self.transCollectionView cellForItemAtIndexPath:tIndexPath];
        //        if ([[self.transCollectionView cellForItemAtIndexPath:tIndexPath] isSelected]) {
        //
        //            NSLog(@"selected count %i",[self.transCollectionView indexPathsForSelectedItems].count);
        //        }
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger) numberOfSectionsInCollectionView : (UICollectionView *) collectionView{
    return 1;
}


- (NSInteger) collectionView : (UICollectionView *) collectionView numberOfItemsInSection : (NSInteger) section{
    return [ sketches count];
}

- (UICollectionViewCell *) collectionView: (UICollectionView *) collectionView cellForItemAtIndexPath: (NSIndexPath *)indexPath{
    NDProjectCollectionViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier : @"projectSidebarThumbnail" forIndexPath: indexPath];
    if ([sketches objectAtIndex:indexPath.row] != nil){
        Sketch * sketch = [ sketches objectAtIndex:indexPath.row];
        cell.projectSidebarThumbnail.image = [UIImage imageWithData:sketch.imageFile];
        cell.backgroundColor = [UIColor clearColor];
        cell.sketch= sketch;
        if (cell.selected== YES) {
            cell.alpha = 1.0;
            cell.backgroundColor = [UIColor redColor];
        } else {
            cell.alpha = 0.6;
            cell.backgroundColor = [UIColor clearColor];
        }
        
    }
    return cell;
}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.item >= 0){
        Sketch * selectedDestinationSketch =[ sketches objectAtIndex: indexPath.row];
        //update transition button
        self.currentlySelectedTransitionButton=[[CoreDataManager sharedManager]createOrModifyTransitionButton:@{@"destinationSketchGuid":selectedDestinationSketch.guid} transButtonGuid:self.currentlySelectedTransitionButton.guid sketchGuid:self.currentlySelectedTransitionButton.sketchGuid syncStatus:@"1" deleted:@"0"];
        [self loadData];
        [self.transitionButtonOverlay setAllButtonBackgroundcolorsToBlue];
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    
}


//- (void) touchesBegan: (NSSet *) touches withEvent: (UIEvent *) event{
//    UITouch *touch = [[event allTouches] anyObject];
//    if ([touch view] != self.transCollectionView) {
//        [self dismissViewControllerAnimated:YES completion:nil];
//        //alle buttons wieder auf blau setzen
//        //self.deleteTransitionButton.enabled = NO;
//        [self.transitionButtonOverlay setAllButtonBackgroundcolorsToBlue];
//    }
//}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
